site_name: Documentation Zotero Fr
site_url: https://docs.zotero-fr.org
repo_url: https://framagit.org/zotero-fr/zotero-fr.frama.io
repo_name: Dépôt GitLab
edit_uri: edit/master/docs/
site_dir: public
theme:
  name: material
  font: false
  language: fr
  custom_dir: overrides
  logo: images/zotero-logo.png
  favicon: images/zotero-favicon.png
  features:
    - navigation.tabs
    - navigation.tabs.sticky
    - navigation.top
    - navigation.indexes
    - attr_list
    - content.action.edit
  icon:
    edit: material/note-edit
markdown_extensions:
  - attr_list
  - meta
  - admonition
  - pymdownx.details
  - pymdownx.superfences
  - footnotes
  - toc:
      permalink: 🔗
      permalink_title: Lien permanent vers cette section
extra_css:
  - stylesheets/extra.css
extra:
  analytics:
    provider: custom
nav:
  - Accueil: 'index.md'
  - 'quick_start_guide.md'
#     - 'translators'
  - 'installation.md'
  - 'Documentation':
    - 'Alimenter votre bibliothèque':
      - 'adding_items_to_zotero.md'
      - 'attaching_files.md'
      - 'feeds.md'
      - 'retrieve_pdf_metadata.md'
      - 'moving_to_zotero.md'
    - 'Organiser votre bibliothèque et prendre des notes':
      - 'collections_and_tags.md'
      - 'searching.md'
      - 'sorting.md'
      - 'pdf_reader.md'
#     - 'note_templates.md'
      - 'notes.md'
      - 'related.md'
      - 'duplicate_detection.md'
    - 'Créer des bibliographies, des citations et des rapports':
      - 'creating_bibliographies.md'
      - 'word_processor_integration.md'
#     - 'word_processor_plugin_usage.md'
#     - 'libreoffice_writer_plugin_usage.md'
#     - 'word_processor_plugin_installation.md'
#     - 'word_processor_plugin_manual_installation.md'
#     - 'word_processor_plugin_usage_classic.md'
      - 'styles.md'
      - 'reports.md'
#     - 'rtf_scan.md'
#     - 'google_docs.md'
    - 'Synchronisation, collaboration et sauvegarde':
      - 'sync.md'
      - 'groups.md'
      - 'my_publications.md'
      - 'zotero_data.md'
    - 'Préférences de Zotero':
      - 'preferences.md'
#    - 'advanced.md'
#    - 'cite.md'
#    - 'export.md'
#    - 'general.md'
#    - 'search.md'
#    - 'preferences_sync.md'
      - 'connector_preferences.md'
      - 'supported_languages.md'
    - 'Tirer le meilleur parti de Zotero':
      - 'locate.md'
      - 'timelines.md'
      - Extensions pour Zotero [EN] : https://www.zotero.org/support/plugins
      - Zotero pour les mobiles [EN] : https://www.zotero.org/support/mobile
      - 'tips_and_tricks.md'
    - 'À propos de Zotero':
      - Contacter Zotero [EN] : https://www.zotero.org/support/contact_us
      - Crédits et remerciements [EN] : https://www.zotero.org/support/credits_and_acknowledgments
      - Licences [EN] : https://www.zotero.org/support/licensing
      - 'security.md'
      - 'privacy.md'
  - "Obtenir de l'aide":
    - 'getting_help.md'
    - 'zotero_support.md'
    - 'forum_guidelines.md'
    - 'reporting_problems.md'
    - 'debug_output.md'
  - 'Base de connaissance':
    - kb/index.md
    - 'Usage basique et dépannage':
      - 'kb/preferences.md'
      - 'kb/profile_directory.md'
      - 'kb/portable_zotero.md'
      - Zotero offre-t-il des paquets installables de Zotero pour des distributions Linux spécifiques ?[EN] : https://www.zotero.org/support/kb/linux_packaging
      - 'kb/non-western_characters.md'
      - 'kb/file_handling_issues.md'
      - 'kb/installing_on_a_chromebook.md'
      - Comment basculer rapidement entre Zotero et mon navigateur, ma visionneuse PDF et/ou mon traitement de texte ?[EN] : https://www.zotero.org/support/kb/switching_between_programs
      - 'kb/font_size.md'
      - Comment changer la langue de l'interface utilisateur de Zotero ? : /supported_languages/#changer-de-langue
      - 'kb/uninstalling.md'
      - J'ai mis à jour vers Zotero 5.0 et maintenant mes données ont disparu ! Comment les récupérer ?[EN] : https://www.zotero.org/support/kb/data_missing_after_zotero_5_upgrade
      - 'kb/web_vs_desktop.md'
      - 'kb/library_items.md'
      - 'kb/corrupted_database.md'
      - 'kb/links_vs_snapshots.md'
      - 'kb/zotero_version.md'
      - 'kb/newer_db_version.md'
      - 'kb/proxy_troubleshooting.md'
      - 'kb/missing_item_tabs.md'
      - 'kb/connector_permissions.md'
      - 'kb/no_toolbar_button.md'
      - 'kb/updates_not_detected.md'
      - 'kb/safari_compatibility.md'
      - 'kb/connector_zotero_unavailable.md'
      - 'kb/keyboard_shortcuts.md'
      - Zotero semble envoyer des données sans me dire ce qu'il fait. Que se passe-t-il ? : /privacy/
    - 'Alimenter votre bibliothèque':
      - 'kb/snapshot_only_captures_first_page.md'
      - 'kb/default_translators.md'
      - 'kb/site_access_limits.md'
      - Comment puis-je modifier les métadonnées des articles ? : /adding_items_to_zotero/#editer-des-documents
      - 'kb/import_from_citavi.md'
      - 'kb/opera_browser.md'
      - 'kb/adding_a_letter_or_memo.md'
      - 'kb/archival_or_other_unpublished_sources.md'
      - 'kb/edited_volumes_and_book_chapters.md'
      - 'kb/storing_files.md'
      - Comment ajouter automatiquement un livre ou un article à Zotero ? : /adding_items_to_zotero/#via-votre-navigateur-web
      - 'kb/importing_standardized_formats.md'
      - 'kb/mendeley_import.md'
      - 'kb/importing_records_from_endnote.md'
      - 'kb/importing.md'
      - 'kb/media_creator_roles.md'
      - 'kb/manually_adding_items.md'
      - 'kb/saving_work.md'
      - 'kb/import_from_clipboard.md'
      - 'kb/importing_formatted_bibliographies.md'
      - Citations juridiques Juris-M[EN] : https://www.zotero.org/support/kb/legal_citations # pas traduit car centré sur les juridictions des Etats-unis, du Royaume-Uni et de l'Australie
      - 'kb/snapshot_is_unreadable.md'
      - 'kb/address_bar_icon_appears_slowly.md'
      - 'troubleshooting_translator_issues.md'
      - 'kb/watch_folder.md'
      - 'kb/page_not_recognized.md'
      - 'kb/no_address_bar_icon.md'
      - 'kb/item_types_and_fields.md'
    - 'Organiser votre bibliothèque et prendre des notes':
      - 'kb/library_sorting.md'
      - 'kb/item_count.md'
      - 'kb/collections_containing_an_item.md'
      - 'kb/merging_tags.md'
      - 'kb/merging_libraries.md'
      - 'kb/organizing_a_library.md'
    - 'Synchronisation':
      - 'kb/connection_error.md'
      - 'kb/incomplete_cert_chain.md'
      - 'kb/using_multiple_computers.md'
      - 'kb/switching_accounts.md'
      - 'kb/webdav_services.md'
      - 'kb/files_not_syncing.md'
      - 'kb/changes_not_syncing.md'
      - 'kb/file_sync_errors.md'
      - 'kb/repeated_conflicts.md'
      - 'kb/unknown_data_error.md'
      - 'kb/sync_reset_options.md'   
    - 'Lecteur PDF et éditeur de notes':
      - 'kb/zotfile_extract_annotations.md'
      - 'kb/doubled_highlights.md'
      - 'kb/annotations_in_database.md'
      - Comment organiser mes notes sous forme de plan ? : /reports/#construire-un-plan-a-partir-des-notes
      - 'kb/sorting_notes.md'
#      - Les annotations sont-elles synchronisées ?[EN] : https://www.zotero.org/support/kb/annotation_sync # lien invisibilisé car la page est complètement obsolète et date de 2017
#     - Puis-je surligner et annoter des PDF avec Zotero ?[EN] : https://www.zotero.org/support/kb/highlighting_and_annotating_pdfs # lien invisibilisé car la page est complètement obsolète et date de 2017
    - 'Modules pour logiciel de traitement de texte':
      - 'kb/debugging_broken_documents.md' # pas liée depuis accueil de la base de connaissance mais ajouté ici pour la rendre plus visible. Normalement liée depuis la page word_processor_plugin_troubleshooting absente de la doc FR
      - 'kb/addcitationdialog_raised.md'
      - 'kb/uninstalling_the_word_processor_plugin.md'
      - 'kb/no_toolbar_in_word_2008_plugin.md'
      - 'kb/citations_highlighted.md'
      - 'kb/citations_underlined.md'
      - 'kb/word_field_codes.md'
      - 'kb/citations_slow.md'
      - 'kb/citations_not_updating.md'
      - 'kb/existing_citations_not_detected.md'
      - 'kb/google_docs_citations_unlinked.md'
      - 'kb/mac_word_permissions_missing.md'
      - 'kb/moving_documents_between_word_processors.md'
    - 'Mise en forme des citations':
      - 'kb/bibliographies_in_different_languages.md'
      - 'kb/doi_in_apa.md'
      - 'kb/ams_alphabetic.md'
      - 'kb/idem.md'
      - 'kb/sentence_casing.md'
      - 'kb/preventing_title_casing_for_non-english_titles.md'
      - 'kb/rich_text_bibliography.md'
      - 'kb/secondary_citation.md'
      - 'kb/name_parsing.md'
      - 'kb/chicago_style_versions.md'
      - 'kb/requesting_styles_for_editors.md'
      - 'kb/journal_abbreviations.md'
      - 'kb/italics_in_word_bibliographies.md'
      - 'kb/word_default_font.md'
      - 'kb/style_standards.md'
      - 'kb/doi_in_bibliography.md'
      - 'kb/harvard_style.md'
      - 'kb/given_name_disambiguation.md'
      - 'kb/uppercase_subtitles.md'
    - 'Autres sujets':
      - Le domaine utilise un certificat de sécurité invalide[EN] : https://www.zotero.org/support/kb/ssl_certificate_error
      - 'kb/transferring_a_library.md'
      - 'kb/multiple_instances.md'
      - Comment puis-je utiliser Zotero avec un proxy SOCKS ?[EN] : https://www.zotero.org/support/kb/socks_proxy
      - 'kb/multiple_profiles.md'
      - 'kb/exporting.md'
      - Comment faire fonctionner ma collection Zotero avec une présentation Exhibit ou Citeline ?[EN] : https://www.zotero.org/support/kb/exhibit
      - Comment imprimer mes références et notes Zotero ?: /reports
      - 'kb/automatic_capitalization.md'
      - 'kb/cert_override.md'
      - Utilisation de Zotero pour créer des métadonnées COinS[EN] : https://www.zotero.org/support/dev/exposing_metadata/coins
      - Affichage des informations du certificat de site[EN] : https://www.zotero.org/support/kb/site_certificate_info
      - Quelles connexions dois-je autoriser à travers un pare-feu pour que Zotero fonctionne correctement ?[EN] : https://www.zotero.org/support/kb/zotero_and_firewalls
      - Que signifie Zotero ?[EN] : https://www.zotero.org/support/kb/etymology_of_zotero
      - 'kb/disk_io_error.md'
      - 'kb/missing_linked_file.md'
      - 'kb/missing_stored_file.md'
      - Pourquoi l'option Enregistrer dans Zotero n'apparaît-elle plus dans la boîte de dialogue Ouvrir/Enregistrer de Firefox ?[EN] : https://www.zotero.org/support/kb/no_firefox_open_save_option
      - 'kb/storage_quota_and_local_storage.md'
      - 'kb/zotero_and_wikipedia.md'
      - Zotero et la Text Encoding Initiative (TEI)[EN] : https://www.zotero.org/support/kb/tei
  - 'a_propos.md'
# - 'mentions_legales.md'
