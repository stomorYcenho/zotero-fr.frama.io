# Contribuer à la documentation

## Contacter nous ! :)

Contactez l'équipe de contributeur·rices via notre mail [contact@zotero-fr.org](mailto:contact@zotero-fr.org).

## Le wiki

Le [wiki dans le dépôt Framagit](https://framagit.org/zotero-fr/documentation-zotero-francophone/-/wikis/home) détaille la syntaxe spécifique à la rédaction de la documentation et conment résoudre les problèmes les plus fréquents.

## Développer localement

Pour travailler localement sur la documentation, suivre ces étapes :

1. Forker, cloner ou télécharger ce dépôt,
2. Installer [Python](https://www.mkdocs.org/user-guide/installation/#installing-python) et éventuellement [Pip](https://www.mkdocs.org/user-guide/installation/#installing-pip),
3. [Installer Material for MkDocs](https://squidfunk.github.io/mkdocs-material/getting-started/),
4. Générer le site en utilisant la commande : `mkdocs serve` (le site est alors accessible à l'adresse `localhost:8000`),
5. Ajouter ou modifier du contenu (le site en local est automatiquement rechargé pour tenir compte des changements),
6. Commiter les changements et les pousser sur le dépôt Framagit.

## Développer en ligne

Il est possible de travailler sur la documentation directement depuis Framagit.org, en utilisant le [Web IDE de GitLab](https://docs.gitlab.com/ee/user/project/web_ide/).
