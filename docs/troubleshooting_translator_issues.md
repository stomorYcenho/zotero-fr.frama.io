# Dépannage des problèmes d’enregistrement dans Zotero

*Consulter cette page dans la documentation officielle de Zotero : [Troubleshooting Problems Saving to Zotero](https://www.zotero.org/support/troubleshooting_translator_issues) - dernière mise à jour de la traduction : 2022-10-18*
{ data-search-exclude }

Si vous ne parvenez pas à enregistrer des métadonnées de haute qualité à partir d'un site Web particulier, il est fort probable que la page ne soit pas prise en charge par un [convertisseur Zotero](./translators.md) existant ou que la mise en page ait été récemment modifiée, ce qui empêche Zotero de reconnaître les données. Sur certains sites, notamment Google Scholar, vous pouvez également vous heurter à des [limites d'accès](./kb/site_access_limits).

Si le problème se produit sur plusieurs sites web, il est possible qu'il y ait un problème avec votre installation de Zotero, et vous devriez essayer ces étapes :

1. Si vous ne voyez pas du tout de bouton "Enregistrer dans Zotero" dans la barre d'outils de votre navigateur, __vérifiez que vous avez installé le connecteur Zotero dans votre navigateur__. Le connecteur Zotero doit être répertorié dans le volet "Extensions" du navigateur. Vous pouvez installer le connecteur Zotero approprié à partir de la [page de téléchargement](https://www.zotero.org/download/) de Zotero. Si l'extension est installée mais n'est pas visible, vous devrez peut-être [l'épingler à votre barre d'outils](./kb/no_toolbar_button.md).

2. Si vous ne voyez qu'une [icône de page Web grise](./kb/page_not_recognized.md), __vérifiez que vous êtes sur un [site pris en charge](./translators.md)__. Si vous n'êtes pas sûr, essayez un article de Wikipedia ou une page de livre Amazon.

3. Si vous êtes sur un site pris en charge, __essayez de recharger la page__ et __vérifiez qu'elle est entièrement chargée__. Appuyer sur le bouton d'arrêt de votre navigateur ou sur la touche Echap de votre clavier peut aider si un élément de la page s'est bloqué.

4. Si l'icône de page Web grise apparaît sur tous les sites, __essayez de redémarrer votre navigateur__.

5. Si le programme Zotero n'est pas ouvert, __essayez d'ouvrir Zotero avant d'enregistrer__. (Si vous n'avez pas installé Zotero, vous pouvez le faire à partir de la [page de téléchargement](https://www.zotero.org/download/)). Bien que le connecteur puisse enregistrer directement dans votre bibliothèque en ligne, vous obtiendrez généralement de meilleurs résultats en enregistrant directement dans Zotero. Si le connecteur Zotero signale que Zotero est indisponible et tente d'enregistrer sur zotero.org, consultez la page [Zotero indisponible](./kb/connector_zotero_unavailable.md).

6. __Vérifiez les mises à jour du connecteur Zotero__ dans le volet Extensions de votre navigateur, au cas où une nouvelle version du connecteur serait disponible et que vous ne l'auriez pas encore reçue.

7. Assurez-vous que vous disposez d'une __version à jour de votre navigateur__. Voir la [configuration requise](https://www.zotero.org/support/system_requirements) pour connaître les versions de navigateur prises en charge.

8. Si vous enregistrez avec Zotero ouvert, __[vérifiez votre version de Zotero](./kb/zotero_version.md)__pour vous assurer que vous avez la dernière version disponible sur zotero.org. Nous ne pouvons dépanner les convertisseurs qui échouent que pour la version actuelle 6.0.x de Zotero

9. Vérifiez __[les problèmes connus des convertisseurs](https://www.zotero.org/support/known_translator_issues)__ pour voir si des problèmes ont été constatés sur le site à partir duquel vous essayez de sauvegarder.

10. Passez votre curseur de souris sur le bouton "Enregistrer dans Zotero". L'infobulle indique quel convertisseur Zotero utilisera pour la page que vous consultez (si applicable). Si le convertisseur affiché est incorrect, publiez le nom du site, une URL et le nom du convertisseur incorrectement détecté sur les forums Zotero. Notez que "Web Page", "Embedded Metadata", "DOI" et "COinS" sont des [convertisseurs génériques](./kb/default_translators.md), et que Zotero peut ne pas être en mesure d'enregistrer les métadonnées complètes ou les PDF de tous les sites sur lesquels ils apparaissent. Un problème avec un convertisseur primaire peut entraîner l'utilisation d'un convertisseur générique à la place.

11. Assurez-vous que les paramètres de confidentialité de votre navigateur ne __bloquent pas les cookies__. Certains sites vous demandent d'accepter des cookies tiers pour pouvoir enregistrer.

12. Si vous rencontrez fréquemment des problèmes pour faire fonctionner le connecteur Zotero sur plusieurs sites, il se peut que vous ayez un conflit d'extensions. Essayez de __désinstaller et de réinstaller le connecteur Zotero__. Si cela ne résout pas le problème, essayez de __désactiver toutes les extensions__, à l'exception du connecteur Zotero. Si cela résout le problème, réactivez les extensions une par une jusqu'à ce que vous trouviez le conflit, puis publiez le nom de l'extension à l'origine du problème dans les forums.

13. Dans de rares cas, si vous voyez une icône de page Web sur tous les sites ou si l'enregistrement échoue sur tous les sites, il peut y avoir un problème avec vos convertisseurs. Sélectionnez "__Réinitialiser les convertisseurs__" dans le volet avancé des préférences de Zotero (onglet "Fichiers et dossiers"), puis faites de même pour le connecteur Zotero (clic droit sur le bouton d'enregistrement → Options/Préférences → onglet Avancé). Cette étape n'est pas nécessaire en tant qu'étape de dépannage ordinaire.

14.     __Si aucune de ces solutions ne résout votre problème__, nous aurons besoin d'informations supplémentaires pour déboguer davantage votre problème. Veuillez créer un nouveau fil de discussion dans les [Forums Zotero](http://forums.zotero.org/) - ou utiliser votre fil de discussion existant si vous en avez déjà créé un - et fournir les éléments suivants :
    * L'URL exacte d'une page qui ne fonctionne pas (même si aucune ne fonctionne).
    * Un [Debug ID](./debug_output.md#connecteurs-zotero-firefox-chrome-et-safari) du connecteur Zotero pour le rechargement de la page et la tentative d'enregistrement ou, si vous ne recevez qu'une icône de page web, pour le rechargement de la page.
    * Ce qui est indiqué dans la fenêtre contextuelle lorsque vous essayez d'enregistrer (par exemple, "Enregistrer dans ma bibliothèque" ou "Enregistrer dans zotero.org").
    * Le nom du convertisseur de l'étape 10, le cas échéant.