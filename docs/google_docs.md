# Utiliser Zotero dans Google Docs

*Consulter cette page dans la documentation officielle de Zotero : [Using Zotero with Google Docs](https://www.zotero.org/support/google_docs) - dernière mise à jour de la traduction : 2023-04-06*
{ data-search-exclude }

Le puissant support Google Docs de Zotero vous permet d'ajouter facilement des citations et des bibliographies aux documents que vous créez dans Google Docs.

Vous pouvez rapidement rechercher des documents dans votre bibliothèque Zotero, ajouter des numéros de page et d'autres détails, et insérer des citations. Lorsque vous avez terminé, un simple clic suffit pour insérer une bibliographie mise en forme, à partir des citations présentes dans votre document. Zotero prend en charge les exigences complexes des styles bibliographiques telles que _Ibid._ et la désambiguïsation des noms. Il maintient vos citations et votre bibliographie à jour lorsque vous apportez des modifications aux références dans votre bibliothèque Zotero. Si vous devez changer de style bibliographique, vous pouvez facilement remettre en forme l'ensemble de votre document dans l'un des plus de 10'000 styles bibliographiques pris en charge par Zotero.

Le support de Google Docs fait partie du connecteur Zotero pour Chrome, Firefox, Edge et Safari et nécessite l'application Zotero pour fonctionner. 

Vous utilisez un autre système de traitement de texte? Zotero fonctionne aussi avec [Word](./word_processor_plugin_usage.md) et [LibreOffice](./libreoffice_writer_plugin_usage.md).

## Interface de citation

Le connecteur Zotero ajoute un nouveau menu "Zotero" dans l'interface de Google Docs :

![/google-docs-menu.png](./images/google-docs-menu.png)

Un bouton Zotero s'ajoute également à la barre d'outils pour l'ajout en un clic de citations :

![/google-docs-toolbar.png](./images/google-docs-toolbar.png)

Les options suivantes se trouvent dans le menu Zotero menu :

| Libellé  | Description |
| :--------------- |:-----|
| Add/Edit Citation   | Ajoutez une nouvelle citation ou modifiez une citation existante dans votre document, à l'emplacement du curseur. |
| Add/Edit Bibliography  |  Insérez une bibliographie à l'emplacement du curseur ou modifiez une bibliographie existante. |
| Document Preferences  | Ouvrez la fenêtre de préférences du document, par exemple pour changer de style bibliographique. |
| Refresh  | Actualisez toutes les citations et la bibliographie, en mettant à jour les métadonnées des documents modifiées dans votre bibliothèque Zotero. |
| Unlink Citations  |  Retirez les liens aux citations Zotero dans votre document, en supprimant les codes de champ. Cela empêche toute mise à jour automatique ultérieure des citations et de la bibliographie. Notez que la suppression des codes de champ est **irréversible**, et elle ne doit être effectuée que dans une copie finale de votre document. |

## Authentification

En interagissant avec la fonctionnalité Zotero pour la première fois dans un document, vous serez invité à authentifier le module avec votre compte Google. Veillez à sélectionner le compte Google que vous avez utilisé pour créer le document ou qui a reçu un accès en édition par le créateur du document. Cela n'a rien à voir avec un éventuel compte Zotero, qui n'est pas nécessaire pour utiliser Zotero ou l'intégration Google Docs de Zotero.

Une fois authentifié, vous pouvez commencer à insérer des citations à partir de vos bibliothèques Zotero.

## Citer

Vous pouvez commencer à citer avec Zotero en cliquant sur le bouton ![bouton_zotero_Google Docs](https://www.zotero.org/support/_media/zotero-z-16px-offline.png) ("Add/Edit Citation") dans la barre d'outils de Google Docs ou en sélectionnant "Add/Edit Citation" depuis le menu Zotero. Les deux feront apparaître la boîte de dialogue de citation.

Cette boîte de dialogue est utilisée pour sélectionner les documents dans votre bibliothèque Zotero, et créer une citation.

![boite dialogue citation](https://www.zotero.org/support/_media/word_integration/citation-dialog-select-5.png)

Commencez à taper une partie d'un titre, le nom de famille d'un ou plusieurs auteurs, et/ou une année dans la boîte de dialogue. Les documents correspondants apparaîtront instantanément sous la boîte de dialogue.

Les documents correspondants seront affichés pour chaque bibliothèque de votre base de données Zotero ("Ma bibliothèque" et tous les groupes dont vous faites partie). Les documents que vous avez déjà cités dans le document seront affichés en haut de la liste sous "Cité".

Sélectionnez un document en cliquant dessus ou en appuyant sur Entrée lorsqu'il est en surbrillance. Le document apparaîtra dans la boîte de dialogue dans une bulle ombrée. Appuyez de nouveau sur Entrée pour insérer la citation et fermer la boîte "Mise en forme rapide des citations".

Dans la boîte de dialogue "Mise en forme rapide des citations", vous pouvez cliquer sur la bulle d'un document cité, puis sur "Ouvrir dans Ma bibliothèque (ou le nom de la bibliothèque de groupe)" pour afficher le document dans Zotero. Les documents qui sont orphelins (non connectés à des documents de votre base de données Zotero) n'auront pas de bouton "Ouvrir dans Ma bibliothèque". Des documents orphelins peuvent exister s'ils ont été insérés par un collaborateur à partir de "Ma bibliothèque" ou d'un groupe auquel vous n'avez pas accès, ou encore si vous les avez supprimés de votre bibliothèque Zotero.

## Bibliographie

Cliquer sur l'entrée de menu “Add/Edit Bibliography” insère une bibliographie à l'emplacement du curseur.

Zotero met automatiquement à jour au fur et à mesure la bibliographie, en fonction des citations présentes dans votre texte. 

Dans les rares cas où vous souhaiteriez ajouter à la bibliographie des documents que vous n'avez pas cités dans votre texte, vous pouvez cliquer à nouveau sur le bouton “Add/Edit Bibliography”, ce qui ouvrira à nouveau l'éditeur de bibliographie, voir [ci-dessous](#modifier-la-bibliographie). Les modifications apportées manuellement à la bibliographie dans Word seront écrasées la prochaine fois que Zotero actualisera le document.

## Collaboration

Google Docs est conçu pour vous permettre de collaborer sur des documents, et l'intégration de Zotero n'est pas différente. Vous et vos coauteurs pouvez tous insérer et modifier des citations dans un document partagé, et vous n'avez même pas besoin d'être dans un groupe Zotero. Cependant, si vous prévoyez un projet collaboratif de grande envergure, nous vous recommandons d'utiliser une bibliothèque de groupe, qui non seulement facilite la collecte et la gestion des documents, mais permet également à tous les collaborateurs de modifier les métadonnées des documents cités (auteurs, titre, date de publication, etc.). Si une personne cite un document de sa bibliothèque personnelle, elle seule pourra mettre à jour les métadonnées de ce document.

Nous recommandons que toute personne apportant des modifications au document ait installé le connecteur Zotero. (L'application Zotero elle-même n'est nécessaire que pour insérer ou modifier des citations). Si quelqu'un coupe et colle une citation active sans le connecteur Zotero, la citation ne sera plus liée à Zotero et disparaîtra de la bibliographie, et la prochaine personne qui rafraîchira le document avec le connecteur Zotero recevra un avertissement concernant les citations non liées. Bien que les personnes qui n'ont pas le connecteur puissent théoriquement modifier les parties du document qui ne sont pas des citations, nous ne le recommandons pas en raison du risque de rupture accidentelle des liens des citations.

Lorsque vous travaillez en collaboration sur un document, vous et vos coauteurs devez éviter d'insérer ou de modifier des citations en même temps. Le connecteur Zotero a mis en place des mécanismes pour empêcher la corruption du document et des citations par l'édition simultanée de citations, mais en raison de limitations techniques, ils n'offrent pas une sécurité parfaite.

## Préférences du document

La fenêtre "Préférences du document" vous permet de contrôler certaines options propres à un document.

1.  Le [style bibliographique](./styles.md).
2.  La langue utilisée pour la mise en forme des citations et de la bibliographie.
3. Pour les styles qui abrègent les titres de revue (par exemple "Nature"), le recours ou non à la liste d'abréviations **MEDLINE** pour abréger les titres.
    -  Si cette option est sélectionnée (par défaut), le contenu du champ "Abrév. de revue" dans Zotero sera ignoré.
4. Si Zotero doit automatiquement mettre à jour les citations pour les désambiguïsations, les ibid et la numérotation, ou si la mise à jour doit être retardée jusqu'à un rafraîchissement manuel. Notez que si vous activez ce mode, Zotero insérera vos citations avec un fond gris pour indiquer que le texte de la citation n'est pas définitif. La citation sera finalisée et le fond gris sera supprimé lorsque vous cliquerez sur "Refresh" dans le menu Zotero.

![/document-preferences-5-0.png](https://www.zotero.org/support/_media/gdocs_document_preferences.png)

## Enregistrement en vue de la publication

Lorsque vous êtes prêt à soumettre votre document, utilisez File → "Make a copy..." et, dans le nouveau document, utilisez Zotero → "Unlink citations" pour convertir les citations et la bibliographie en texte brut. Vous pouvez ensuite télécharger ce second document (par exemple, en tant que PDF), tout en conservant les citations actives dans le document original au cas où vous auriez besoin d'apporter d'autres modifications. Zotero vous invitera à créer une copie si vous essayez de télécharger votre document original.

## Personnaliser les citations

Les citations peuvent être personnalisées de différentes manières. 

Si une citation est simplement incorrecte ou s'il manque des données, commencez par vous assurer que les métadonnées du document sont correctes et complètes dans votre bibliothèque Zotero. Cliquez ensuite sur Refresh dans le module Word pour mettre à jour votre fichier Word en prenant en compte les modifications effectuées dans votre bibliothèque Zotero.

D'autres personnalisations peuvent être effectuées via la boîte de dialogue de citation. Cliquez sur une citation existante, puis sur Add/edit citation pour ouvrir la boîte de dialogue de citation. Cliquez ensuite sur la bulle de la citation à modifier pour ouvrir la fenêtre des options de citation, où vous pouvez effectuer les modifications suivantes.

### Indiquer une page précise ou un autre localisateur

![/citation-dialog-affixes-5.png](./images/citation-dialog-affixes-5.png)

Dans certains cas, vous souhaitez citer une partie déterminée d'un document, par exemple une page précise, un intervalle de pages ou un volume. Cette informations additionnelle spécifique à une seule citation (par exemple "p. 4-7" dans la citation "Durand et al. 2001, p. 4-7") est appelé le "localisateur" (en anglais: "locator").

La fenêtre des options dispose d'une liste déroulante pour les différents types de localisateur ("Page" est la valeur par défaut), et d'une zone de texte dans laquelle vous pouvez entrer la valeur du localisateur ("4-7" par exemple). Pour mentionner un localisateur autre que ceux de cette liste ("Table" par exemple), utilisez le champ suffixe.

Vous pouvez également ajouter des numéros de page à partir du clavier lorsque vous insérez des citations. Recherchez un document, appuyez une fois sur Entrée pour l'ajouter à la boîte de dialogue de citation, puis, avant d'appuyer à nouveau sur Entrée pour l'insérer dans le document, tapez simplement "p.34" ou un équivalent, et le numéro de page sera ajouté à la citation.

### Préfixe et suffixe

Les zones de texte "Préfixe" et "Suffixe" vous permettent de spécifier le texte qui précède ou qui suit la citation automatiquement créée. Vous pourriez ainsi vouloir "cf. Tribe 1999, voir aussi..." au lieu de "Tribe 1999" .

Tout texte saisi dans les champs préfixe et suffixe peut être mis en forme avec les balises HTML &lt;i&gt; (pour l'italique), &lt;b&gt; (gras), &lt;sup&gt; (exposant) et &lt;sub&gt; (indice). Par exemple, si vous saisissez "&lt;i&gt;Cf&lt;/i&gt;. l'exemple classique", il sera affiché: "*Cf*. l'exemple classique".

Les préfixes et les suffixes peuvent être appliqués à chaque document d'une citation pour créer des citations complexes, par exemple : "(voir Smith 1776 pour l'exemple classique ; Marx 1867 pour une vue présente et alternative)". Pour modifier des citations, entrer du texte dans les champs préfixe et suffixe est toujours préférable à la saisie directe dans les champs Word. Les modifications manuelles empêchent la mise à jour automatique des citations par Zotero.

### Citations textuelles avec omission de l'auteur ("D'après Smith (1776)")

Avec les styles auteur-date, le nom des auteurs apparaît souvent dans le corps du texte et il est alors omis de la citation entre parenthèses, par exemple : "D'après Smith (1776), la division du travail est cruciale...". Pour omettre le nom des auteurs de la citation, cochez la case "Supprimer l'auteur", ce qui aura pour effet d'afficher la citation sous la forme "(1776)" au lieu de "(Smith, 1776)". Ecrivez le nom de l'auteur ("Smith") dans le texte normal de votre document. 

### Citations multiples

![/citation-dialog-select-multiple-5.png](./images/citation-dialog-select-multiple-5.png)

Pour créer une citation contenant plusieurs références citées (par exemple "\[2,4-6\]" pour les styles numériques ou "(Smith 1776, Schumpeter 1962)" pour les styles auteur-date), ajoutez les références l'une après l'autre dans la boîte "Mise en forme rapide des citations". Après avoir sélectionné le premier document, ne pressez pas la touche Entrée, mais tapez le nom de l'auteur, le titre ou l'année de la référence suivante.

![/citation-dialog-options-5.png](./images/citation-dialog-options-5.png)

Certains styles bibliographiques exigent que les documents d'une même citation soient classés soit alphabétiquement (par exemple "(Doe 2000, Grey 1994, Smith 2008)"), soient chronologiquement ("(Grey 1994, Doe 2000, Smith 2008)"). Zotero suivra automatiquement ces règles de tri.

-   Pour désactiver le tri automatique des références citées dans la citation, faites glisser les citations en les réordonnant dans la boîte "Mise en forme rapide des citations". Vous pouvez également cliquer sur l'icône "Z" à gauche de la boîte de dialogue et décocher l'option "Trier les sources automatiquement". *Cette option n'apparaît que pour les styles bibliographiques qui spécifient un ordre de tri pour les citations*. Pour restaurer le tri automatique, cochez à nouveau l'option "Trier les sources automatiquement".

### Basculer vers la "Vue classique"

Vous pouvez basculer vers la ["Vue classique"](./word_processor_plugin_usage_classic.md) en cliquant sur l'icône "Z" à gauche de la boîte de dialogue, puis en choisissant "Vue classique". Pour basculer de façon permanente vers la vue classique, cochez la case "Utiliser la fenêtre classique d'ajout de citation" de [l'onglet "Citer"](./cite.md) des [préférences](./preferences.md) de Zotero.

## Modifier la bibliographie

Au fur et à mesure que vous ajoutez et supprimez des citations dans le document, Zotero met automatiquement à jour la bibliographie pour refléter vos modifications. En général, c'est tout ce que vous avez à faire.

Dans de rares cas, cependant, vous pouvez vouloir ajouter des sources non citées à votre bibliographie (par exemple, des documents inclus dans une recension mais non cités dans le document) ou supprimer des documents cités dans le texte mais qui ne doivent pas être inclus dans la bibliographie (par exemple, des communications personnelles). Pour ce faire, cliquez à nouveau sur le l'option"Add/Edit Bibliography" pour ouvrir la fenêtre "Modifier la bibliographie". 

![/word_processor_edit_bibliography.png](./images/word_processor_edit_bibliography.png)

Vous pouvez alors utiliser les flèches pour ajouter ou retirer des documents.

Bien qu'il soit également possible de modifier le texte ou la mise en forme des références bibliographiques dans cette fenêtre, il est déconseillé de le faire. Les références éditées ici ne seront pas automatiquement mises à jour par Zotero si vous modifiez les données dans votre bibliothèque. Modifier les références dans cette fenêtre peut par ailleurs se révéler peu fiable ; plusieurs utilisateurs ont signalé que les modifications apportées ici ne persistent pas toujours lorsque Zotero indexe le document, entre autres problèmes.

Si vous avez besoin de modifier des entrées de votre bibliographie, il est préférable de le faire dans une dernière étape, juste avant de soumettre votre texte. Créez tout d'abord une copie de votre document. Utilisez, dans cette copie, utilisez l'option "Unlink Citations" pour déconnecter votre document de Zotero et convertir les citations et la bibliographie en texte normal. Enfin, apportez vos ajustements au texte de la bibliographie.

Ce processus peut être utilisé pour une variété de modifications mineures de la bibliographie, y compris :

-   Ajouter des astérisques \* avant les références incluses dans une revue ou une méta-analyse,
-   Appliquer une mise en forme au nom de certains auteurs (gras, italique ou majuscules),
-   Ajouter des annotations ou des commentaires à une entrée,
-   Ajouter des intitulés pour des sous-sections de la bibliographie (par exemple pour distinguer les sources primaires et secondaires)

**Remarque:** Corriger généralement la mise en forme d'un style doit être effectuée dans le [style bibliographique CSL](./styles.md), et non ici. Corriger les données des documents doit être effectué dans votre bibliothèque Zotero, et non ici.

## Commandes clavier

Le module Zotero pour Word peut être utilisé avec le clavier, pour une accessibilité accrue et une plus grande la rapidité d'utilisation.

-   Appuyez sur Ctrl-Command-C (Mac) ou Ctrl-Alt-C (Windows/Linux) pour insérer une citation. Vous pouvez configurer cela depuis le volet "Advanced" des préférences du connecteur Zotero.
-   Boîte de dialogue "Mise en forme rapide des citations"
    -   Utilisez les touches fléchées haut et bas pour naviguer dans les résultats de recherche. Appuyez sur Entrée pour sélectionner un document.
    -   Tapez "p.45-48" ou ":45-48" après une citation pour citer une page spécifique ou un intervalle de pages.
    -   Tapez "ibid" pour sélectionner automatiquement le dernier document cité. Cela fonctionne avec tous les styles de citation, indépendamment de l'utilisation effective d'"ibid" dans les citations. Si vous utilisez Zotero dans une langue autre que l'anglais, utilisez l'abréviation correspondant à ibid dans cette langue, par exemple "ebd." en allemand.
    -   Appuyez sur Ctrl/Cmd+↓ (flèche du bas) pour ouvrir la fenêtre des options pour la citation sélectionnée avec le curseur. Utilisez les touches Tab et Maj+tab pour naviguer entre les différents documents, utilisez les flèches haut/bas pour changer le type de localisateur dans la liste déroulante, et la barre d'espace pour cocher/décocher la case "Supprimer l'auteur".

## Limitations

Bien que nous ayons essayé de créer la même expérience que celle disponible dans Word et LibreOffice, il y a quelques limitations à connaître lorsque vous travaillez dans Google Docs.

* Comme indiqué ci-dessus, toute personne apportant des modifications au document doit avoir installé le connecteur Zotero. (L'application Zotero elle-même n'est nécessaire que pour insérer ou mettre à jour des citations.) Les citations qui sont coupées et collées sans le connecteur installé ne seront pas liées.
* Si vous faites glisser des citations dans le document, elles ne seront plus liées. Le copier-coller ne pose aucun problème tant que le connecteur Zotero est installé.
* Si quelqu'un consulte le document sans avoir installé le connecteur Zotero, ou si vous téléchargez le document au lieu d'en faire une copie et de délier les citations, les citations actives dans le document apparaîtront comme des liens menant à des URL telles que https://www.zotero.org/google-docs/?abc123.
* L'insertion et la modification des citations ralentissent considérablement à mesure que le nombre de citations augmente. Avec plus de 100 citations, la mise à jour d'une seule citation peut prendre jusqu'à 10 secondes. Pour les documents plus longs, il est donc préférable de désactiver les mises à jour automatiques des citations dans les préférences Zotero du document.
* Google Docs offre des possibilités limitées pour la mise en forme du texte. Les styles qui utilisent des polices en petites capitales n'utiliseront pas un vrai style de mise en forme en petites capitales dans Google Docs et reviendront à la police "Alegreya Sans SC". Les citations insérées avec la mise à jour automatique des citations désactivée seront insérées avec un fond gris au lieu d'être soulignées en pointillés comme dans Word et LibreOffice.

## Résolution des problèmes

### Le menu n'apparaît pas

Si rien n'apparaît lorsque vous cliquez sur le menu Zotero, ou si vous voyez une fine ligne grise, essayez de redémarrer Zotero et votre navigateur.

Si cela ne suffit pas, désactivez toutes les autres extensions du navigateur, rechargez Google Docs et réessayez. En particulier, l'extension Google Docs Offline a été signalée comme interférant avec l'intégration de Zotero dans Google Docs.

Si cela ne fonctionne toujours pas, essayez dans un nouveau profil de navigateur (par exemple, un nouveau profil Chrome) ou dans un autre navigateur.

### La boîte de dialogue de citation n'apparaît pas lorsque vous cliquez sur "Add/Edit citation"

Si vous pouvez ouvrir le menu Zotero mais que la boîte de dialogue de citation n'apparaît pas lorsque vous cliquez sur "Add/Edit citation", assurez-vous que cette boîte de dialogue n'est pas déjà ouverte et qu'elle n'apparaît pas derrière la fenêtre d'un autre navigateur ou de Zotero.

### Citations non liées

Voir [Pourquoi Zotero ne détecte-t-il pas mes citations existantes dans un Google Docs?](./kb/google_docs_citations_unlinked.md)

### “The Google account you selected does not have permission to edit this document”

Vous n'avez probablement pas sélectionné le bon compte Google. Reportez-vous à la rubrique [Authentification](#authentification). 

### Autres problèmes

Si vous rencontrez d'autres difficultés en citant dans Google Docs, indiquez-nous le sur le [forum Zotero](https://forums.zotero.org/). Fournissez un ["Debug ID"](./debug_output.md) du connecteur Zotero pour la situation suivante : recharger Google Docs et essayer d'effectuer l'action concernée.

Vous devez toujours effectuer le dépannage à l'aide d'une copie du document original, en utilisant File → "Make a copy... ". Si quelque chose ne fonctionne pas dans un document particulier, l'historique des versions du document peut vous permettre de revenir à une version antérieure. Certaines des [étapes de dépannage des documents endommagés](https://www.zotero.org/support/word_processor_plugin_troubleshooting#fixing_broken_documents) peuvent également être utiles dans Google Docs. 



