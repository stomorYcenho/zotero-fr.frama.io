# Notes

*Consulter cette page dans la documentation officielle de Zotero : [Notes](https://www.zotero.org/support/notes) - dernière mise à jour de la traduction : 2022-09-15*
{ data-search-exclude }

En plus des documents et des fichiers joints, vous pouvez également stocker dans votre bibliothèque Zotero des notes : des notes fille, qui sont attachées à un document spécifique, et des notes indépendantes. Les notes sont [synchronisées](./sync.md) avec les métadonnées des documents ; elles ne comptent pas dans votre [quota de stockage de fichiers Zotero](https://www.zotero.org/storage?id=storage).

### Notes fille

Pour créer une note fille, sélectionnez un document dans le volet central. Ensuite, cliquez sur le bouton "Nouvelle note" en haut du volet central (![note_add.png](./images/note_add.png)) et sélectionnez "Ajouter une note fille", ou allez dans l'onglet "Notes" du volet droit et cliquez sur le bouton "Ajouter". Vous pouvez également cliquer avec le bouton droit de la souris sur un document et sélectionner "Ajouter une note".

![/note_in_window.png](./images/note_in_window.png)

Une note sera créée en pièce jointe au document et elle apparaîtra également sous l'onglet "Notes". Un éditeur de notes s'affichera dans le panneau de droite. Vous pouvez créer une fenêtre dédiée à l'éditeur de notes en cliquant sur le bouton "Modifier dans une nouvelle fenêtre" en bas de l'éditeur. Le texte des notes est sauvegardé au fur et à mesure que vous le saisissez.

### Notes indépendantes

Les notes indépendantes fonctionnent de la même manière que les notes fille, mais ne sont pas directement liées à un document de votre bibliothèque. Les notes indépendantes apparaîtront à côté de tous les autres documents de votre bibliothèque. Pour créer une note indépendante, cliquez sur le bouton "Nouvelle note" (![note_add.png](./images/note_add.png)) et sélectionnez "Nouvelle note indépendante".

### Marqueurs et connexes

Comme tout autre document Zotero, les notes peuvent être avoir des [marqueurs](./collections_and_tags.md) et [être liées](./related.md) à d'autres documents. Ces deux fonctionnalités sont accessibles en bas de l'éditeur de note.

### Recherche dans les notes

Le contenu des notes peut être interrogé par la [recherche](./searching.md) générale de Zotero. Vous pouvez rechercher du texte à l'intérieur d'une note en utilisant le bouton "Trouver et remplacer" de l'éditeur de notes ou Ctrl/Cmd+F

### Images dans les notes

Vous pouvez intégrer des images dans des notes Zotero en les faisant glisser depuis votre système de fichiers ou, dans certains cas, en les collant. Les images intégrées se synchronisent entre les appareils et sont comptabilisées dans votre quota de stockage Zotero.
