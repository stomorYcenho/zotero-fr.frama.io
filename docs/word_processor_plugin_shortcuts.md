# Raccourcis clavier pour les modules de traitement de texte

*Consulter cette page dans la documentation officielle de Zotero : [Word Processor Plugin Shortcuts](https://www.zotero.org/support/word_processor_plugin_shortcuts) - dernière mise à jour de la traduction : 2022-12-20*
{ data-search-exclude }

Dans la plupart des traitements de texte, il est possible d'attribuer des raccourcis clavier aux différentes fonctions de la barre d'outils du module Zotero (c'est-à-dire ajouter une citation, modifier une citation, etc.) La manière de procéder dépend de votre traitement de texte.

## Word pour Windows

1. Cliquez sur Fichier -> Options -> Personnaliser le ruban.
2. Cliquez sur le bouton "Personnaliser" en bas de la fenêtre, à côté de "Raccourcis clavier".
3. Sélectionnez la catégorie "Macros" dans la case de gauche.
4. Identifiez les commandes Zotero dans la case de droite et sélectionnez-en une pour lui attribuer un raccourci clavier.
5. S'il existe déjà un raccourci, il apparaîtra dans la case "Touches actuelles".
6.  Pour en attribuer un, placez le curseur dans le champ "Nouvelle touche de raccourci".
7. Appuyez sur la combinaison de touches que vous souhaitez attribuer, par exemple appuyez sur Ctrl+Alt+A pour attribuer cette combinaison à l'ajout d'une nouvelle citation Zotero.
8. Si le champ "Affectées à" a pour valeur [non attribuée], vous pouvez utiliser ce raccourci sans conflit avec d'autres commandes.
9. Répétez le processus pour les autres commandes de Zotero.

Consultez l'article [Personnaliser les raccourcis clavier](https://support.microsoft.com/fr-fr/office/personnaliser-les-raccourcis-clavier-9a92343e-a781-4d5a-92f1-0f32e3ba5b4d) du support technique de Microsoft pour des indications plus longues et illustrées.

## Word pour Mac 2016 et plus récent

1. Ouvrez Outils → Personnaliser le clavier...
2. Sélectionnez la catégorie "Macros" dans la case située à gauche.
3.  Identifiez les commandes Zotero dans la case de droite et sélectionnez-en une pour lui attribuer un raccourci clavier.
4. Appuyez sur la combinaison de touches que vous souhaitez attribuer, par exemple appuyez sur Cmd+Alt+A pour attribuer cette combinaison à l'ajout d'une nouvelle citation Zotero.
5. S'il existe déjà un raccourci, il apparaîtra dans la zone "Touches actuelles".
6. Si le champ "Affectées à" a pour valeur [non attribuée], vous pouvez utiliser ce raccourci sans conflit avec d'autres commandes.
7. Répétez le processus pour les autres commandes de Zotero.

Voir [cette discussion du forum Zotero](https://forums.zotero.org/discussion/51204?page=1#Item_3) pour plus de détails.

## LibreOffice

1. Ouvrez Outils → Personnaliser...
2. Cliquez sur le raccourci que vous voulez utiliser, dans la liste des raccourcis utilisables affichée dans la partie supérieure de la boîte de dialogue.
3. Trouvez la macro Zotero appropriée dans la partie inférieure de la boîte de dialogue. Les macros se trouvent dans Macros LibreOffice → Mes Macros → Zotero → Zotero. Cliquez sur l'action souhaitée dans la liste (probablement ZoteroAddCitation).
4. Cliquez sur le bouton "Modifier".
5. Cliquez sur "OK" et profitez-en !

## Google Docs

Voir [la rubrique "Utiliser Zotero dans Google Docs" > "Commandes clavier"](./google_docs.md#commandes-clavier).
