# Préférences : Avancées

*Consulter cette page dans la documentation officielle de Zotero : [Preferences : Advanced](https://www.zotero.org/support/preferences/advanced) - dernière mise à jour de la traduction : 2023-01-06*
{ data-search-exclude }

Le volet "Avancées" comporte quatre onglets : "Générales", "Fichiers et dossiers", "Raccourcis clavier" et "Flux".

## Générales

![preferences\_advanced\_general.png](./images/preferences_advanced_general.png)

### Divers

- **Vérifier automatiquement la mise à jour des convertisseurs et des styles** : Permet à Zotero de mettre automatiquement à jour ses convertisseurs (pour détecter et enregistrer les données bibliographiques provenant de différents sites Web) et ses styles bibliographiques. Vous pouvez vérifier manuellement les mises à jour immédiatement en cliquant sur le bouton "Mettre à jour maintenant".
- **Signaler les convertisseurs défectueux** : Permet à Zotero d'informer ses développeurs lorsqu'un convertisseur ne parvient pas à enregistrer un document. Ces informations sont soumises de manière anonyme.


### Langue

Définit la langue de l'interface de Zotero.

### OpenURL

Vous pouvez spécifier ici le résolveur de liens utilisé par la fonctionnalité [Rechercher dans la bibliothèque](./locate.md#rechercher-dans-la-bibliotheque) de Zotero.

Si vous êtes sur le réseau d'une institution, vous pouvez cliquer sur le bouton "Chercher des résolveurs de liens". Si Zotero trouve un résolveur de liens appartenant à votre institution, vous pouvez le sélectionner dans le menu déroulant "Personnaliser...".

Vous pouvez également saisir manuellement une URL OpenURL dans le champ "Résolveur de liens". La plupart des résolveurs utilisent OpenURL version 1.0, mais la version 0.1 est toujours utilisée. Demandez plus d'informations à votre bibliothécaire ou consultez notre propre [liste de résolveurs de liens](https://www.zotero.org/support/locate/openurl_resolvers).

### Configuration avancée

Cliquez sur le bouton "Editeur de configuration" pour configurer les [préférences cachées de Zotero](https://www.zotero.org/support/hidden_prefs).

## Fichiers et dossiers

![preferences\_advanced\_files.png](./images/preferences_advanced_files.png)

### Répertoire de base pour les pièces jointes liées

*Si vous stockez les fichiers joints dans Zotero - fonctionnement par défaut - ce paramètre ne vous affecte pas. Il ne s'applique qu'aux fichiers* liés.

Ce paramètre vous permet d'accéder aux [fichiers liés](./attaching_files.md#fichiers-joints-et-fichiers-lies) sur plusieurs ordinateurs, même s'ils sont stockés à des emplacements différents sur chaque ordinateur. Vous devez définir comme répertoire de base le dossier dans lequel vous stockez les fichiers liés sur chaque ordinateur. Par exemple, si le dossier contenant vos fichiers liés se trouve à l'emplacement `/Users/Sarah/Dropbox/PDFs` sur votre ordinateur portable et à `C:\Users\Sarah\Dropbox\PDFs` sur votre ordinateur professionnel, définissez respectivement ces chemins comme répertoire de base sur chacune des machines. Si vous ajoutez un fichier lié dans le répertoire de base, Zotero enregistre un chemin relatif vers ce répertoire de base plutôt qu'un chemin absolu.

Notez que ce paramètre ne contrôle pas l'endroit où les fichiers sont stockés, mais seulement si les fichiers liés dans le dossier spécifié sont référencés par des chemins absolus ou relatifs. Si vous utilisez [le module complémentaire ZotFile](http://zotfile.com/) pour faciliter le flux de travail des fichiers liés, vous devez le configurer pour stocker les fichiers liés dans le répertoire de base que vous avez configuré. 

#### Restauration automatique des liens pour les fichiers liés

Si vous avez précédemment ajouté des fichiers liés sur un autre ordinateur sans avoir défini de répertoire de base et que vos fichiers se trouvent maintenant à un emplacement différent sur votre ordinateur actuel, vous pouvez simplement définir comme répertoire de base le répertoire souhaité sur votre ordinateur actuel (par exemple, `C:\Users\Sarah\Dropbox\PDFs`). Lorsque vous ouvrirez ensuite un fichier manquant ayant un chemin absolu (par exemple, `/Users/Sarah/Dropbox/PDFs/Smith - 2019 - Foo.pdf`), Zotero localisera automatiquement le fichier dans le répertoire de base que vous venez de définir et vous proposera de restaurer les liens pour tous les fichiers qui ont le même chemin de base (`/Users/Sarah/Dropbox/PDFs`).


### Emplacement du répertoire de données

Par défaut, Zotero stocke votre [répertoire de données](./zotero_data.md) (qui contient la base de données de votre bibliothèque, les fichiers joints et plusieurs autres fichiers) dans votre répertoire personnel d'utilisateur sur votre ordinateur. C'est le meilleur emplacement pour la plupart des utilisateurs, mais il est possible de le changer. Lorsque l'emplacement du répertoire de données a été changé, Zotero stocke dans ce nouvel emplacement les nouvelles données créées.

Notez toutefois que Zotero ne copiera pas les données existantes vers le nouvel emplacement. Si vous voulez conserver vos données, vous devez déplacer les fichiers manuellement. Cliquez sur le bouton "Ouvrir le répertoire de données" pour ouvrir le répertoire Zotero dans le navigateur de fichiers de votre ordinateur. Vous devrez déplacer l'ensemble du dossier "Zotero" (y compris "zotero.sqlite", "storage", "styles", et tous les autres fichiers) au nouvel emplacement.


#### Emplacements risqués pour votre répertoire de données

Plusieurs emplacements de répertoire de données sont potentiellement risqués et **susceptibles d'entraîner la corruption de la base de données ou même la perte de données**. Si vous utilisez l'une de ces configurations, assurez-vous de [sauvegarder vos données Zotero](./zotero_data.md) fréquemment .

-   **Dossiers synchronisés en ligne** : Stocker votre répertoire de données Zotero dans un dossier de synchronisation en ligne (par exemple, Dropbox, Google Drive, ou d'autres services de synchronisation de dossiers similaires) est extrêmement risqué et entraînera presque certainement une corruption de la base de données et potentiellement une perte de données. Le forum contient [de nombreuses](http://forums.zotero.org/discussion/13359/) [discussions](http://forums.zotero.org/discussion/27900/synching-to-dropbox/) [concernant](http://forums.zotero.org/discussion/6128/dropbox-and-zotero-15-case-conflicts/) [les difficultés](http://forums.zotero.org/discussion/24593/backing-up-a-large-database-without-corrupting-it/) auxquelles les utilisateurs ont eu à faire face avec des configurations basées sur Dropbox ou Google Drive. Consultez la rubrique [solutions alternatives de synchronisation](./sync.md#solutions-alternatives-de-synchronisation) pour de potentielles méthodes sûres d'utilisation de services de synchronisation en ligne pour synchroniser les fichiers joints Zotero.
-   **Lecteurs réseau** : Si vous stockez votre répertoire de données Zotero sur un lecteur réseau et que vous y accédez depuis plusieurs ordinateurs en même temps, vous risquez fort d'être confronté à une corruption de votre base de données. Par exemple, si vous laissez Zotero ouvert sur votre ordinateur portable, puis ouvrez la même base de données Zotero sur le lecteur réseau à partir de votre ordinateur professionnel, votre base de données sera probablement corrompue. Vous ne devez jamais utiliser un lecteur réseau pour permettre à plusieurs utilisateurs d'accéder à la même base de données Zotero sur différentes machines (utilisez [les groupes](./groups.md) ou [la synchronisation Zotero](./sync.md) pour cela).
-   **Machines virtuelles** : Comme pour les lecteurs réseau, il peut être dangereux d'utiliser le même fichier de base de données Zotero depuis une machine virtuelle et le système d'exploitation hôte de l'ordinateur (ou une autre machine virtuelle). Si on accède à la même base de données Zotero depuis deux emplacements en même temps (par exemple, si Zotero est ouvert à la fois sur la machine virtuelle et sur le système d'exploitation hôte), la corruption est probable. Si vous voulez utiliser Zotero dans une machine virtuelle, il est préférable de configurer un répertoire de données Zotero séparé dans la machine virtuelle et de le maintenir à jour en utilisant [la synchronisation Zotero](./sync.md).

### Maintenance de la base de données

-   **Vérifier l'intégrité de la base de données** : Cette fonction vérifie que votre base de données Zotero n'est pas corrompue. La corruption de la base de données est rare. Dans la plupart des cas, elle est causée par le stockage de votre répertoire de données à un [emplacement peu sûr](#emplacements-risques-pour-votre-repertoire-de-donnees). La vérification de l'intégrité de la base de données peut prendre beaucoup de temps si votre base de données est très volumineuse. Si votre base de données est corrompue, vous pouvez utiliser les [outils de réparation de base de données](https://www.zotero.org/utils/dbfix/) pour réparer la corruption.
-   **Réinitialiser les convertisseurs...** : Réinitialiser les convertisseurs web et d'import/export aux versions fournies avec l'application ou fournies comme mises à jour par les serveurs Zotero.
-   **Réinitialiser les styles...** : Réinitialiser les styles bibliographiques aux versions fournies avec l'application ou fournies comme mises à jour par les serveurs Zotero.

## Raccourcis clavier

![preferences\_advanced\_shortcuts.png](./images/preferences_advanced_shortcuts.png)

Cet onglet vous permet de modifier les raccourcis clavier par défaut de Zotero.

-   **Créer un nouveau document**: Crée un nouveau document vide dans la collection en cours.
-   **Créer une nouvelle note**: Crée une nouvelle note indépendante dans la collection en cours.
-   **Placer le curseur dans le volet de gauche (bibliothèques)**: Place le curseur dans le volet de gauche de Zotero (bibliothèques, collections et flux).
-   **Recherche rapide**: Place le curseur dans la boîte de [recherche rapide](./searching.md). `Ctrl/Cmd`+`F` aura le même effet.
-   **Copier dans le presse-papiers les documents sélectionnés, comme des citations**: Copie dans le presse-papiers les documents sélectionnés en tant que citations. (Selon le style, cela peut être long et détaillé ou, si le style exige des notes de bas de page, simplement un chiffre.)
-   **Copier dans le presse-papiers les documents sélectionnés, comme une bibliographie**: Copie dans le presse-papiers les documents sélectionnés sus la forme d'une bibliographie.
-   **Afficher / cacher le sélecteur de marqueurs** : Affiche / cache le sélecteur de marqueurs.
-   **Marquer tous les documents de flux comme lus / non lus**: Marque tous les documents dans le [flux](./feeds.md) sélectionné comme lus / non lus.

Toute modification apportée à cette page ne prendra effet qu'après le redémarrage de Zotero.

### Valeurs par défaut de Windows

<table>
<thead>
<tr class="header">
<th>Fonction</th>
<th>Commande</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Créer un nouveau document</td>
<td>Ctrl+Maj+N</td>
</tr>
<tr class="even">
<td>Créer une nouvelle note</td>
<td>Ctrl+Maj+O</td>
</tr>
<tr class="odd">
<td>Placer le curseur dans le volet de gauche (bibliothèques)</td>
<td>Ctrl+Maj+L</td>
</tr>
<tr class="even">
<td>Recherche rapide</td>
<td>Ctrl+Maj+K</td>
</tr>
<tr class="odd">
<td>Copier dans le presse-papiers les documents sélectionnés, comme des citations</td>
<td>Ctrl+Maj+A</td>
</tr>
<tr class="even">
<td>Copier dans le presse-papiers les documents sélectionnés, comme une bibliographie</td>
<td>Ctrl+Maj+C</td>
</tr>
<tr class="odd">
<td>Afficher / cacher le sélecteur de marqueurs</td>
<td>Ctrl+Maj+T</td>
</tr>
<tr class="even">
<td>Marquer tous les documents de flux comme lus / non lus</td>
<td>Ctrl+Maj+R</td>
</tr>
</tbody>
</table>

### Valeurs par défaut de Mac OS X 

<table>
<thead>
<tr class="header">
<th>Fonction</th>
<th>Commande</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Créer un nouveau document</td>
<td>Cmd+Maj+N</td>
</tr>
<tr class="even">
<td>Créer une nouvelle note</td>
<td>Cmd+Maj+O</td>
</tr>
<tr class="odd">
<td>Placer le curseur dans le volet de gauche (bibliothèques)</td>
<td>Cmd+Maj+L</td>
</tr>
<tr class="even">
<td>Recherche rapide</td>
<td>Cmd+Maj+K</td>
</tr>
<tr class="odd">
<td>Copier dans le presse-papiers les documents sélectionnés, comme des citations</td>
<td>Cmd+Maj+A</td>
</tr>
<tr class="even">
<td>Copier dans le presse-papiers les documents sélectionnés, comme une bibliographie</td>
<td>Cmd+Maj+C</td>
</tr>
<tr class="odd">
<td>Afficher / cacher le sélecteur de marqueurs</td>
<td>Cmd+Maj+T</td>
</tr>
<tr class="even">
<td>Marquer tous les documents de flux comme lus / non lus</td>
<td>Cmd+Maj+R</td>
</tr>
</tbody>
</table>

## Flux

![preferences\_advanced\_feeds.png](./images/preferences_advanced_feeds.png)

Cet onglet contient les préférences pour la fonctionnalité de Zotero [Flux RSS](./feeds.md).

-   **Classement** : Change l'ordre de classement des documents des plus récents ou des plus anciens en premier.
-   **Paramètres par défaut des flux** : Modifie la fréquence de mise à jour des flux et la durée pendant laquelle les documents lus et non lus sont conservés dans votre base de données avant d'être retirés.


