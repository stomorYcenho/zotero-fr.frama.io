
# Le lecteur PDF et l'éditeur de notes de Zotero

*Consulter cette page dans la documentation officielle de Zotero : [The Zotero PDF Reader and Note Editor](https://www.zotero.org/support/pdf_reader) - dernière mise à jour de la traduction : 2022-12-21*
{ data-search-exclude }

**Ceci est une ébauche de documentation pour le nouveau lecteur PDF et l'éditeur de notes dans Zotero 6. Voir l'[annonce de Zotero 6](https://www.zotero.org/blog/zotero-6/) pour plus d'informations.**

![pdf-reader.jpg](https://www.zotero.org/static/images/blog/6.0/pdf-reader.jpg)

## Ajout d'annotations aux notes

Vous pouvez ajouter des annotations aux notes de différentes manières. Les annotations ajoutées aux notes incluront automatiquement des liens vers la page PDF ainsi que des citations que vous pourrez ajouter ultérieurement à un document Word, LibreOffice ou Google Docs avec l'un des plugins pour traitement de texte.

### Dans le lecteur PDF

Vous pouvez facilement ajouter des annotations aux notes directement depuis le lecteur PDF.

Tout d'abord, utilisez le bouton Notes dans le coin supérieur droit pour ouvrir le volet Notes, où vous pouvez créer une nouvelle note ou ouvrir une note existante.

Pour créer une nouvelle note à partir de toutes les annotations du PDF actuel, cliquez sur l'un des boutons "+" et sélectionnez "Ajouter une note fille à partir des annotations" ou "Ajouter une note indépendante à partir des annotations".

Si vous avez déjà une note ouverte dans le volet Notes, vous pouvez faire glisser des annotations individuelles à partir du PDF ou de l'onglet Annotations dans la barre latérale gauche pendant la saisie votre note. Vous pouvez également sélectionner une ou plusieurs annotations dans le PDF ou dans l'onglet Annotations dans la barre latérale gauche, faire un clic droit et sélectionner "Ajouter à la note".

Vous pouvez également faire glisser les annotations du lecteur PDF vers une note ouverte dans une fenêtre distincte.

Si vous êtes sûr-e de ne pas utiliser une citation plus d'une fois, il est également possible d'ajouter des citations aux notes Zotero sans créer d'annotation au préalable. Il suffit de sélectionner le texte dans le PDF et de le faire glisser vers une note Zotero ouverte.

### Dans la liste des documents

Vous pouvez créer une note enfant à partir de toutes les annotations d'un PDF en faisant un clic droit sur le document parent dans la liste des documents, et en choisissant "Ajouter une note depuis les annotations".

Vous pouvez également créer une note indépendante à partir des annotations de plusieurs documents en sélectionnant les documents parents, en cliquant avec le bouton droit de la souris et en choisissant "Créer une note depuis les annotations". 


![add-note-from-annotations.png](https://www.zotero.org/support/lib/exe/fetch.php?w=310&h=300&tok=b2acde&media=https%3A%2F%2Fwww.zotero.org%2Fstatic%2Fimages%2Fblog%2F6.0%2Fadd-note-from-annotations.png)

## Personnalisation des annotations dans les notes

Vous pouvez personnaliser la façon dont les annotations sont ajoutées aux notes en utilisant des [modèles de note](./note_templates.md).

## Travailler avec les annotations dans les notes

### Afficher une annotation dans son contexte

Lorsque vous ajoutez une annotation à une note, cela ajoute par défaut une annotation et une citation.

Pour afficher l'annotation dans son contexte, cliquez sur l'annotation puis sur "Montrer dans la page" dans la fenêtre contextuelle qui apparaît. Cela ouvrira le PDF original soit dans le lecteur PDF intégré, soit dans un lecteur PDF externe, si vous en avez configuré un dans le volet [le volet "Générales"](./general.md) des préférences. Lorsque cela est possible, Zotero ouvrira le PDF à la page où l'annotation a été faite - cela fonctionne pour le lecteur PDF intégré et la plupart des lecteurs PDF externes populaires (par exemple Acrobat sur Windows, Preview sur macOS, evince sur Linux). Si Zotero ne s'ouvre pas à la page appropriée dans votre lecteur PDF externe, faites-le nous via le Forum Zotero.

Pour afficher la référence associée dans votre bibliothèque, cliquez sur la citation dans la note et sélectionnez "Montrer le document".

### Afficher les couleurs des annotations

Pour afficher les couleurs des annotations dans votre note, cliquez sur le bouton "..." dans le coin supérieur droit de l'éditeur de note et sélectionnez "Afficher les couleurs des annotations". Vous pouvez supprimer les couleurs plus tard avec "Masquer les couleurs des annotations".

### Masquer ou afficher les citations

Pour masquer ou afficher toutes les citations dans une note, cliquez sur le bouton "..." dans le coin supérieur droit de l'éditeur de note et sélectionnez "Masquer les citations des annotations" ou "Afficher les citations des annotations".

Vous pouvez masquer une citation individuelle en cliquant dessus dans l'éditeur et en sélectionnant "Masquer la citation" dans le popup, ou alors supprimer complètement la citation avec le même menu. Vous pouvez rajouter la citation à tout moment en cliquant sur le texte surligné et en sélectionnant "Ajouter une citation" dans le menu contextuel ou en utilisant "Afficher les citations des annotations" dans le menu "...".

## Utilisation d'un lecteur PDF externe

Si vous préférez ouvrir les PDFs dans un lecteur externe, vous pouvez en choisir un dans le volet [le volet "Générales"](./general.md) des préférences de Zotero.

Pour ouvrir un seul PDF dans un lecteur externe, faites un clic droit sur le document et choisissez "Localiser le fichier", puis ouvrez le PDF depuis le gestionnaire de fichiers de votre système d'exploitation. Certaines modifications que vous effectuez dans des lecteurs PDF externes peuvent causer des problèmes dans le lecteur intégré - par exemple, l'ajout, la suppression ou la réorganisation de pages fera apparaître les annotations sur la mauvaise page.

Notez que les annotations créées dans le lecteur PDF intégré sont stockées dans la base de données Zotero ; elles ne seront donc pas visibles dans les lecteurs PDF externes, sauf si vous exportez un PDF avec des annotations intégrées. Voir [Annotations dans la base de données](./kb/annotations_in_database.md) pour plus d'informations.
