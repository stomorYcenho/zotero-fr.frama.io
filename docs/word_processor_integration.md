# Modules pour logiciel de traitement de texte

*Consulter cette page dans la documentation officielle de Zotero : [Word Processor Plugins](https://www.zotero.org/support/word_processor_integration) - dernière mise à jour de la traduction : 2022-06-08*
{ data-search-exclude }

Parmi les différentes façons de [créer des bibliographies](./creating_bibliographies.md) (ainsi que des citations dans le texte et des notes de bas de page) automatiquement, les modules pour logiciel de traitement de texte, faciles à utiliser, sont les plus puissants. Ces modules, disponibles pour Microsoft Word, LibreOffice et Google Docs, créent des bibliographies dynamiques : insérez une nouvelle citation dans le texte de votre manuscrit, et la bibliographie sera automatiquement mise à jour pour inclure le document cité. Corrigez le titre d'une rférence dans votre bibliothèque Zotero, et d'un simple clic le changement sera répercuté dans vos documents.

Consultez les liens suivants pour débuter avec ces modules.

-    [Utiliser le module Zotero pour Word](./word_processor_plugin_usage.md)
-    [Utiliser le module Zotero pour LibreOffice](./libreoffice_writer_plugin_usage.md)
-    [Utiliser Zotero avec Google Docs](./google_docs.md)
-    [Dépannage des modules pour logiciel de traitement de texte](https://www.zotero.org/support/word_processor_plugin_troubleshooting)

[Des extensions tierces](https://www.zotero.org/support/plugins#word_processor_and_writing_integration) sont également disponibles pour intégrer Zotero à d'autres systèmes de traitement de texte et d'écriture.

## Installer les modules pour logiciel de traitement de texte

Les modules pour logiciel de traitement de texte sont fournis avec Zotero et devraient être installés automatiquement pour chaque [logiciel de traitement de texte pris en charge (page en anglais)](https://www.zotero.org/support/system_requirements#word_processor_plugins) installé sur votre ordinateur lorsque vous démarrez Zotero pour la première fois.

Vous pouvez réinstaller ultérieurement ces modules depuis l'onglet Citer -&gt; Traitements de texte dans les préférences de Zotero. Si vous rencontrez des difficultés, consultez les pages [Installer manuellement les modules pour logiciel de traitement de texte de Zotero](./word_processor_plugin_manual_installation.md) ou [Dépannage des modules pour logiciel de traitement de texte](https://www.zotero.org/support/word_processor_plugin_troubleshooting).


