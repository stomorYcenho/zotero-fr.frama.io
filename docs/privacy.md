# Politique de confidentialité de Zotero

*Consulter cette page dans la documentation officielle de Zotero : [Zotero Privacy Policy](https://www.zotero.org/support/privacy) - dernière mise à jour de la traduction : 2022-11-22*
{ data-search-exclude }

## Vue d’ensemble

Zotero est un projet open-source qui s’engage à fournir le meilleur outil pour gérer vos recherches. Notre philosophie est que ce que vous mettez dans Zotero vous appartient, et l’un des principes fondateurs est de nous assurer que vous restez maître de vos données et que vous pouvez les partager comme vous le souhaitez - ou choisir de ne pas les partager du tout. 

**Nous sommes une organisation indépendante à but non lucratif et n’avons aucun intérêt financier dans vos informations privées.** Nous finançons notre développement en offrant un espace de stockage en ligne supplémentaire aux personnes qui trouvent le logiciel utile, et non en vendant des données.

## Données que nous collectons

Zotero est conçu comme un programme local qui enregistre les données sur votre propre ordinateur par défaut, et ne nécessite pas le partage de données avec nous pour être utilisable. Cependant certaines des fonctions avancées de Zotero nécessitent que vous nous fournissiez des informations.

* Nous recueillons les informations que vous fournissez volontairement (par exemple, votre nom et votre adresse mail) si vous créez un compte Zotero et utilisez les services optionnels de Zotero. Zotero peut être utilisé sans créer de compte.
* Nous recueillons les données bibliographiques que vous mettez en ligne si vous choisissez de synchroniser votre bibliothèque avec les serveurs de Zotero. La synchronisation est entièrement facultative et est désactivée par défaut.
* Nous collectons les fichiers joints que vous mettez en ligne si vous choisissez d’utiliser la synchronisation des fichiers de Zotero. Celle-ci est facultative, et vous pouvez choisir de synchroniser les fichiers de votre bibliothèque personnelle en utilisant un serveur webDAV à la place des serveurs Zotero.
* Nous conservons un enregistrement des dernières adresses IP utilisées pour accéder à votre bibliothèque synchronisée, afin de vous permettre de vérifier la sécurité de votre compte et de vous permettre de [révoquer l’accès](https://www.zotero.org/settings/keys) dans vos paramètres Zotero.
* Nous enregistrons les visites sur notre site web, y compris l’adresse IP et le navigateur, afin d’éviter les abus et de diagnostiquer les problèmes techniques. Nous conservons ces journaux d’accès pendant 90 jours.
* Nous enregistrons les requêtes adressées à nos serveurs par Zotero ou un logiciel tiers, y compris l’adresse IP et les informations relatives au client, afin d’éviter les abus et de diagnostiquer les problèmes techniques et d’évaluer l’utilisation. Nous conservons ces journaux pendant une durée maximale de 90 jours. Vous pouvez [refuser toutes les demandes adressées à nos serveurs](#désactivation-des-demandes-automatiques).
* Nous recueillons les rapports d’erreurs que vous soumettez. Voir [Interactions avec le support](#support-interactions).
* Si vous tentez d’enregistrer quelque chose sur Zotero et que l’enregistrement échoue, nous recueillons des informations sur l’échec, notamment l’URL, le navigateur et sa version, afin de pouvoir résoudre plus rapidement les problèmes de compatibilité du site. Nous conservons ces informations pendant une semaine au maximum. Aucune autre information d’identification personnelle (par exemple le nom d’utilisateur ou l’adresse IP) n’est stockée, et les rapports ne sont généralement consultés que agrégés. Ce rapport d’erreur peut être désactivé dans les préférences de Zotero et  du connecteur Zotero.

## Statistiques de la bibliothèques

Zotero anonymise et agrège les données synchronisées des bibliothèques des utilisateurs et des groupes pour générer des statistiques sur le lectorat. Ces données anonymes et agrégées ne comprennent que les métadonnées accessibles publiquement (par exemple le titre de la publication et l’auteur). Ces données ne sont jamais vendues ou mises à disposition sous d’autres formes que celles proposées au public. Nous pouvons également utiliser les informations anonymes et agrégées sur les utilisateurs pour faire des audits, de la recherche et de l’analyse afin d’administrer et d’améliorer les services Zotero.

##  Sécurité des données

Voir [Sécurité des données Zotero](./security.md).

## Désactivation des demandes automatiques

Vous pouvez désactiver toutes les communications automatiques avec les serveurs de Zotero à partir des préférences de Zotero et du connecteur Zotero :

* **Synchronisation** : Préférences de synchronisation → laisser non configuré ou désactiver la synchronisation automatique.
* **Récupération automatique des métadonnées des PDF** : Préférences générales → désactiver "Récupérer automatiquement les métadonnées des PDF".
    * Nous n'enregistrons aucune information sur le contenu des récupérations de métadonnées de PDF.
* **Récupération des PDF en libre accès** : Préférences générales → désactiver "Joindre automatiquement les fichiers PDF associés lors de l’enregistrement d’un document".
    * Si un PDF ne peut pas être enregistré pour un document ayant un DOI, Zotero enverra le DOI aux serveurs de Zotero pour vérifier les versions en accès libre. Nous n'enregistrons pas le contenu de ces demandes. Si vous désactivez cette préférence, l'enregistrement automatique des pièces jointes sera désactivé.
* **Signalement des traducteurs de sites cassés** : désactivez "Signalez les convertisseurs défectueux" dans les préférences avancées de Zotero et du connecteur Zotero.
* **Vérification des mises à jour des convertisseurs et des styles** : Préférences avancées → désactiver "Vérifier automatiquement les mises à jour des convertisseurs et des styles".
* **Vérification des mises à jour de Zotero** : Préférences avancées → Éditeur de configuration → définir `app.update.auto` sur false.
    * La vérification automatique des mises à jour est fortement recommandée pour des raisons de sécurité et de stabilité.
* **Vérification des articles rétractés** : Préférences avancées → Éditeur de configuration → définir `retractions.enabled` sur false.
    * Les vérifications de rétraction sont effectuées [sans partager](https://www.zotero.org/blog/retracted-item-notifications/#designed-for-privacy) les documents que vous avez dans votre base de données.
* **Vérification de l'authentification par proxy** : Préférences avancées → Éditeur de configuration → définir `extensions.zotero.triggerProxyAuthentication` sur false.
    * Au démarrage de Zotero, des requêtes HEAD sont effectuées vers un fichier de test sur Amazon S3 et vers des sites Web d'éditeurs sélectionnés (contrôlés par `extensions.zotero.proxyAuthenticationURLs`) pour déclencher une invite d'authentification par proxy si et seulement si Zotero détecte qu'un proxy est nécessaire pour se connecter à Internet. Si vous désactivez cette option et que l’identification à un proxy est nécessaire, les connexions réseaux de Zotero échoueront.

Si la synchronisation automatique ou les mises à jour automatiques des convertisseurs et des styles sont activées, Zotero maintiendra une connexion persistante aux serveurs Zotero lorsqu’il est ouvert afin de fournir des mises à jour immédiates. Vous pouvez désactiver cette connexion en désactivant ces deux options ou en définissant `extensions.zotero.streaming.enabled` sur false dans l’éditeur de configuration.

Si vous utilisez le connecteur Zotero sans que Zotero soit ouvert, le connecteur fera une demande une fois par jour aux serveurs de Zotero pour obtenir des informations sur les convertisseurs de sites disponibles. Il téléchargera ensuite les convertisseurs des sites que vous visitez. Par exemple, si vous chargez un article du New York Times, le connecteur téléchargera le convertisseur du New York Times de Zotero et le mettra en cache. Si Zotero n’a pas de convertisseur pour un site spécifique, aucune demande ne sera faite. Aucune information sur les pages spécifiques que vous visitez n’est transmise, et aucune demande supplémentaire ne sera faite pour le même convertisseur jusqu’à ce que vous redémarriez votre navigateur ou que le traducteur soit mis à jour. Vous pouvez éviter ces demandes en gardant Zotero ouvert pendant que vous naviguez sur le Web. 

## Avertissements concernant les autorisations

Lors de l’utilisation de plateformes tierces, nous demandons les autorisations les plus restreintes possibles tout en permettant à Zotero de remplir les fonctionnalités prévues. Dans certains cas, les autorisations nécessaires peuvent sembler un peu effrayantes, nous voulons donc expliquer pourquoi elles sont nécessaires.

### Connecteur Zotero

Lors de l’installation du connecteur Zotero, votre navigateur vous prévient que l’extension peut "Lire et modifier toutes vos données sur les sites Web que vous visitez" (ou similaire). Il s’agit de l’autorisation standard exigée par les extensions de navigateur qui s’exécutent sur toutes les pages. Zotero l’utilise pour déterminer le contenu qu’il peut enregistrer sur une page donnée et mettre à jour en conséquence le bouton d’enregistrement, ainsi que pour fournir des fonctionnalités avancées telles que la redirection automatique vers un proxy et l’importation automatique de RIS / BibTeX. Aucune donnée n’est stockée, sauf lorsque vous choisissez d’enregistrer une page dans votre bibliothèque Zotero locale ou en ligne.

### Intégration de Google Docs

Lorsque vous utilisez l’intégration de Google Docs pour la première fois, Google vous demande d’accorder à l’intégration Google Docs de Zotero l’autorisations de "Voir, modifier, créer et supprimer tous vos documents Google Docs". L’extension a besoin de cette autorisation pour insérer des citations dans vos documents. L’extension ne fait rien d’autre avec le contenu de vos documents et n’accède pas aux documents autres que ceux sur lesquels il est déclenché. L’intégration fonctionne entièrement en local sur votre ordinateur, donc même lorsque vous déclenchez l’extension sur un document donné, rien n’est envoyé aux serveurs de Zotero.

## Achats de stockage en ligne

Au moment de l’achat, nous envoyons deux données personnelles à notre processeur de paiement :
* Votre nom d’utilisateur Zotero, afin d’associer votre paiement à votre compte Zotero,
* Votre adresse électronique, afin que le processeur de paiement puisse vous envoyer un reçu de paiement.

Zotero ne collecte ni ne stocke les informations relatives à votre carte de crédit, à votre compte bancaire ou à tout autre paiement. Lorsque vous saisissez votre nom complet, votre adresse de facturation et vos informations bancaires pour effectuer un achat sur Zotero, ces informations sont transmises directement à notre processeur de paiement et ne transitent jamais sur les serveurs de Zotero.

## Interactions avec le support

La plus grande partie du support Zotero a lieu dans les forums publics de Zotero. Si vous souhaitez supprimer les messages que vous avez publiés dans les forums, vous pouvez le faire vous-mêmes à tout moment, mais nous vous encourageons à laisser vos messages en ligne pour le bénéfice des autres utilisateurs. Si vous préférez que vos messages apparaissent sous un autre nom, vous pouvez modifier votre nom d’utilisateur dans les paramètres de votre compte.

Il peut vous être demandé de soumettre un rapport d’erreur ou le résultat d’un débogage. Ces rapports contiennent des informations techniques sur votre ordinateur, comme votre système d’exploitation et les extensions de navigateur installées, et peuvent inclure des informations personnelles accessoires comme les URL des sites que vous avez visités avant ou pendant la génération du rapport. Vous pouvez vérifier le résultat de ces rapports avant de les soumettre. Nous ne stockons aucune donnée personnelle (nom d’utilisateur, adresse IP) qui relie le rapport à vous, et nous ne regardons généralement pas les rapports, sauf s’ils sont référencés par un Report ID ou un Debug ID dans les forums de Zotero. Les rapports sont conservés pendant un an au maximum.

Si vous nous envoyez un mail, nous recueillons votre adresse électronique et toute autre information que vous fournissez, et nous pouvons stocker vos messages indéfiniment afin de fournir un contexte pour toute demande d’assistance future que vous ferez. 

## Services tiers utilisés

* Toutes les données des serveurs Zotero sont stockées aux États-Unis, dans Amazon Web Services. Voir [Sécurité des données de Zotero](./security.md) pour plus d’informations.
* Certaines opérations que vous effectuez dans Zotero peuvent déclencher des requêtes auprès de services publics tiers tels que Crossref ou la Library of Congress pour la récupération des métadonnées. Ces tiers peuvent enregistrer votre adresse IP et vos termes de recherches (par exemple DOI ou ISBN) conformément à leur politique de confidentialité, mais aucune autre information d’identification n’est fournie.
* Lorsque vous enregistrez ou mettez à jour des documents dans Zotero, Zotero peut, en fonction de vos paramètres, se connecter aux sites associés pour télécharger des métadonnées ou enregistrer des PDF ou des instantanés. Cela équivaut à charger ces sites dans votre navigateur, et les mêmes implications en matière de confidentialité s’appliquent. Voir [Zotero et les pare-feu])() pour les conditions d’accès.
* Certaines polices d’écriture présentes sur les sites de Zotero sont sous licence myfonts.com. Afin de vérifier le respect de cette licence par Zotero, myfonts.com collecte votre adresse IP et l’URL de la page web de Zotero à laquelle vous avez accédé.
* Lorsqu’un compte est enregistré, nous utilisons Google reCAPTCHA pour vérifier qu’il ne s’agit pas d’une tentative d’enregistrement automatique.
* Le traitement des paiements est assuré par Stripe.
* Lorsque les acheteurs ne peuvent utiliser Stripe, nous traitons les paiements avec Paypal.

## Suppression de vos données

Vous pouvez supprimer votre compte Zotero pour effacer les informations que vous avez volontairement fournies lors de votre inscription et pour effacer les données de vos bibliothèques Zotero si vous avez choisi d’utiliser la synchronisation sur les serveurs de Zotero. Pour supprimer votre compte, allez dans [vos paramètres Zotero](https://www.zotero.org/settings) et cliquez sur "Permanently Delete Account".

## Sauvegarde des données

Nous effectuons régulièrement des sauvegardes automatisées pour nous protéger contre la perte accidentelle des données des utilisateurs. Ces sauvegardes sont destinées à la reprise après sinistre et ne seront accessibles qu’en cas de perte importante de données. Les sauvegardes peuvent être conservée jusqu’à 6 mois.

## Réquisitions judiciaires

Nous pouvons être légalement tenus de répondre à des demandes de données provenant de services de police ou d’agences gouvernementales.

## Modifications

Nous pouvons mettre à jour notre politique de confidentialité au fil du temps. Des informations actualisées, y compris les détails des nouvelles fonctionnalités, seront toujours disponibles sur cette page.

## Questions

Si vous avez des questions ou des préoccupations concernant la politique de confidentialité de Zotero, merci de nous en faire part dans le [forum de Zotero](https://forums.zotero.org) ou par mail à [privacy@zotero.org](mailto:privacy@zotero.org)
