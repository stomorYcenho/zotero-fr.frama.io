# Convertisseurs par défaut

*Consulter cette page dans la documentation officielle de Zotero : [Default translators](https://www.zotero.org/support/kb/default_translators) - dernière mise à jour de la traduction : 2023-04-06*
{ data-search-exclude }

Zotero dispose de quatre convertisseurs qui tentent de trouver des données bibliographiques utilisables sur des pages qui ne sont reconnues par aucun des convertisseurs de site plus spécifiques. Vous pouvez savoir quel convertisseur a détecté les données bibliographiques sur une page en plaçant le curseur sur le bouton du connecteur Zotero dans la barre d'outils de votre navigateur ; le nom du convertisseur apparaît sous forme d'info-bulle. Un nom similaire est généralement enregistré dans le champ « Catalogue de bibl. » des documents créés dans votre bibliothèque.

* DOI. Zotero tente de détecter un ou plusieurs morceaux de texte qui pourraient être des DOI. Le convertisseur DOI fournit un support assez passable pour de nombreuses bases de données académiques et sites qui n'ont pas de convertisseur Zotero dédié. Les métadonnées des DOI sont récupérées via [CrossRef](http://www.crossref.org/) ; les données n'incluent jamais le texte intégral ni le résumé. Parfois, les enregistrements basés sur DOI échouent en raison de données incomplètes dans la base de données CrossRef.
* [COinS](https://www.zotero.org/support/dev/exposing_metadata/coins) peut inclure des informations sur un nombre limité de types de documents. Il ne prend jamais en charge le texte intégral.
* Les métadonnées intégrées et unAPI *peuvent* prendre en charge tous les types de documents et tous les champs de Zotero, ainsi que les pièces jointes en texte intégral. Les métadonnées intégrées, cependant, détectent souvent des métadonnées minimales sur les pages Web, et les documents qu'elles enregistrent à partir de ces pages ne sont souvent pas très utiles.

Pour plus d'informations sur la façon dont Zotero utilise les convertisseurs par défaut, consultez la [documentation développeur (en anglais)](https://www.zotero.org/support/dev/exposing_metadata#using_an_open_standard_for_exposing_metadata).
