# Comment importer une bibliothèque Mendeley dans Zotero ?

*Consulter cette page dans la documentation officielle de Zotero : [How do I import a Mendeley library into Zotero?](https://www.zotero.org/support/kb/mendeley_import) - dernière mise à jour de la traduction : 2023-01-13*
{ data-search-exclude }

Zotero peut importer directement toutes les données, y compris la structure complète des dossiers, d'une bibliothèque Mendeley en ligne.

Pour importer votre bibliothèque Mendeley, suivez ces étapes :

1. Assurez-vous que toutes les données et tous les fichiers ont été synchronisés avec les serveurs de Mendeley.
    * Si vous utilisez **Mendeley Desktop**, vérifiez vos paramètres de synchronisation pour vous assurer que les données et les fichiers sont synchronisés, et assurez-vous que vous pouvez ouvrir les PDF dans votre bibliothèque Mendeley en ligne.
    * Si vous utilisez **Mendeley Reference Manager**, vos données et fichiers sont déjà tous en ligne.
2. Assurez-vous que vous exécutez au moins Zotero 6, qui contient la dernière version de l'importateur ainsi qu'un [nouveau lecteur PDF](https://www.zotero.org/blog/zotero-6) capable d'afficher les annotations PDF importées de Mendeley.
3. Allez dans Fichier → Importer dans Zotero et choisissez l'option "Mendeley Reference manager (import en ligne)".

Il vous sera demandé de vous connecter à Mendeley pour permettre à Zotero d'effectuer l'importation. Votre mot de passe Mendeley n'est jamais vu ni stocké.

## Méthode alternative

Si, pour une raison quelconque, vous n'êtes pas en mesure d'effectuer une importation directe en ligne, il est possible d'importer à partir d'une base de données Mendeley locale en installant une ancienne version de Mendeley Desktop datant d'avant que Mendeley ne commence à [chiffrer](#chiffrement-de-la-base-de-données-mendeley)) la base de données locale. Voir les [instructions d'importation locale](https://www.zotero.org/support/kb/mendeley_local_import) pour plus d'informations.

## Utilisation des citations de Mendeley

Les plugins Word et LibreOffice de Zotero peuvent lire les citations créées par Mendeley Desktop et les relier automatiquement aux documents Mendeley importés dans votre bibliothèque Zotero, afin que vous puissiez continuer à utiliser les mêmes documents avec Zotero.

Avant Zotero 6.0.19 (publié en décembre 2022), Zotero pouvait lire et utiliser les citations Mendeley, mais elles n'étaient pas liées aux documents importés dans votre bibliothèque Zotero. Si vous avez importé votre bibliothèque Mendeley en utilisant Zotero 6.0.18 ou une version antérieure, vous devrez répéter le processus d'importation une fois en utilisant Zotero 6.0.19 ou une version ultérieure. Il vous suffit de sélectionner "Relier les citations de Mendeley Desktop" lors du démarrage de l'importateur (si vous avez déjà importé des citations en utilisant Zotero 6.0.19 ou une version ultérieure, cette option n'apparaîtra plus).

Les citations créées avec Mendeley Cite ne sont pas lisibles par Zotero.

## Problèmes connus

Il y a quelques problèmes à connaître lors de l'importation à partir de Mendeley.

* Si votre compte Mendeley nécessite des informations d'identification institutionnelles pour se connecter, vous devrez peut-être créer un compte Mendeley distinct et le connecter au compte institutionnel, puis utiliser le compte personnel pour vous connecter. Voir [Comment utiliser les informations d'identification institutionnelles (Shibboleth) avec Mendeley](https://service.elsevier.com/app/answers/detail/a_id/33535/supporthub/mendeley/p/16075/) ? sur le site d'assistance de Mendeley pour plus d'informations.
* Il n'est pas possible d'importer directement des bibliothèques de groupe. Pour importer des documents dans des bibliothèques de groupe, copiez les documents du groupe dans une collection de votre bibliothèque Mendeley avant de les importer. Vous pouvez ensuite créer un groupe Zotero et faire glisser les collections ou les documents importés vers ce groupe.
* Mendeley permet d'ajouter n'importe quel champ à n'importe quel type- de document. Lors de l'importation dans Zotero, si un champ n'est pas valide pour un type de document donné, le champ est placé dans le champ Extra. Dans la mesure du possible, ces champs seront utilisés automatiquement dans les citations (par exemple, la date d'origine), et les futures versions de Zotero les convertiront automatiquement vers les champs champs réels qui seront devenus disponibles.

## Dépannage

Assurez-vous que vous exécutez la dernière version de Zotero disponible via Aide → "Vérifier les mises à jour...".

Si vous exécutez la dernière version et que quelque chose ne se passe pas comme vous l'attendez ou que vous rencontrez des problèmes, faites-le nous savoir dans les [Forums Zotero](https://forums.zotero.org/).

## Chiffrement de la base de données Mendeley

L'importateur décrit ci-dessus importe des données directement à partir d'une bibliothèque Mendeley en ligne, qui exige que toutes les données et tous les fichiers soient téléchargés sur les serveurs d'Elsevier afin d'être importés dans Zotero.

Zotero avait initialement annoncé travailler sur un importateur entièrement local au début de 2018, mais quelques mois plus tard, Elsevier a commencé à chiffrer la base de données locale de Mendeley, la rendant illisible par Zotero et les autres outils standard de base de données. Ce changement est intervenu alors que Mendeley vantait depuis longtemps l'ouverture de son format de base de données comme une garantie contre le verrouillage et expliquait dans sa documentation que la base de données était accessible avec des outils standard. Mendeley Desktop importait lui-même des données de la base de données ouverte de Zotero depuis 2009.

Les [notes de mise à jour de Mendeley 1.19](https://www.mendeley.com/release-notes/v1_19) affirmaient que le chiffrement était destiné à "améliorer la sécurité" sur les appareils partagés, mais les applications chiffrent rarement leurs fichiers de données locaux, car les protections de fichiers sont généralement gérées par le système d'exploitation avec des autorisations de compte et un chiffrement complet du disque, et quelqu'un utilisant le même compte du système d'exploitation ou un compte administrateur peut déjà installer un keylogger pour capturer les mots de passe. Mendeley [a ensuite affirmé](https://twitter.com/mendeley_com/status/1006915998841221120) que le changement était requis par les nouvelles réglementations européennes sur la protection de la vie privée - une affirmation étrange, étant donné que ces réglementations sont conçues pour donner aux gens le contrôle de leurs données et garantir la portabilité des données, et non l'inverse - et a continué à affirmer, à tort, que l'exportation locale complète était toujours possible, tout en [rejetant](https://twitter.com/mendeley_com/status/1006919608471818240) à [plusieurs reprises](https://twitter.com/MendeleySupport/status/1006920802120470528) les rapports sur le changement comme "#fakenews".

L'accès direct à la base de données Mendeley est le seul moyen entièrement local d'exporter le contenu complet de ses propres recherches. Les formats d'exportation pris en charge par Mendeley ne contiennent pas les dossiers, différents champs de métadonnées (date d'ajout, favoris, et autres), ou les annotations PDF. Bien que Mendeley offre une API basée sur le Web, elle ne contient que des données téléchargées, ce qui signifie que toute personne souhaitant exporter ses propres données doit d'abord télécharger toutes ses données et tous ses fichiers sur les serveurs d'Elsevier. L'API est sous le contrôle d'Elsevier et peut être [modifiée](https://service.elsevier.com/app/answers/detail/a_id/31598/supporthub/mendeley/p/16075/) ou [supprimée](https://blog.mendeley.com/2021/03/11/mendeley-refocusing-announcement-mobile-app-retirement/) à tout moment.

Depuis ce changement, Elsevier a remplacé Mendeley Desktop par Mendeley Reference Manager, qui est essentiellement une enveloppe autour du site web et ne contient pas du tout de véritable base de données locale. 
