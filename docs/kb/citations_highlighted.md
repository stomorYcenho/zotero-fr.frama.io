# Pourquoi les citations ou les bibliographies Zotero sont-elles toujours surlignées en gris ou dans une autre couleur ?

*Consulter cette page dans la documentation officielle de Zotero : [Why are Zotero citations or bibliographies always highlighted in gray or another color?](https://www.zotero.org/support/kb/citations_highlighted) - dernière mise à jour de la traduction : 2022-09-28*
{ data-search-exclude }

Par défaut, Zotero stocke les données des références bibliographiques pour les citations et la bibliographie dans des [Champs](https://support.office.com/fr-fr/article/Insert-fields-in-Word-c429bbb0-8669-48a7-bd24-bab6ba6b06bb) (Word) ou des [Marques de référence](https://help.libreoffice.org/Writer/About_Fields/fr) (LibreOffice). Les données bibliographiques sont ainsi stockées derrière le texte mis en forme.

Word et LibreOffice mettent en surbrillance les Champs/Marques de référence sur votre écran pour indiquer que le texte est généré automatiquement. Cela peut vous aider à éviter de taper manuellement dans les champs par erreur (pour modifier le texte affiché dans une citation Zotero, voir [Personnaliser les citations](../word_processor_plugin_usage.md#personnaliser-les-citations). Cette mise en surbrillance ne s'affiche qu'à l'écran et n'apparaît pas si vous imprimez ou enregistrez le document au format PDF.

Vous pouvez modifier les paramètres de mise en surbrillance des Champs/Marques de référence dans votre traitement de texte.

-   **[Word pour Windows](https://helpdeskgeek.com/office-tips/show-field-shading-in-word-and-convert-the-fields-to-plain-text/) :** Dans le menu Fichier -&gt; Options, ouvrez les "Options avancées", puis dans la rubrique "Affichage du contenu des documents" positionnez la valeur de "Champs avec trame" sur "Jamais", "Toujours" ou "Lors de la sélection".
-   **Word pour Mac :** Ouvrez les Préférences -&gt; Affichage et positionnez la valeur de "Champs avec trame" sur "Jamais", "Toujours" ou "Lors de la sélection".
-   **[LibreOffice](https://help.libreoffice.org/Writer/About_Fields/fr) :** Ouvrez Outils -&gt; Options -&gt; LibreOffice -&gt; Couleurs de l'interface et cochez/décochez la case "Trame de fond des champs". Vous pouvez également contrôler la couleur utilisée pour la trame de fond des champs.


