# Comment faire pour que les titres apparaissent en mode phrase (sentence case) dans les bibliographies ?


*Consulter cette page dans la documentation officielle de Zotero : [How do I get titles to show up in sentence case in bibliographies?](https://www.zotero.org/support/kb/sentence_casing) - dernière mise à jour de la traduction : 2022-10-04*
{ data-search-exclude }

Certains styles bibliographiques, comme APA, exigent que les titres soient en mode phrase, **_sentence case_**, par exemple "Oxidation and reduction of iron by acidophilic bacteria". D'autres, comme Chicago Manual of Style, exigent qu'ils soient en mode titre, **_title case_**, par exemple "Oxidation and Reduction of Iron by Acidophilic Bacteria".

Malheureusement, il n'est pas possible pour Zotero ou tout autre outil d'automatiser de manière fiable la conversion vers le mode phrase, car un ordinateur ne peut pas savoir avec certitude si un mot désigne un nom propre (pensez, par exemple, au mot "united", qui peut être un adjectif, un verbe, le nom d'une société ou une partie du nom d'un pays). 

La solution est de **stocker les titres en mode phrase** dans votre bibliothèque Zotero et de laisser Zotero automatiser la conversion vers le mode titre si nécessaire. Par exemple, si vous utilisez Chicago Manual of Style, Zotero convertit automatiquement les titres en mode titre, tandis que si vous utilisez APA, il n'essaie pas de forcer le mode phrase et affiche les titres exactement comme ils apparaissent dans votre bibliothèque Zotero.

## Correction d'une capitalisation incorrecte

Si vous constatez qu'un titre est rendu de manière incorrecte en mode titre dans votre bibliographie, assurez-vous qu'il est stocké en mode phrase dans votre bibliothèque Zotero. Vous pouvez automatiser une partie de la conversion vers le mode phrase avec l'option "Lettre capitale en début de phrase" du menu contextuel activé par un clicc-droit sur le champ "Titre" d'un document dans votre bibliothèque Zotero. Vous devez alors rétablir manuellement les majuscules à tous les noms propres.

Il est également possible d'[empêcher la conversion en mode titre des titres non anglais](./preventing_title_casing_for_non-english_titles.md).


## Mode phrase et sous-titres

Certains styles qui exigent le mode phrase, comme APA, exigent également que la première lettre du sous-titre qui suit les deux points soit aussi en capitale, par exemple "Age and environmental sustainability : A meta-analysis". Zotero met automatiquement une capitale à l'initiale du sous-titre pour les styles qui l'exigent. Si le titre contient plusieurs deux-points ou tirets, enregistrez le titre principal (avant les deux-points) dans le champ "Titre abrégé" pour indiquer où commence le sous-titre. 



