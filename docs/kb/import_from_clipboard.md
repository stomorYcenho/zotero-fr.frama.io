# Comment utiliser la fonction "Importer depuis le presse-papiers" ?

*Consulter cette page dans la documentation officielle de Zotero : [How does the Import from Clipboard feature work?](https://www.zotero.org/support/kb/import_from_clipboard) - dernière mise à jour de la traduction : 2022-12-19*
{ data-search-exclude }

La fonction "Importer depuis le presse-papiers" vous permet d'importer des documents depuis le code brut de n'importe quel format pris en charge (RIS, BibTeX, CSL JSON, etc.).

Lorsque vous visualisez des données bibliographiques brutes, vous pouvez les importer dans Zotero en les copiant dans le presse-papiers et en choisissant "Importer depuis le presse-papiers" dans le menu Fichier de Zotero ou en utilisant le raccourci clavier Ctrl-Alt-Maj-I (Windows/Linux) ou Cmd-Alt-Maj-I (Mac).

Voir [Comment importer depuis BibTeX ou d'autres formats standards ?](./importing_standardized_formats.md) pour une liste des formats pris en charge.

Notez que sur la plupart des sites web il est préférable d'utiliser le bouton "Enregistrer dans..." du connecteur Zotero pour enregistrer les données bibliographiques et les fichiers dans Zotero, plutôt que d'importer directement des métadonnées. Voir [Ajouter des documents à Zotero](../adding_items_to_zotero.md) pour plus d'informations.
