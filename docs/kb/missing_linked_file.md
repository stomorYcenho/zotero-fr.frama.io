# Pourquoi Zotero ne  trouve-t-il pas un fichier lié?

*Consulter cette page dans la documentation officielle de Zotero : [Why can't Zotero find a linked file?](https://www.zotero.org/support/kb/missing_linked_file) - dernière mise à jour de la traduction : 2023-01-20*
{ data-search-exclude }

Plusieurs raisons peuvent expliquer qu'un [fichier lié](../attaching_files.md#fichiers-lies) ne puisse pas être trouvé.

1. Le fichier a été déplacé ou supprimé en dehors de Zotero. Retrouvez le fichier et remettez-le à son emplacement d'origine, ou utilisez le bouton "Localisation en cours..." de la boîte de dialogue "Fichier introuvable" pour indiquer à Zotero son nouvel emplacement.
2. Le [répertoire de base pour les pièces jointes liées](../advanced.md#repertoire-de-base-pour-les-pieces-jointes-liees) est mal configuré sur un ou plusieurs de vos ordinateurs. Si le répertoire de base des pièces jointes liées est correctement défini sur votre ordinateur actuel mais que Zotero recherche toujours un fichier avec un chemin absolu sur un autre ordinateur, vous devez corriger le paramètre de répertoire de base des pièces jointes liées sur cet autre ordinateur. Cela convertira les pièces jointes du répertoire spécifié pour utiliser des chemins relatifs. Enfin,  synchronisez les deux ordinateurs.
3. Zotero cherche au bon endroit, mais le fichier n'a pas encore été synchronisé depuis un autre ordinateur. Zotero ne synchronise que les fichiers joints, pas les fichiers liés. Les fichiers liés doivent donc être synchronisés à l'aide d'un autre outil.

Si nécessaire, le module complémentaire tiers [Zutilo](https://github.com/wshanks/Zutilo/tree/master/i18n/fr/readme) peut être utilisé pour corriger par lots les chemins des fichiers liés.
