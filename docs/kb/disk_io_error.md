# Pourquoi est-ce que je reçois le message "disk I/O error" au démarrage de Zotero ?

*Consulter cette page dans la documentation officielle de Zotero : [Why am I getting a "disk I/O error" at Zotero startup?](https://www.zotero.org/support/kb/disk_io_error) - dernière mise à jour de la traduction : 2022-10-04*
{ data-search-exclude }

Une erreur d'entrée/sortie de disque (disk I/O error) est un problème général, non spécifié, concernant la capacité de Zotero à accéder à votre disque. Si votre répertoire de données Zotero se trouve sur un lecteur réseau ou un disque externe, vous devriez le déplacer sur le disque local pour éviter ce genre de problèmes.

Si vous obtenez cette erreur avec votre répertoire de données sur votre disque local, il se peut qu'il y ait un véritable problème avec votre système de fichiers ou votre disque physique, ou qu'un logiciel de sécurité sur votre système interfère avec la capacité de Zotero à accéder à la base de données.