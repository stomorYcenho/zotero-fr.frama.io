# Raccourcis clavier de Zotero

*Consulter cette page dans la documentation officielle de Zotero : [Zotero Keyboard Shortcuts](https://www.zotero.org/support/kb/keyboard_shortcuts) - dernière mise à jour de la traduction : 2022-11-02*
{ data-search-exclude }

## Application de bureau Zotero

Notez que le [panneau des raccourcis](https://www.zotero.org/support/preferences/shortcut_keys) dars les Préférences vous permettent de changer certains de ces raccourcis clavier.

### Ajouter des documents dans votre bibliothèque Zotero

| Fonction                               | Windows/Linux          | Mac OS                   |
|----------------------------------------|------------------------|--------------------------|
| Enregistrer dans Zotero                | `Ctrl`+`Maj`+S       | `Cmd`+`Maj`+S          |
| Créer un nouveau document manuellement | `Ctrl`+`Maj`+N       | `Cmd`+`Maj`+N          |
| Créer une nouvelle note                | `Ctrl`+`Maj`+O       | `Cmd`+`Maj`+O          |
| Importer                               | `Ctrl`+`Maj`+I       | `Cmd`+`Maj`+I          |
| Importer depuis le presse-papier       | `Ctrl`+`Maj`+`Alt`+I | `Cmd`+`Option`+`Maj`+I |

### Modifier des documents (Onglet "Info")

| Fonction                                                       | Windows/Linux   | Mac OS          |
|----------------------------------------------------------------|-----------------|-----------------|
| Ajouter un autre Auteur/Créateur lors de l’édition du Créateur | `Maj`+`Entrée` | `Maj`+`Entrée` |
| Enregistrer le Résumé ou le champ Extra                                   | `Maj`+`Entrée` | `Maj`+`Entrée` |

### Retirer ou supprimer des documents et des collections

| Fonction                                           | depuis "Ma bibliothèque" | depuis une collection              |
|----------------------------------------------------|--------------------------|------------------------------------|
| Mettre à la Corbeille                              | `Suppr`                    | `Maj`+`Suppr`                      |
| Mettre à la Corbeille sans fenêtre de confirmation | `Maj`+`Suppr`            | _Non disponible_                   |
| Retirer de la collection                            | _Non applicable_        | `Suppr` _(Seulement pour les documents de premier niveau)_ |

| Fonction                                                                                                           | Key           |
|--------------------------------------------------------------------------------------------------------------------|---------------|
| Supprimer la collection (et conserver les documents dans "Ma bibliothèque" et les autres collections, s’il y en a) | `Suppr`         |
| Supprimer la collection et mettre les documents à la Corbeille                                                                          | `Maj`+`Suppr` |

### Créer des citations et des bibliographies (Copie rapide)

| Fonction                                                                                | Windows/Linux    | Mac OS          |
|-----------------------------------------------------------------------------------------|------------------|-----------------|
| Copier les citations des documents sélectionnées dans le presse-papier                  | `Ctrl`+`Maj`+A | `Cmd`+`Maj`+A |
| Copier les documents sélectionnés dans le presse-papier | `Ctrl`+`Maj`+C | `Cmd`+`Maj`+C |

### Naviguer entre les panneaux de Zotero

| Fonction                                                   | Windows/Linux                                                                     | Mac OS                                                                                      |
|------------------------------------------------------------|-----------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------|
| Naviguer dans le panneau des bibliothèque (panneau gauche) | `Ctrl`+`Maj`+ L                                                                  | `Cmd`+`Maj`+L                                                                             |
| Naviguer dans les panneaux et champs                       | `Tab`/`Maj`+`Tab`                                                               | `Tab`                                                                                       |
| Naviguer entre les onglets Info/Notes/Marqueurs/Connexes   | `→` et `←` (ou `Ctrl`+`Tab`/`Ctrl`+`Maj`+`Tab` ou `Ctrl`+`touche de page précédente`/`Ctrl`+`touche de page suivante`) | `Ctrl`+`touche de page précédente`/`Ctrl`+`touche de page suivante` |
| Recherche rapide                                           | `Ctrl`+`Maj`+K                                                                  | `Cmd`+`Maj`+K                                                                             |
| Recherche rapide                                           | `Ctrl`+F                                                                          | `Cmd`+F                                                                                     |

Les MacBooks et le clavier sans fil Apple n'ont pas de touches dédiées Page précédente / Page suivante, vous devez donc utiliser Fn+Flèche Haut/Bas sur ces derniers pour simuler ces touches.

### Naviguer entre les onglets

Zotero supporte la plupart des raccourcis standards pour naviguer entre les onglets :

* `Ctrl`+`PageDown`/`PageUp` (`Control`+`Fn`+`Up`/`Down` sur la plupart des claviers Mac)
    
* `Ctrl`+`Tab` / `Ctrl`+`Maj`-`Tab`
    
* `Cmd`+`Maj`+`[`/`]` (macOS seulement)
    
* `Cmd`+`Option`+`←`/`→` (macOS seulement)
    
* `Cmd`/`Ctrl`+`1` à `9`
    

### Recherche

| Fonction                                        | Windows/Linux                               | Mac OS             |
|-------------------------------------------------|---------------------------------------------|--------------------|
| Recherche rapide                                | `Ctrl`+`Maj`+K                            | `Cmd`+`Maj`+K    |
| Recherche rapide                                    | `Ctrl`+F                                    | `Cmd`+F            |
| Trouver / Surligner le(s) collections(s) contenant le document | Maintenir `Ctrl` (Windows) ou `Alt` (Linux) | Maintenir `Option` |

### Marqueurs

| Fonction                                  | Windows/Linux    | Mac OS          |
|-------------------------------------------|------------------|-----------------|
| Basculer le sélecteur de marqueurs        | `Ctrl`+`Maj`+T | `Cmd`+`Maj`+T |
| Assigner un marqueur coloré à un document | Touches 1 à 6    | Touches 1 à 6     |

### Flux RSS

| Fonction                                             | Windows/Linux    | Mac OS          |
|------------------------------------------------------|------------------|-----------------|
| Marqueur tous les documents du flux comme lus/non lus | `Ctrl`+`Maj`+R | `Cmd`+`Maj`+R |

### Autres raccourcis

| Fonction                                                               | Windows/Linux                                      | Mac OS               |
|------------------------------------------------------------------------|----------------------------------------------------|----------------------|
| Déployer / Replier les collections ou la liste des documents           | `+`/`-`                                            | `+`/`-`              |
| Trouver / Surligner le(s) collection(s) contenant le document          | _Maintenir_ `Ctrl` _(Windows) ou_ `Alt` _(Linux)_ | _Maintenir_ `Option` |
| Compter les documents (le résultat apparaît dans le panneau de droite) | `Ctrl`+A                                           | `Cmd`+A              |
| Renommer la collection (panneau de gauche)                                      | `F2`                                               | _Non disponible_     |

### Lecteur PDF

_Cette section est incomplète._

| Fonction                               | Windows/Linux                     | macOS                    |
|----------------------------------------|-----------------------------------|--------------------------|
| Basculer entre les outils d’annotation | `Alt`-`1`/`2`/`3`/`4`             | `Option`-`1`/`2`/`3`/`4` |
| Retour (lien dans le PDF)              | `Alt`+`Left`, `Ctrl`+`[` (Linux)  | `Cmd`+`Left`, `Cmd`+`[`  |
| Avancer (lien dans le PDF)                  | `Alt`+`Right`, `Ctrl`+`]` (Linux) | `Cmd`+`Right`, `Cmd`+`]` |

### Notes

**Cette section n’a pas été complètement mise à jour pour le nouvel éditeur des notes de Zotero 6.**

| Fonction                     | Windows/Linux                | Mac OS                     |
|------------------------------|------------------------------|----------------------------|
| Gras                         | `Ctrl`+B                     | `Cmd`+B                    |
| Italique                     | `Ctrl`+I                     | `Cmd`+I                    |
| Souligné                     | `Ctrl`+U                     | `Cmd`+U                    |
| Tout sélectionner            | `Ctrl`+A                     | `Cmd`+A                    |
| Annuler                      | `Ctrl`+Z                     | `Cmd`+Z                    |
| Répéter                      | `Ctrl`+Y ou `Ctrl`+`Maj`+Z | `Cmd`+Y ou `Cmd`+`Maj`+Z |
| Couper                       | `Ctrl`+X                     | `Cmd`+X                    |
| Copier                       | `Ctrl`+C                     | `Cmd`+C                    |
| Coller                       | `Ctrl`+V                     | `Cmd`+V                    |
| Coller sans la mise en forme | `Ctrl`+`Maj`+V             | `Cmd`+`Maj`+V            |
| Formater le titre de 1 à 6   | `Maj`+`Alt`+1/6            | `Ctrl`+`Alt`+1/6           |
| Formater comme paragraphe    | `Maj`+`Alt`+7              | `Ctrl`+`Alt`+7             |
| Formater comme Div           | `Maj`+`Alt`+8              | `Ctrl`+`Alt`+8             |
| Format comme adresse         | `Maj`+`Alt`+9              | `Ctrl`+`Alt`+9             |
| Chercher et remplacer        | `Ctrl`+F                     | `Cmd`+F                    |
| Insérer un lien              | `Ctrl`+K                     | `Cmd`+K                    |
| Basculer vers la barre d’outils        | `Alt`+F10                    | `Alt`+`F10`                |

## Connecteur Zotero

Chrome, Firefox et Edge permettent tous d'attribuer un raccourci clavier au bouton "Enregistrer dans Zotero". Consultez la documentation de votre navigateur pour plus d'informations. 

## Modules de traitement de texte 

* [Word](../word_processor_plugin_usage.md)
* [LibreOffice](../libreoffice_writer_plugin_usage.md)
* [Google Docs](../google_docs.md)

## Personnaliser vos raccourcis

Le module complémentaire tiers [Zutilo](https://github.com/willsALMANJ/Zutilo) ajoute diverses fonctions non disponibles dans Zotero lui-même par le biais d'options de menu supplémentaires et de raccourcis clavier.
