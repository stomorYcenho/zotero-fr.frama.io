# Comment puis-je enregistrer mon travail?

*Consulter cette page dans la documentation officielle de Zotero : [How do I save my work?](https://www.zotero.org/support/kb/saving_work) - dernière mise à jour de la traduction : 2023-04-06*
{ data-search-exclude }

Zotero enregistre automatiquement tout ce que vous saisissez.
