# Comment puis-je accéder à ma bibliothèque depuis plusieurs ordinateurs ? Puis-je stocker ma bibliothèque Zotero et les fichiers associés sur un disque externe ?

*Consulter cette page dans la documentation officielle de Zotero : [How can I access my library from multiple computers? Can I store my Zotero library and associated files on an external drive?](https://www.zotero.org/support/kb/using_multiple_computers) - dernière mise à jour de la traduction : 2023-01-20*
{ data-search-exclude }

La meilleure façon d'accéder à votre bibliothèque Zotero à partir de plusieurs ordinateurs est d'utiliser la [synchronisation Zotero](../sync.md). La synchronisation Zotero synchronise automatiquement les données de votre bibliothèque en utilisant les serveurs Zotero. Les fichiers joints peuvent être synchronisés à l'aide des serveurs Zotero ou d'un [serveur WebDAV tiers](../sync.md#webdav).

Vous devez absolument ne pas stocker votre base de données Zotero dans un dossier de stockage en nuage (par exemple, Dropbox, Google Drive, OneDrive), car [cela entraînerait la corruption des données](https://www.zotero.org/support/kb/data_directory_in_cloud_storage_folder). Vous pouvez toutefois configurer Zotero pour [synchroniser vos fichiers joints à l'aide de ces services tiers](../sync.md#solutions-alternatives-de-synchronisation), tout en utilisant la synchronisation Zotero pour synchroniser les données de votre bibliothèque.

Si, pour une raison quelconque, vous ne pouvez pas utiliser les fonctions de synchronisation intégrées de Zotero, vous pouvez également stocker votre [répertoire de données Zotero](../zotero_data.md) sur un disque dur externe et l'utiliser pour déplacer vos données Zotero entre ordinateurs. Vous pouvez configurer votre installation de Zotero sur chaque ordinateur pour qu'elle pointe vers ce disque externe dans le [volet "Avancées" des préférences de Zotero](../advanced.md).

Une autre option consiste à [exécuter une copie de Zotero directement à partir d'un disque portable](https://www.zotero.org/support/kb/portable_zotero).
