# Pourquoi Zotero ne détecte-t-il pas mes citations existantes ?

*Consulter cette page dans la documentation officielle de Zotero : [Why isn’t Zotero detecting my existing citations?](https://www.zotero.org/support/kb/existing_citations_not_detected) - dernière mise à jour de la traduction : 2023-02-03*
{ data-search-exclude }

Si vous avez déjà utilisé un des modules de traitement de texte de Zotero pour insérer des citations dans un document et que vous constatez par la suite que 1) le module affiche "Vous devez insérer une citation avant d'effectuer cette opération", que 2) la bibliographie ne contient pas toutes les citations du document, ou que 3) dans un style bibliographique numérique les références commencent à 1 au lieu d'un numéro approprié plus élevé, les citations existantes dans le document peuvent ne plus être des champs actifs.

Vous pouvez vérifier si les champs d'un document ne sont plus actifs en cliquant dessus et en recherchant une surbrillance grise (Word/LibreOffice) ou un popup "Edit with Zotero" (Google Docs). Si vous cliquez ensuite sur "Add/Edit Citation", la boîte de dialogue de citation doit apparaître et afficher la citation. Si la boîte de dialogue de citation est vide, la citation n'est plus active. Dans Word et LibreOffice, vous pouvez également essayer de [basculer les codes de champ](./word_field_codes.md).



Il se peut que vos citations soient devenues inactives pour un certain nombre de raisons.

1.  Vous avez utilisé le bouton "Unlink Citations". Ce bouton déconnecte votre document de Zotero et convertit toutes les citations et bibliographies en texte brut.
2.  Vous avez enregistré le document dans un format de fichier non pris en charge.
    -   Lorsque vous utilisez Word, vous devez enregistrer votre document au format .docx. Si vous enregistrez en .odt, les citations actives sont perdues.
    -   Lorsque vous utilisez LibreOffice, si vous stockez les citations dans des "Marques de référence" (option par défaut), vous devez enregistrer votre document en .odt. Si vous stockez les citations dans des "Signets", vous devez enregistrer votre document en .docx.

3.  Vous (ou quelqu'un d'autre) avez ouvert et enregistré votre document en utilisant un traitement de texte non pris en charge, ou sans suivre les étapes appropriées pour transférer les citations actives entre les traitements de texte pris en charge.
    -   **Google Docs :** Si vous souhaitez ouvrir un document Word ou LibreOffice dans Google Docs, ou inversement, vous devez effectuer une étape supplémentaire pour [transférer le document](./moving_documents_between_word_processors.md). Ouvrir directement un document Word ou LibreOffice dans Google Docs, ou inversement, casse les citations existantes.
    -   **Autres traitements de texte en ligne :** La plupart des traitements de texte en ligne ne prennent pas en charge les Champs/Marques de référence/Signets. Ouvrir un document existant dans ces outils brise les liens avec Zotero. Word Online de Microsoft prend en charge les champs et peut donc être utilisé en toute sécurité avec les documents Word contenant des champs Zotero, bien qu'un module Zotero ne soit pas disponible pour Word Online.
    -   **Pages :** Apple Pages ne prend pas en charge les Champs/Marques de référence/Signets. Ouvrir un document dans Pages brise les liens avec Zotero.
    -   **Word :** Si vous ouvrez un fichier .odt (créé par LibreOffice) dans Word, les citations Zotero stockées dans des "Marques de référence" (option par défaut) seront brisées. Pour partager un document entre les utilisateurs de Word et de LibreOffice, cliquez sur "Document Preferences" dans le module Zotero et changez l'option "Stocker les citations dans des : " en sélectionnant "Signets". Les signets peuvent causer des erreurs s'ils sont modifiés accidentellement, ils ne doivent donc être utilisés que si la compatibilité entre Word et LibreOffice est nécessaire. Vous pouvez également préférer [transférer le document](./moving_documents_between_word_processors.md#word-et-libreoffice).
    -   **LibreOffice :** Si vous ouvrez un fichier .doc ou .docx (créé par Word) dans LibreOffice, les citations Zotero stockées dans des "Champs" (option par défaut) seront brisées. Pour partager un document entre les utilisateurs de Word et de LibreOffice, cliquez sur "Document Preferences" dans le module Zotero et changez l'option "Stocker les citations dans des : " en sélectionnant "Signets". Les signets peuvent causer des erreurs s'ils sont modifiés accidentellement, ils ne doivent donc être utilisés que si la compatibilité entre Word et LibreOffice est nécessaire. Vous pouvez également préférer [transférer le document](./moving_documents_between_word_processors.md#word-et-libreoffice).

 4. Si vous utilisez Google Docs, référez-vous à la page [Pourquoi Zotero ne détecte-t-il pas mes citations existantes dans Google Docs?](./google_docs_citations_unlinked.md) pour d'autres raisons possibles.

Si vous constatez que vos citations ont été écrasées, vos seules options sont de restaurer le document à partir d'une sauvegarde, de réinsérer les citations à l'aide du module, ou de modifier manuellement les citations et la bibliographie du document à l'avenir, et de générez une bbiliographie récapitulative à partir d'une collection de Zotero, sans utiliser le module de traitement de texte. Si vous réinsérez les citations, il peut être utile d'ajuster les paramètres des champs Word pour que les champs soient toujours mis en surbrillance en gris, plutôt que de ne le faire que lorsqu'ils sont sélectionnés.

Si votre bibliographie est écrasée mais que vos citations sont toujours actives, vous pouvez simplement insérer une nouvelle bibliographie en cliquant sur "Add/Edit Bibliography".
