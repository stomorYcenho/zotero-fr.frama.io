# Comment référencer les différents rôles de créateur, tels que réalisateur ou producteur, pour les films et autres médias ?

*Consulter cette page dans la documentation officielle de Zotero : [How do I label different creator roles, such as Director or Producer, for films and other media?](https://www.zotero.org/support/kb/media_creator_roles) - dernière mise à jour de la traduction : 2023-02-16*
{ data-search-exclude }

Pour les citations de films, d'enregistrements et d'émissions, Zotero propose actuellement une prise en charge limitée au référencement des producteurs, des scénaristes et de certains autres rôles de créateurs.

Pour référencer les réalisateurs, laissez le champ principal de Zotero vide (ou entrez les noms en tant que "Contributeur") et entrez le nom des réalisateurs en utilisant `Director` dans "Extra". Voir la section [Champs citables à partir du champ Extra](./item_types_and_fields.md#champs-citables-a-partir-du-champ-extra).

Tous les rôles de créateur (réalisateur, producteur, scénariste, etc.) peuvent également être référencés en entrant le nom à l'aide du rôle "auteur" par défaut pour le document (*Interprète* pour un enregistrement audio, *Diffuseur* pour une baladodiffusion, et *Metteur en scène* pour un film, une émission de radio ou de télévision, et un enregistrement vidéo) et en ajoutant l'intitulé approprié entre parenthèses après le prénom de l'auteur - par exemple, MacNaughton || Ian (Producteur). Notez que les intitulés sont rendus tels qu'ils sont saisis dans les citations ; entrez des termes abrégés (p. ex., "Prod.") ici si nécessaire.

Si le style utilise des initiales pour le prénom des auteurs plutôt que la forme complète des noms (par exemple, le style APA), si l'intitulé contient plusieurs mots (par exemple, "Producteur exécutif" ou "Écrivain et réalisateur"), Zotero abrégera les mots de l'intitulé suivant le premier mot. Pour éviter cela, tapez un caractère "Gluon de mots" (Unicode U+2060) de chaque côté de chaque espace de l'intitulé.

Voir aussi [Intitulés des rôles pour les créateurs de médias](./item_types_and_fields.md#intitules-des-roles-pour-les-createurs-de-medias)
