#  Comment remplacer les occurrences successives d'un même auteur par un terme/symbole déterminé ?

*Consulter cette page dans la documentation officielle de Zotero : [How can subsequent occurences of the same author replaced by a fixed term/symbol?](https://www.zotero.org/support/kb/idem) - dernière mise à jour de la traduction : 2022-11-27*
{ data-search-exclude }


Certains styles bibliographiques exigent de remplacer toutes les occurrences successives du même auteur/auteur de livre/éditeur par [idem/eadem](http://en.wikipedia.org/wiki/Idem), leur abréviation id./ead., ou un autre terme ou symbole comme un tiret.

a) Il se peut que vous ayez besoin de tels remplacements _à l'intérieur de la bibliographie_, par exemple :

```
 Doe, Johnson & Williams. 2001.
 --- & Smith. 2002.
 Doudou, Stevens et Miller. 2003.
 ---, --- & ---. 2004.
```

Cela est **possible** et dépend simplement du style bibliographique que vous utilisez. Si vous voulez l'utiliser dans votre propre style bibliographique, reportez-vous aux [règles de l'attribut subsequent-author-substitute dans CSL](https://docs.citationstyles.org/en/stable/specification.html#reference-grouping).

b) Il se peut que vous ayez besoin de tels remplacements _à l'intérieur d'une même référence_, par exemple pour une œuvre dans une anthologie (œuvre collectée ou sélectionnée), par exemple

```
 DOE, JOHN (1998) : Article. In : IDEM (ed) : Livre.
```
Cela n'est actuellement pas possible, mais comme **solution de contournement**, vous pouvez ici entrer dans Zotero "IDEM" comme auteur ou éditeur du livre, ce qui vous donnera le résultat souhaité.

c) Il se peut également que vous ayez besoin de tels remplacements pour _des citations ultérieures du même auteur, mais de livres différents_, par exemple

```
 V. HUGO, Les Misérables, Livre de poche, 2002, p. 127.
 ID., La Légende des siècles, Hatier, 1998, p. 45.
```
Cela n'est actuellement pas possible, mais comme **solution de contournement**, vous pouvez ici supprimer l'auteur et ajouter "ID. " manuellement.

Sujet principal pour ce problème : https://forums.zotero.org/discussion/4785/check-for-identical-authoreditor/
