# Comment trier mes notes par numéro de page ?

*Consulter cette page dans la documentation officielle de Zotero : [How do I sort my notes by page number?](https://www.zotero.org/support/kb/sorting_notes) - dernière mise à jour de la traduction : 2022-01-13*
{ data-search-exclude }

Les notes sont classées par défaut par ordre alphabétique. Ainsi, vous pouvez commencer chaque note par le numéro de page, en ajoutant des zéros en tête, par exemple [002], [045], [367]. Pour plusieurs notes sur la même page, saisissez [367a], [367b], etc.

Remarque : L'ajout d'un grand nombre de notes à de nombreux documents peut avoir un impact négatif sur les performances de Zotero. Nous recommandons de stocker tous les commentaires de documents individuels dans une seule note attachée.
