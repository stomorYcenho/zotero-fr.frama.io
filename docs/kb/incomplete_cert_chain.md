# "[domaine] utilise un certificat de sécurité non valide. Le certificat n'est pas fiable car […]"

*Consulter cette page dans la documentation officielle de Zotero : ["[domain] uses an invalid security certificate. The certificate is not trusted because […]"](https://www.zotero.org/support/kb/incomplete_cert_chain) - dernière mise à jour de la traduction : 2022-10-24*
{ data-search-exclude }

Si un rapport d'erreur Zotero affiche une erreur similaire à celle ci-dessus pour le serveur proxy ou WebDAV de votre institution ou un site à partir duquel vous essayez d'enregistrer, il y a deux possibilités :

1. Vous vous connectez à un serveur avec un "certificat auto-signé". Dans le cas d'un serveur proxy ou WebDAV, vous devez [inscrire le certificat sur une liste blanche](https://www.zotero.org/support/kb/cert_override) dans Zotero. C'est probablement le cas si vous essayez d'utiliser un serveur que vous avez configuré vous-même.
2. Le serveur est mal configuré et devra être réparé par votre service informatique ou celui de l'exploitant du site. Voir les détails techniques ci-dessous pour plus d'informations.

Si vous utilisez un proxy institutionnel ou WebDAV et que vous n'êtes pas sûr de ce qu'il en est, indiquez à votre service informatique cette page ainsi que l'URL du rapport d'erreur.

Si vous obtenez une erreur de certificat pour une URL zotero.org ou s3.amazonaws.com - par exemple, lors d'une synchronisation - il s'agit d'[un problème différent](https://www.zotero.org/support/kb/ssl_certificate_error).

## Détails techniques : Certificat intermédiaire manquant

Si le serveur n'utilise pas un certificat auto-signé (c'est-à-dire, s'il est lié à un certificat racine qui est reconnu par les magasins du navigateur), cette erreur se produit généralement parce que le serveur ne sert pas le "certificat intermédiaire" nécessaire pour les connexions sécurisées, et Zotero (comme Firefox, sur lequel il est basé) ne le téléchargera pas de lui-même. Sans certificat intermédiaire, il est impossible de déterminer si la connexion est sécurisée, et la connexion échoue.

Pour vérifier que c'est le cas, soumettez l'URL du rapport d'erreur à https://www.ssllabs.com/ssltest/ et affichez les résultats. Si vous voyez "Chain issues : Incomplete" en orange sous "Additional Certificates (if supplied)", vous rencontrez ce problème. Le rapport indiquera alors également "Extra download" (au lieu de "Sent by server" ou "In trust store") pour un ou plusieurs certificats répertoriés sous "Certification Paths". Il se peut également qu'un ou plusieurs certificats intermédiaires groupés soient répertoriés comme étant expirés. Le ou les certificats intermédiaires manquants doivent être fournis avec le certificat primaire du site lorsque les clients HTTPS se connectent.

Si vous êtes en mesure de charger la même URL HTTPS dans un navigateur, soit le navigateur télécharge automatiquement les certificats intermédiaires (comme le fait Chrome), soit vous avez précédemment chargé un autre site (peut-être même celui de votre institution) qui comprenait le certificat intermédiaire, que le navigateur a mis en cache et utilise même sur les sites qui ne le servent pas correctement. Les sites devraient toujours fournir leurs certificats intermédiaires et sont mal configurés s'ils ne le font pas. Dans le cas de Firefox, si vous créez un nouveau profil Firefox, vous devriez obtenir une erreur de certificat en essayant de charger la même URL, ce qui est essentiellement la situation dans laquelle se trouve Zotero.