# Connecteur Zotero et Safari

*Consulter cette page dans la documentation officielle de Zotero : [Zotero Connector and Safari](https://www.zotero.org/support/kb/safari_compatibility) - dernière mise à jour de la traduction : 2023-04-04*
{ data-search-exclude }

## Installation

Le connecteur Zotero pour Safari est inclus dans Zotero 6. (Les versions actuelles de Safari ne permettent pas l'installation directe d'extensions de navigateur comme les autres navigateurs). Après avoir ouvert Zotero 6 pour la première fois, vous pouvez activer le connecteur Zotero dans le volet "Extensions" des préférences de Safari (menu “Safari” → “Paramètres”/“Préférences” → “Extensions”).

Le connecteur Zotero pour Safari nécessite Safari 15 sur macOS 11 Big Sur ou une version ultérieure.

Vous utilisez un iPhone ou un iPad ? Vous pouvez enregistrer dans l’[application Zotero pour iOS](https://apps.apple.com/us/app/zotero/id1513554812) en utilisant les options de partage de Safari et des autres navigateurs web.

![Le connecteur Zotero dans le gestionnaire d'extensions de Safari](../images/safari-compatibility.png)

## Le bouton d'enregistrement n'apparaît pas ? Le bouton d'enregistrement clignote ?

En raison d'un bug macOS, vous pouvez parfois constater que l'extension a disparu de la barre d'outils Safari ou qu'elle apparaît et réapparaît rapidement après une mise à jour de Zotero. Pour restaurer l'extension, supprimez l'application Zotero des Applications et réinstallez-la. (Vos données Zotero ne seront pas affectées.) Il peut également être nécessaire de redémarrer votre ordinateur entre la suppression de l'application et sa réinstallation.

Nous pensons que ce bug est moins fréquent dans macOS 11 Big Sur et les versions ultérieures. Ainsi Zotero inclut le connecteur pour Safari uniquement pour ces versions. Il est possible d'exécuter le connecteur Zotero sur macOS Mojave (10.14) et Catalina (10.15) en utilisant la [version bêta de Zotero](https://www.zotero.org/support/beta_builds), mais vous devrez peut-être corriger l'extension plus fréquemment après les mises à jour de l'application Zotero et vous risquez de rencontrer plus d'instabilité, ce qui pourrait nécessiter un démarrage en [mode sans échec](https://support.apple.com/guide/mac-help/start-up-your-mac-in-safe-mode-mh21245/mac) avant d'effectuer les étapes ci-dessus. (Les premières versions de Zotero 6 activaient par accident l'extension Safari pour les anciennes versions de macOS, mais cela a été corrigé et est à nouveau disponible pour ces versions de macOS uniquement sur le canal beta).

Si vous rencontrez des difficultés de façon répétée lors de l'utilisation du connecteur Zotero dans Safari, vous pouvez envisager de passer à un navigateur doté d'un cadre d'extension plus stable, tel que Firefox, Chrome ou Edge.

## Limitations

En raison des limitations techniques du système d'extensions de Safari, certaines fonctionnalités disponibles dans Firefox, Chrome et Edge ne le sont pas dans Safari :

* Redirection automatique vers un proxy
* Importation automatique de RIS/BibTeX
* Installation automatique de CSL

**Autres différences :**

* Les PDF sous accès contrôlé (*gated PDF*) peuvent ne pas être enregistrés sur certains sites.
* Il n'est pas possible de cliquer avec le bouton droit de la souris sur le bouton de la barre d'outils pour accéder aux convertisseurs secondaires. À la place, cliquez avec le bouton droit de la souris sur la page elle-même.
