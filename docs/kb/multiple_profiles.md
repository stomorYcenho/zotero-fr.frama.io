# Comment utiliser plusieurs profils Zotero ?

*Consulter cette page dans la documentation officielle de Zotero : [How can I use multiple profiles in Zotero?](https://www.zotero.org/support/kb/multiple_profiles) - dernière mise à jour de la traduction : 2022-10-06*
{ data-search-exclude }

Zotero vous permet de créer plusieurs profils, chacun avec ses propres paramétrages et son propre [répertoire de données Zotero](../zotero_data.md). Si vous êtes familier avec l'utilisation de plusieurs profils dans Firefox, Zotero fonctionne de la même manière et supporte les mêmes options.

Un profil par défaut est créé au premier démarrage de Zotero. Pour créer un nouveau profil, il faut lancer Zotero en ligne de commande et utiliser l'option `-P` afin d'ouvrir le gestionnaire de profils.

## macOS

* Ouvrez Terminal via Spotlight *ou* /Applications/Utilities.
* Collez `/Applications/Zotero.app/Contents/MacOS/zotero -P` dans la fenêtre Terminal.
* Appuyez sur Entrée.

## Windows

* Ouvrez la boîte de dialogue Exécuter (Recherche/Cortana → tapez “Exécuter” → Exécuter (Windows 10) *ou* Démarrer → Exécuter (Windows 7)) *ou* raccourci clavier touche Windows + R
* Collez `C:\Program Files (x86)\Zotero\zotero.exe -P`
* Appuyez sur Entrée.

## Linux

* Lancez Zotero de la ligne de commande, en ajoutant l'option `-P`.

Le gestionnaire de profils devrait apparaître, vous permettant de sélectionner, créer ou supprimer des profils Zotero.

Quand vous créez un nouveau profil (par exemple "Travail"), s'il existe déjà un profil pointant sur l'emplacement par défaut du répertoire de donnnées, Zotero créera un nouveau répertoire de données nommé d'après le nouveau profil (par exemple "Zotero Travail) quand vous le démarrerez. Votre répertoire de données initial ne sera pas impacté.

Vous pouvez utiliser la ligne de commande pour ouvrir un profil spécifique en précisant son nom avec l'option `-p` (par ex. : `-p Travail` où Travail est le nom du profil), ce qui permet de créer des raccourcis ouvrant un profil en particulier. (Sur Mac, vous pouvez enregistrer [un AppleScript intégrant les invites de commandes](https://superuser.com/a/116237) en tant qu'application dans Script Editor).

Une configuration complémentaire peut être requise pour ([utiliser plusieurs instances Zotero en même temps](./multiple_instances.md)).
