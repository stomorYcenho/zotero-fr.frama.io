# Pourquoi le texte surligné apparaît-il deux fois dans le lecteur PDF ou dans les notes créées à partir d'annotations ?

*Consulter cette page dans la documentation officielle de Zotero : [Why do I see highlighted text twice in the PDF reader or in notes created from annotations?](https://www.zotero.org/support/kb/doubled_highlights) - dernière mise à jour de la traduction : 2023-02-16*
{ data-search-exclude }

Certains lecteurs PDF externes copient le texte surligné dans le champ de commentaire de l'annotation pour le rendre modifiable. Zotero analyse et affiche le texte surligné lui-même et utilise les commentaires d'annotation pour l'usage pour lequel ils sont conçus, c'est-à-dire pour des commentaires modifiables. Si vous affichez ensuite dans Zotero des annotations créées en externe, vous verrez le texte surligné deux fois. Souvent, la version  du texte en surbrillance dans le champ de commentaire comporte des sauts de ligne supplémentaires, car les lecteurs PDF externes ne les suppriment pas toujours automatiquement comme le fait Zotero.

Si vous utilisez Zotero pour afficher les annotations ou les ajouter aux notes, vous devez désactiver le paramètre de votre lecteur PDF qui copie le texte surligné dans les commentaires.

Dans une prochaine version, Zotero essaiera de supprimer automatiquement les commentaires contenant du texte qui duplique le texte surligné.
