# Quel est le style Harvard officiel?

*Consulter cette page dans la documentation officielle de Zotero : [What is the official Harvard style?](https://www.zotero.org/support/kb/harvard_style) - dernière mise à jour de la traduction : 2023-04-06*
{ data-search-exclude }

"Le style Harvard" est un nom populaire pour désigner le mode de référencement auteur-date entre parenthèses.

Ce mode de référencement ne tire pas son origine de l'université d'Harvard[^1]. De multiples variantes du "style Harvard" ont cours, et il n'y a pas de style Harvard "officiel".

Si vous recherchez un style appelé "Harvard", vous pouvez vérifier si parmi les nombreux styles "Harvard" [disponibles dans l'entrepôt des styles Zotero](https://www.zotero.org/styles?q=harvard) il en existe un spécifique à votre institution.

S'il n'y a pas de style pour votre institution, vous pouvez utiliser [l'outil CSL de recherche à partir d'un exemple](https://editor.citationstyles.org/searchByExample) pour trouver un style qui corresponde aux consignes de rédaction votre institution.

Si vous ne trouvez pas de correspondance satisfaisante, vous pouvez [demander un nouveau style](https://github.com/citation-style-language/styles/wiki/Requesting-Styles).

***

[^1]:  Des preuves anecdotiques suggèrent que le nom provient d'un visiteur anglais du Musée de zoologie comparée de l'université Harvard. Voir Chernin, Eli (1988). ["The 'Harvard system': a mystery dispelled"](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC1834803/), *British Medical Journal*. 22 octobre 1988, p. 1062-1063.
