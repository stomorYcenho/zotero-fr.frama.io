# Zotero et Wikipédia/ Wikidata

*Consulter cette page dans la documentation officielle de Zotero : [Zotero and Wikipedia/ Wikidata](https://www.zotero.org/support/kb/zotero_and_wikipedia) - dernière mise à jour de la traduction : 2023-02-22*
{ data-search-exclude }

## Wikipédia

Zotero peut être un outil puissant pour les utilisateurs de Wikipédia, qu'ils soient des visiteurs occasionnels ou des contributeurs réguliers. En insérant des références imprimées, les utilisateurs de Wikipédia peuvent apporter une plus grande crédibilité à leurs articles et fournir aux lecteurs une base de référence plus large, forgeant de nouvelles relations entre les ressources en ligne et hors ligne. Wikipédia utilise son propre format pour les références bibliographiques, connu sous le nom de [Wikipedia Citation Templates](http://en.wikipedia.org/wiki/Citation_templates). Les modèles de citation de Wikipédia pour les livres et les articles intègrent des balises COinS dans chaque entrée bibliographique - voir, par exemple, le balisage HTML brut de [l'article de Wikipédia sur Zotero](https://en.wikipedia.org/wiki/Zotero). Par conséquent, Zotero peut importer des références depuis des articles qui les utilisent. En outre, un format d'exportation "Wikipedia Citation Templates" dans Zotero permet d'exporter des références vers Wikipedia. Cette relation de lecture/écriture promet de favoriser une meilleure circulation de l'information à travers les différents réseaux de communication académique.

*N.B. : si vous souhaitez citer une page web dans Wikipédia, vous devez inclure une date dans le champ "Consulté le".*

Voir : https://en.wikipedia.org/wiki/Wikipedia:Citing_sources_with_Zotero

### Copie rapide

La fonction de [copie rapide](../creating_bibliographies.md#copie-rapide) de Zotero permet d'exporter facilement des documents Zotero vers Wikipédia. Ouvrez [le volet "Exportation" des préférences de Zotero](../export.md) et sélectionnez "Wikipedia Citation Templates" comme format par défaut. Vous pouvez ensuite faire glisser et déposer des documents de votre bibliothèque vers Wikipédia pour insérer des citations correctement formatées. Vous pouvez également copier les données au format "Wikipedia Citation Templates" dans votre presse-papiers en appuyant sur Ctrl/Cmd+Maj+C.

## Wikidata

Wikidata est un projet jumeau de Wikipédia, géré par la même organisation à but non lucratif (la Wikimedia Foundation) et fonctionnant selon le même modèle de modification par des bénévoles. Il fournit des données ouvertes et liées sur les entités décrites dans Wikidata, et bien d'autres encore, sous une licence de domaine public.

Zotero dispose de deux convertisseurs à utiliser avec Wikidata :

* l'un pour lire les pages Wikidata et ajouter leurs données à Zotero,
* l'autre pour sortir les données provenant d'autres sources dans un format qui puisse être utilisé pour créer de nouveaux objets Wikidata, en utilisant un outil appelé QuickStatements.

Pour plus de détails, voir : https://www.wikidata.org/wiki/Wikidata:Zotero
