#  Pourquoi une citation n'est-elle pas mise à jour dans mon document, alors que j'ai modifié la notice correspondante dans ma bibliothèque Zotero ?

*Consulter cette page dans la documentation officielle de Zotero : [Why is a citation not updated in my document after editing the item in Zotero?](https://www.zotero.org/support/kb/citations_not_updating) - dernière mise à jour de la traduction : 2022-11-18*
{ data-search-exclude }

Lorsque vous apportez des modifications aux données d'une notice (titre, auteur, date, etc.) dans votre bibliothèque Zotero, ces modifications sont reflétées dans les citations correspondantes dans votre traitement de texte dès que vous cliquez sur le bouton "Refresh" du module de traitement de texte.

Si vos citations sont du texte plat et ne sont pas détectées du tout, consultez la page [Pourquoi Zotero ne détecte-t-il pas mes citations existantes ?](./existing_citations_not_detected.md).

Si vous avez toujours des citations actives (par exemple, surlignées en gris lorsque vous cliquez dessus dans Word ou LibreOffice, ou affichant la fenêtre contextuelle "Edit in Zotero" dans Google Docs) mais que les modifications ne sont pas reflétées, la citation dans votre document n'est plus liée à la notice correspondante dans votre bibliothèque Zotero. Cela peut se produire pour un certain nombre de raisons.

1. Vous avez des références en double dans votre bibliothèque, vous avez cité l'un des doublons, puis vous l'avez supprimé au lieu de le fusionner avec l'autre référence en doublon.
2. Vous avez supprimé une référence de votre bibliothèque, puis vous l'avez importée à nouveau.
3. Vous avez cité une référence avec Mendeley, puis vous avez transféré votre bibliothèque vers Zotero.

Si vous avez des citations orphelines dans votre document, vous devez les supprimer de votre document et les réinsérer, en veillant à les sélectionner dans la section "Ma bibliothèque" plutôt que dans la section "Cité" de la boîte de dialogue de citation .
