# Comment ajouter une archive ou une autre source non publiée ?

*Consulter cette page dans la documentation officielle de Zotero : [How do I add an archival or other unpublished source?](https://www.zotero.org/support/kb/archival_or_other_unpublished_sources) - dernière mise à jour de la traduction : 2022-12-19*
{ data-search-exclude }

Cliquez sur le bouton vert "Nouveau document" et sélectionnez "Manuscrit". Entrez le type de source (communiqué de presse, brouillon, notes de terrain, notes de conférence, etc.) dans le champ "Type". Utilisez le champ "Loc. dans l'archive" pour indiquer l'emplacement du document, c'est-à-dire la boîte, le dossier, le groupe d'enregistrements, etc. Utilisez le champ "Archive" pour entrer le nom de l'archive ou de la collection. Pour ajouter des lettres, voir [Comment ajouter une lettre ou un mémo ?](./adding_a_letter_or_memo.md)
