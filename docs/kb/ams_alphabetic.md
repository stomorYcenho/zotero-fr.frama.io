# Zotero prend-il en charge des styles utilisant un trigraphe label/auteur, comme [ddb98] ?

*Consulter cette page dans la documentation officielle de Zotero : [Does Zotero support label/authorship trigraph styles, like [ddb98]?](https://www.zotero.org/support/kb/ams_alphabetic) - dernière mise à jour de la traduction : 2023-04-06*
{ data-search-exclude }

Non, pas encore complètement. Le processeur de citation de Zotero [citeproc-js](https://github.com/juris-m/citeproc-js) crée automatiquement pour la variable "citation-label" de CSL une valeur composée de quatre lettres, à partir des premières lettres du nom de l'auteur. Voyez [DIN-1505-2](https://www.zotero.org/styles/din-1505-2-alphanumeric) pour un exemple de style utilisant ce format.

La mise en forme des labels de citation automatiques ne peut actuellement pas être personnalisée dans Zotero. Vous pouvez saisir manuellement des labels de citation pour chacun des documents dans votre bibliothèque Zotero, en les ajoutant dans le champ "Extra" sous le format suivant.

```
citation-label: Smi01
```

Plusieurs discussions à ce sujet sont disponibles [ici](https://github.com/citation-style-language/schema/issues/41) et [là](https://github.com/citation-style-language/styles/issues/678).
