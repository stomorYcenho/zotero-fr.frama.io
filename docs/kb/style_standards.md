# Styles bibliographiques normalisés

*Consulter cette page dans la documentation officielle de Zotero : [Standard Citation Styles](https://www.zotero.org/support/kb/style_standards) - dernière mise à jour de la traduction : 2023-03-22*
{ data-search-exclude }

Des sociétés savantes ou professionnelles produisent des guides de style (on dit aussi code typographique) afin de normaliser les méthodes de citation dans leur domaine. Ces normes doivent être respectées, à moins qu'il n'y ait une **très** bonne raison d'utiliser ou de créer un autre style.

Vous trouverez ci-dessous une liste de quelques guides de style de sociétés savantes ou professionnelles. Les styles CSL correspondants à la plupart d'entre eux se trouvent dans le [dépôt de styles Zotero](https://www.zotero.org/styles).

Si vous connaissez d'autres guides de style d'associations officielles, n'hésitez pas à les ajouter
(il s'agit d'un wiki, et [votre contribution est la bienvenue](https://www.zotero.org/support/dev/documentation)).

De nombreux guides de style se présentent sous la forme d'ouvrages imprimés qui ne sont pas librement accessibles ; votre bibliothèque en possède peut-être un exemplaire, et il existe également des pages web comme [celle-ci pour APA](https://bib.umontreal.ca/citer/styles-bibliographiques/apa) ou [celle-là pour Chicago](https://bib.umontreal.ca/citer/styles-bibliographiques/chicago) qui donnent gratuitement des indications simples sur l'utilisation correcte ou adaptée des styles.

L'utilisation d'identifiants uniques, tels que le [DOI](./doi_in_bibliography.md), est de plus en plus encouragée dans les styles de citation.


## Normes internationales

- [ISO 690](http://www.iso.org/iso/iso_catalogue/catalogue_tc/catalogue_detail.htm?csnumber=4888) ([page Wikipedia en français](https://fr.wikipedia.org/wiki/ISO_690))
- [ISO 832](https://www.iso.org/standard/5195.html)

## Normes nationales

- [American National Standards Institute & National Information Standards Organization](https://www.ansi.org/) (standard Z39.29-2005)
- [British Standards Institution](http://www.bsi-global.com/) (standards BS 1629:1989, BS 5605:1990, BS 6371:1983)
- [Australian Government](https://www.stylemanual.gov.au/)


## Normes en droit

- [Bluebook Standard System of American Legal Citation](http://www.legalbluebook.com/)
- [Indigo Book: A Manual of Legal Citation](https://law.resource.org/pub/us/code/blue/IndigoBook.html)
- [Oxford University Standard for Citation of Legal Authorities (OSCOLA)](https://www.law.ox.ac.uk/oscola) (libre)


## Normes dans les humanités

- [Modern Languages Association](https://www.mla.org/MLA-Style) ([page Wikipedia](http://en.wikipedia.org/wiki/The_MLA_Style_Manual))
- [Unified Style for Linguistics](https://clas.wayne.edu/linguistics/resources/style)
- [American Political Science Association](https://connect.apsanet.org/stylemanual/)
- [Modern Humanities Research Association](https://www.mhra.org.uk/style/)


## Normes en sciences

### Chimie, physique et sciences naturelles

- [Council of Science Editors](https://www.councilscienceeditors.org/scientific-style-and-format)
- [American Chemical Society](https://pubs.acs.org/doi/book/10.1021/acsguide) ([page Wikipedia](http://en.wikipedia.org/wiki/ACS_style))
- [American Institute of Physics](https://publishing.aip.org/wp-content/uploads/2021/03/AIP_Style_4thed.pdf) (PDF, libre)


### Ingénierie, informatique et technologie

- [IEEE](https://ieeeauthorcenter.ieee.org/wp-content/uploads/IEEE-Reference-Guide.pdf) (PDF, libre)


### Psychologie et sciences sociales

- [American Psychological Association](http://www.apastyle.org/) ([page Wikipedia en français](https://fr.wikipedia.org/wiki/Style_APA), [tutoriel libre en anglais APA](http://www.apastyle.org/learn/tutorials/basics-tutorial.aspx))
- [American Sociological Association](https://www.asanet.org/publications/journals/asa-style-guide/)
- [American Anthropological Association](http://www.aaanet.org/publications/guidelines.cfm)


### Sciences médicales and biomédicales

- [American Medical Association](http://www.amamanualofstyle.com//oso/public/index.html)
- [International Committee of Medical Journal Editors](http://www.icmje.org/urm_main.html/) (libre) basé sur:
- [National Library of Medicine](https://www.ncbi.nlm.nih.gov/books/NBK7256/) (libre)


## Guides de style commerciaux

De plus, il existe des guides de style commerciaux pour tous types de publications.

- [Chicago Manual of Style](http://www.chicagomanualofstyle.org)
- [Turabian](http://www.press.uchicago.edu/books/turabian/index.html)

De nombreux exemples de styles (ou codes typographiques) peuvent être trouvé à la [page Wikipédia correspondante](https://fr.wikipedia.org/wiki/Code_typographique).
