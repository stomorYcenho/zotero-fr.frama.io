# Comment puis-je fusionner des marqueurs ?

*Consulter cette page dans la documentation officielle de Zotero : [How do I merge tags?](https://www.zotero.org/support/kb/merging_tags) - dernière mise à jour de la traduction : 2022-12-19*
{ data-search-exclude }

Des marqueurs équivalents (par exemple "page web" et "site web") peuvent être fusionnés en renommant un marqueur au nom de l'autre. Faites un clic droit sur l'un des marqueurs dans le sélecteur de marqueurs (en bas à gauche de la fenêtre Zotero), sélectionnez "Renommer le marqueur..." et entrez le nom de l'autre marqueur, correspondant au nom que vous souhaitez conserver.
