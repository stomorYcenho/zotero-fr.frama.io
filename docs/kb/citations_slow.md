#  Pourquoi Zotero est-il lent à insérer des citations ou à mettre à jour la bibliographie ?

*Consulter cette page dans la documentation officielle de Zotero : [Why is Zotero slow to insert citations or update the bibliography?](https://www.zotero.org/support/kb/citations_slow) - dernière mise à jour de la traduction : 2022-11-27*
{ data-search-exclude }

Lorsque vous insérez une citation dans un document à l'aide du module de traitement de texte de Zotero, Zotero doit rechercher toutes les citations dans l'ensemble du document pour assurer une mise en forme correcte. Certaines règles du style bibliographiques tels que la mention _ibid_ ou la désambiguïsation des noms d'auteur impliquent que :

* la mise en forme d'une citation puisse dépendre des citations qui la précèdent;
* les bibliographies dépendent de la présence, et dans certains cas de l'ordre, de toutes les citations du document, y compris celles qui ont pu être supprimées ou déplacées depuis la dernière insertion d'une citation.

Dans les documents longs, l'analyse de l'ensemble du document peut prendre plusieurs secondes, voire plusieurs minutes, et ces mises à jour peuvent perturber le processus de rédaction. Word pour Mac 2008 et Google Docs sont particulièrement sujets à des ralentissements en raison des limitations techniques de ces programmes.

Pour accélérer votre rédaction, vous pouvez désactiver les mises à jour automatiques et reporter la mise à jour des citations jusqu'à ce qu'une actualisation manuelle soit déclenchée. Lorsque les mises à jour automatiques sont désactivées, l'insertion des citations reste instantanée, quelle que soit la taille du document.

Pour désactiver les mises à jour automatiques, cliquez sur le bouton "Document Preferences" du module de traitement de texte et décochez la case "Mise à jour automatique des citations".

![word_disable_automatic_updates](../images/word_disable_automatic_updates.png){class="imgcenter img500px"}

Pour illustrer le fonctionnement de l'insertion des citations lorsque les mises à jour sont désactivées, prenons un exemple. Supposons que nous ayons ajouté une citation pour un article de Jessica Smith en utilisant le style APA.

![delayed-citations-1](../images/delayed-citations-1.png)


Si nous insérons ensuite un article de James Smith, Zotero créera une citation selon la mise en forme par défaut requise par le style, sans tenir compte des autres citations du document.

![delayed-citations-2](../images/delayed-citations-2.png)


Zotero ajoute un soulignement en pointillés sous les citations nouvellement ajoutées pour nous rappeler qu'elles n'ont pas été mises à jour. Gardez toutefois à l'esprit que les citations existantes plus loin dans le document pourraient aussi être maintenant incorrectes.

Il remplace également la bibliographie par un avertissement.

![delayed-citations-3](../images/delayed-citations-3.png)


Nous pouvons rapidement insérer des citations de cette manière sans attendre chaque mise à jour. Lorsque nous sommes prêts à soumettre notre document, nous cliquons sur le bouton "Refresh" du module de traitement de texte.

![delayed-citations-4](../images/delayed-citations-4.png)


Zotero a analysé le document et mis à jour les citations et la bibliographie pour se conformer aux règles du style, qui dans ce cas exigent une désambiguïsation pour les premiers auteurs ayant le même nom de famille.

Pour éviter de soumettre accidentellement un document avec des citations non mises en forme, nous vous recommandons de laisser les mises à jour automatiques activées, sauf si vous constatez que les insertions prennent trop de temps pour un document donné.

Les méthodes alternatives pour accélérer les citations si vous souhaitez conserver l'activation des mises à jour automatiques sont de diviser les longs documents en chapitres ou d'utiliser pendant la rédaction un style de citation moins exigeant, tel que [Annual Reviews (auteur-date)](https://www.zotero.org/styles/annual-reviews-author-date), pour augmenter la vitesse d'insertion des citations.
