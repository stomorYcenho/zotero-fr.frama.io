# Comment installer Zotero sur un Chromebook?

*Consulter cette page dans la documentation officielle de Zotero : [How Do I Install Zotero on a Chromebook?](https://www.zotero.org/support/kb/installing_on_a_chromebook) - dernière mise à jour de la traduction : 2023-03-22*
{ data-search-exclude }

*Veuillez noter que Zotero ne peut fonctionner que sur les Chromebooks x86_64. Il ne peut pas fonctionner sur les Chromebooks basés sur ARM.*

## Étape 1 : Installez Linux dans votre Chrome OS

1. Dans votre Chromebook, ouvrez les Paramètres (*Settings*) et cliquez sur le menu de trois lignes en haut à gauche.
2. Dans le menu qui s'ouvre, sélectionnez “Installation de Linux (Beta)”.
3. Passez à travers le processus d'installation de Linux sur votre Chromebook.

## Étape 2 : Ouvrez le Terminal

1. Après que Linux est installé sur votre Chromebook, vous remarquerez qu'une nouvelle application appelée Terminal est disponible dans votre menu Applications.
2. Cliquez sur l'icône Terminal pour lancer l'application. Cela peut prendre quelques minutes la première fois.

## Étape 3 : Installez Zotero

Pour installer une version par paquet de Zotero ([maintenue par un membre de la communauté](https://github.com/retorquere/zotero-deb)), saisissez ces lignes de commande successivement dans le terminal :

`curl -sL https://raw.githubusercontent.com/retorquere/zotero-deb/master/install.sh | sudo bash`

`sudo apt update`

`sudo apt install zotero`

Si vous préférez, vous pouvez installer la version *tarball* officielle à partir de la page d'[installation de Zotero](https://www.zotero.org/download) mais vous devrez tout [paramétrer manuellement](../installation.md#linux).

Une fois terminé, vous pouvez fermer le Terminal et retourner dans le menu des Applications de votre Chromebook. Vous y verrez une icône pour Zotero et, en cliquant dessus, vous ouvrirez l'application Zotero. Vous pouvez alors épingler l'application dans votre zone de démarrage de Chrome.

## Étape 4 : Installez le connecteur Zotero

Pour utiliser le connecteur Zotero, qui permet d'enregistrer des références depuis Chrome vers Zotero et qui permet d'utiliser Zotero dans Google Docs, vous aurez peut être besoin d'intaller une application de redirection de port (*port-forwarding app*) comme Connection Forwarder. Sur certains Chromebooks cela peut ne pas être requis.

Pour paramétrer la redirection avec Connection Forwarder, fermez Zotero et toutes les autres applications Linux, puis créez la règle de redirection suivante :

* Protocol: TCP
* Source: 127.0.0.1 (*Localhost*) port 23119 (*Source Port*, c'est-à-dire le port de connexion de Chrome)
* Destination: 127.0.0.1 (*Localhost*) port 8080 (*Destination Port*, i.e., c'est-à-dire le port de destination dans Linux)

Dans l'application Zotero, allez dans Édition → Préférences → Avancées → Éditeur de configuration, réglez la préférence `extensions.zotero.httpServer.port` à `8080`, et ensuite redémarrez Zotero.

Une fois ces étapes terminées, vous devriez être prêt à utiliser Zotero.
