# Que dois-je faire si ma base de données Zotero est corrompue ?

*Consulter cette page dans la documentation officielle de Zotero : [What do I do if my Zotero database is corrupted?](https://www.zotero.org/support/kb/corrupted_database) - dernière mise à jour de la traduction : 2023-02-22*
{ data-search-exclude }

Zotero stocke vos informations dans un fichier de base de données, `zotero.sqlite`, dans [le répertoire de données de Zotero](../zotero_data.md). Si la base de données est corrompue, Zotero peut ne plus être en mesure de démarrer ou certaines opérations peuvent échouer.

La corruption de la base de données se produit généralement lorsque le répertoire de données est placé dans un dossier de stockage en nuage ou sur un lecteur réseau. Si vous avez déplacé votre répertoire de données vers l'un de ces endroits, vous devez le remettre à l'emplacement par défaut. Vous ne devez [jamais stocker le répertoire de données dans un stockage en nuage](https://www.zotero.org/support/kb/data_directory_in_cloud_storage_folder). (Il en va de même pour tout programme reposant sur une base de données).

## Si vous utilisiez la synchronisation

Si vos données sont toutes dans votre [bibliothèque en ligne](https://www.zotero.org/mylibrary), vous pouvez simplement synchroniser pour télécharger la dernière version de votre bibliothèque :

* soit à partir d'une base de données locale vide (après avoir déplacé votre fichier `zotero.sqlite` corrompu et redémarré Zotero),
* soit à partir d'une sauvegarde antérieure et non corrompue de la base de données. 

Dans ce dernier cas, Zotero récupérera simplement les modifications apportées depuis la dernière fois que vous avez utilisé cette base de données, sans avoir besoin de retélécharger toute votre bibliothèque. Une fois que vous êtes sûr d'avoir récupéré vos données, vous pouvez supprimer toutes les copies du fichier `zotero.sqlite` corrompu.

## Si vous n'utilisiez pas la synchronisation

Si la base de données était dans un stockage en nuage ou si vous avez une sauvegarde locale, vous pouvez essayer de restaurer à partir d'une version antérieure du fichier `zotero.sqlite`, y compris à partir de [l'une des dernières sauvegardes automatiques](../zotero_data.md#restaurer-a-partir-de-la-derniere-sauvegarde-automatique) dans votre répertoire de données Zotero. Pour cela, fermez Zotero, déplacez le fichier `zotero.sqlite` actuel hors du chemin, et  copiez à la place le fichier de sauvegarde, en tant que `zotero.sqlite`. Après avoir restauré à partir d'une sauvegarde et démarré Zotero, vérifiez l'intégrité de la base de données depuis le volet Avancées → Fichiers et dossiers des préférences de Zotero.

Si vous n'avez pas de sauvegarde, ou si les sauvegardes sont également corrompues, vous pouvez essayer de réparer les dégâts avec [l'outil de réparation de base de données Zotero](https://www.zotero.org/utils/dbfix/).

