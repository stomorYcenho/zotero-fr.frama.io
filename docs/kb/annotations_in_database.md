# Pourquoi Zotero stocke-t-il les annotations PDF dans sa base de données plutôt que dans le fichier PDF ?

*Consulter cette page dans la documentation officielle de Zotero : [Why does Zotero store PDF annotations in its database instead of in the PDF file?](https://www.zotero.org/support/kb/annotations_in_database) - dernière mise à jour de la traduction : 2022-09-15*
{ data-search-exclude }

Le nouveau [lecteur PDF](../pdf_reader.md) de Zotero 6 fait de la lecture et de l'annotation des PDFs une partie intégrante de l'expérience Zotero.

Pour permettre cette intégration, Zotero stocke les annotations dans la base de données Zotero, et non dans le fichier PDF. Cela permet une synchronisation rapide et sans conflit, y compris dans les groupes, et offre des fonctionnalités avancées qui ne seraient pas possibles autrement.

Les annotations Zotero peuvent être exportées vers des PDFs avec des annotations intégrées à tout moment et ne seront jamais verrouillées dans Zotero.

## Avantages des annotations Zotero

Avec les annotations stockées dans la base de données, Zotero est capable de synchroniser rapidement les détails de chaque annotation, nouvelle ou mise à jour. En revanche, avec les annotations standard dans le PDF, le fichier PDF entier devrait être transféré après chaque modification. Ainsi, si deux personnes d'un groupe ajoutaient une annotation en même temps, ou même si un PDF était simplement laissé ouvert sur un ordinateur, cela créerait un conflit de fichiers insoluble, obligeant l'utilisateur à choisir un fichier ou l'autre. Cela se produisait régulièrement dans les versions précédentes de Zotero, à la fois dans les bibliothèques personnelles et de groupe, et nous nous attendons à ce que l'annotation de PDF soit beaucoup plus utilisée maintenant qu'elle est intégrée à Zotero.

Le stockage des annotations dans la base de données permet également des fonctionnalités avancées, comme la possibilité de marquer les annotations et de les filtrer dans l'interface de Zotero. Nous prévoyons d'ajouter d'autres fonctionnalités étendues comme celle-ci à l'avenir.

Il y a également des avantages majeurs en termes de performance. Pour la synchronisation, comme nous l'avons vu plus haut, la sauvegarde des annotations dans le fichier nécessite que Zotero transfère le fichier entier - qui peut représenter plusieurs mégaoctets - après chaque modification, alors que le transfert d'une seule annotation est instantané. Il est également plus difficile pour Zotero de suivre les changements apportés aux fichiers externes, donc si vous annotez quelque chose en externe, il peut y avoir un délai avant de pouvoir rechercher ces annotations dans Zotero - vous devrez peut-être attendre que Zotero remarque la modification du fichier ou déclencher manuellement le traitement du fichier mis à jour.

Nous nous efforcerons toujours de prendre en charge les flux de travail externes de la manière la plus efficace possible, mais nous ne pourrons jamais offrir une expérience aussi transparente que celle que nous sommes en mesure de fournir lorsque tout se fait dans l'application.

## Interagir avec les annotations intégrées

Bien que Zotero enregistre ses propres annotations dans sa base de données, il est possible d'interagir avec les annotations intégrées dans un fichier PDF de la même manière, ainsi que d'exporter des PDF avec des annotations intégrées.

Les annotations intégrées s'affichent dans le lecteur PDF de Zotero, et vous pouvez les [ajouter aux notes](../pdf_reader.md#ajout-dannotations-aux-notes) Zotero exactement de la même manière que les annotations créées par Zotero. Les annotations externes sont en lecture seule par défaut - indiquées par une icône de verrouillage - mais vous pouvez les transférer dans Zotero en sélectionnant Fichier → "Importer des annotations..." depuis le lecteur PDF, après quoi elles seront entièrement modifiables. Les annotations sont supprimées du PDF pour éviter les conflits et les doublons. (Les premières versions de Zotero 6 comprenaient également une option "Store Annotations in File...", mais elle pouvait entraîner des conflits de fichiers et des pertes de données, et elle a été [supprimée](https://forums.zotero.org/discussion/comment/404140/#Comment_404140)).

Vous pouvez exporter une copie du PDF avec les annotations incorporées en utilisant Fichier → "Exporter le PDF..." depuis la vue de la bibliothèque ou "Enregistrer sous..." depuis le lecteur PDF. (Pour exporter le fichier original, faites glisser le fichier joint de la liste des documents vers votre système de fichiers ou utilisez le clic droit → "Afficher le fichier" et copiez le fichier à partir de là).

Lors de l'exportation de métadonnées (par exemple, BibTeX ou RIS) à partir de votre bibliothèque, il y a une nouvelle option "Inclure les annotations" sous "Exporter les fichiers" qui incorporera les annotations dans tous les PDF exportés. Nous prévoyons de prendre en charge d'autres méthodes d'exportation d'annotations dans les futures mises à jour.

## Utilisation d'un lecteur PDF externe

Bien que nous ayons essayé de créer une nouvelle et meilleure expérience pour les PDFs dans Zotero, qui exige que les annotations soient stockées dans la base de données, vous pouvez toujours choisir d'utiliser un autre lecteur PDF si vous décidez qu'il vous convient mieux. Vous pouvez définir le lecteur PDF par défaut dans le volet "Générales" des préférences de Zotero.

Si vous souhaitez conserver le lecteur PDF de Zotero comme lecteur par défaut, mais ouvrir occasionnellement un PDF en externe, il suffit de faire un clic droit sur le PDF dans Zotero et de choisir "Localiser le fichier", puis de double-cliquer sur le PDF dans votre système de fichiers. (Les annotations n'apparaîtront évidemment pas, et certaines modifications, comme le déplacement ou la suppression de pages, peuvent causer des problèmes avec les annotations existantes dans Zotero; c'est pourquoi nous n'exposons pas cette option directement. La rotation et la suppression de pages individuelles peuvent être effectuées en toute sécurité à partir de l'onglet des vignettes de la barre latérale du lecteur PDF de Zotero).

## Portabilité des données

La portabilité des données est l'un des principes fondateurs de Zotero, et nous nous sommes donné beaucoup de mal pour garantir que, si vous choisissez d'utiliser le lecteur PDF intégré, vos annotations ne seront jamais verrouillées dans Zotero.

Si vous décidez d'arrêter d'utiliser Zotero, vous pouvez simplement exporter toute votre bibliothèque avec les annotations intégrées dans vos PDFs.

En plus des méthodes détaillées ci-dessus pour l'exportation des annotations, les annotations sont stockées localement dans la base de données SQLite ouverte de Zotero et sont extractibles à l'aide d'outils open source standard. Elles sont également accessibles aux plugins dans Zotero et aux outils externes via l'API web de Zotero. 
