# Préférences : Générales

 *Consulter cette page dans la documentation officielle de Zotero : [Preferences : General](https://www.zotero.org/support/preferences/general) - dernière mise à jour de la traduction : 2022-08-30*
{ data-search-exclude }

Le volet "Générales" des préférences contrôle l'interface utilisateur de Zotero, les paramètres d'importation des documents, la gestion des fichiers et le comportement des bibliothèques de groupe. 

![general_pref.png](./images/preferences_general.png)

## Gestion des fichiers

* **Faire une capture automatique de la page lors de la création de documents à partir de pages Web :** Lorsque vous importez des documents à partir de sites Web ou archivez une page Web [avec le bouton d'enregistrement Zotero de votre navigateur](./adding_items_to_zotero.md), Zotero doit-il enregistrer une capture de la page Web en tant que pièce jointe au nouveau document (activé par défaut) ?

* **Joindre automatiquement les fichiers PDF associés lors de l'enregistrement d'un document :** Zotero doit-il télécharger automatiquement le fichier PDF de texte intégral des articles (ou parfois d'autres fichiers connexes) lorsque vous importez des documents avec le bouton d'enregistrement Zotero de votre navigateur (activé par défaut) ?
    * Pour télécharger automatiquement le matériel supplémentair en même temps que le fichier de l'article principal, voir [la préférence cachée correspondante](https://www.zotero.org/support/preferences/hidden_preferences#zotero_connector).

## Divers

* **Ajouter automatiquement aux documents des marqueurs à partir des mots-clés founis :**  Certains sites utilisent des marqueurs pour annoter et organiser les documents (par exemple le catalogue de la Bibliothèque du Congrès, qui comporte des descripteurs, ou la version en ligne du New York Times, qui utilise des mots-clés). Lorsque cette option est cochée (par défaut), ces annotations sont associées aux documents enregistrés en tant que [marqueurs ajoutés automatiquement](./collections_and_tags.md/#marqueurs-ajoutes-automatiquement). 

* **Supprimer automatiquement les documents mis à la corbeille depuis plus de ... jours :** Modifiez la durée pendant laquelle les documents sont conservés dans la corbeille avant d'être automatiquement supprimés (par défaut : 30 jours).

## Groupes

Par défaut, lorsque vous copiez des documents entre votre bibliothèque personnelle et une bibliothèque de groupe (ou entre différentes bibliothèques de groupe), les notes enfants, les captures de page Web et autres fichiers joints, les URI liées (à des pages Web ou à d'autres applications) et les marqueurs sont également copiés en même temps. Vous pouvez décocher "les notes enfant", "les captures d'écran et fichiers importés enfants", "les liens enfants" ou "les marqueurs" pour empêcher la copie des notes, pièces jointes, liens ou marqueurs. Notez que [les fichiers liés](./attaching_files.md#fichiers-joints-et-fichiers-lies) ne sont pas pris en charge dans les bibliothèques de groupe et ne seront pas copiés. 

