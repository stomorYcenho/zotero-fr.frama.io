# Langues

De par sa conformité à [Unicode](http://fr.wikipedia.org/wiki/Unicode), Zotero peut importer, sauvegarder, et citer des documents en n'importe quelle langue. Vous pouvez également changer la langue de l'interface de Zotero et des références et bibliographies. Pour finir, il existe une version multilingue non officielle de Zotero, qui permet de stocker les métadonnées des articles dans plusieurs langues (translittérations et traductions). 

Zotero s'installera automatiquement dans la lange de votre système d'exploitation. Si Zotero n'est pas disponible dans la langue de votre système d'exploitation, son interface sera en anglais, la langue par défaut.

## Changer de langue

### Zotero

Dans Zotero, la langue correspond automatiquement à celle de votre système d'exploitation. Pour sélectionner une autre langue, dans le menu Édition (Windows/Linux) ou le menu Zotero (Mac), sélectionnez Préférences, puis l'onglet Avancées. Utilisez le menu déroulant pour sélectionner la langue désirée.

### Références et bibliographies

Pour sélectionner une langue différente de celle de Zotero pour les références et bibliographies, sélectionnez celle-ci à l'endroit approprié selon le contexte :

-   La boîte de dialogue "Créer une citation/bibliographie" accessible via le menu contextuel
-   La boîte de dialogue "Zotero-Préférences du document" dans votre logiciel de traitement de texte
-   Les options de copie rapide dans l'onglet Exportation des Préférences de Zotero

## Contribuer aux traductions

Les erreurs de traduction peuvent être signalées sur les [forums de Zotero](http://forums.zotero.org/categories/). Si vous souhaitez faire davantage de contributions (comme par exemple traduire le logiciel Zotero dans une langue pas encore prise en charge), consultez les instructions pour les développeurs concernant la [localisation](https://www.zotero.org/support/dev/localization).

## Juris-M : Champs multilingues, traductions et translittérations

Juris-M (anciennement appelé Multilingual Zotero ou MLZ) est une version non officielle de Zotero gérée par la communauté qui ajoute un support supplémentaire pour les citations multilingues et juridiques. Juris-M vous permet de stocker les translittérations et les traductions des noms, des titres et d'autres champs, et de créer des citations et des bibliographies qui affichent ces informations (par exemple, "Soseki, Wagahai ha neko de aru [Je suis un chat] (1905-06)").

Juris-M est développé par Frank Bennett, un utilisateur de Zotero et un contributeur actif de Zotero. Si vous souhaitez essayer Juris-M, consultez la [page web du projet](https://juris-m.github.io/).
