# Le répertoire de données Zotero

*Consulter cette page dans la documentation officielle de Zotero : [The Zotero Data Directory](https://www.zotero.org/support/zotero_data) - dernière mise à jour de la traduction : 2022-12-21*
{ data-search-exclude }

## Localiser vos données Zotero

La méthode la plus simple et la plus fiable pour trouver vos données Zotero est de cliquer sur le bouton "Ouvrir le répertoire de données" dans l'onglet [Avancées](./advanced.md) de la fenêtre des [Préférences](./preferences.md) de Zotero. Cela affichera le répertoire de votre ordinateur qui contient votre base de données Zotero et les fichiers joints.

### Emplacements par défaut

A moins que vous n'ayez sélectionné un répertoire de données personnalisé dans les [préférences avancées](./advanced.md) de Zotero, vos données Zotero sont stockées à l'intérieur de l'un des répertoires suivants, en fonction de votre système d'exploitation.

<table>
<tbody>
<tr class="odd">
<td><strong>Mac</strong></td>
<td><code>/Users/&lt;nom_d_utilisateur&gt;/Zotero</code></td>
</tr>
<tr class="even">
<td><strong>Windows 7 et versions ultérieures</strong></td>
<td><code>C:\Users\&lt;nom_d_utilisateur&gt;\Zotero</code></td>
</tr>
<tr class="odd">
<td><strong>Windows XP/2000</strong></td>
<td><code>C:\Documents and Settings\&lt;nom_d_utilisateur&gt;\Zotero</code></td>
</tr>
<tr class="even">
<td><strong>Linux</strong></td>
<td><code>~/Zotero</code></td>
</tr>
</tbody>
</table>

Le bouton "Ouvrir le répertoire de données" affiche toujours le répertoire de données en cours d'utilisation et constitue la méthode recommandée pour trouver votre répertoire de données. Si vous ne pouvez pas accéder aux préférences de Zotero, une recherche du nom de fichier 'zotero.sqlite' peut également vous aider à localiser le répertoire de données de Zotero. 

??? Note "Versions antérieures"

    #### Zotero 4 pour Firefox (2017 et avant)

    <table>
    <tbody>
    <tr class="odd">
    <td><strong>Mac</strong></td>
    <td><code>/Users/&lt;nom_d_utilisateur&gt;/Bibliotheque/Application Support/Firefox/Profiles/&lt;caracteres_aleatoires&gt;/zotero</code> <small>Note : Le répertoire /Users/&lt;nom_d_utilisateur&gt;/Bibliotheque est caché par défaut. Pour y accéder, cliquez sur votre bureau, maintenez la touche Option (Alt) enfoncée, cliquez sur le menu "Aller" du Finder, puis sélectionnez "Bibliothèque" dans le menu.</small></td>
    </tr>
    <tr class="even">
    <td><strong>Windows 7 et versions ultérieures</strong></td>
    <td><code>C:\Users\&lt;nom_d_utilisateur&gt;\AppData\Roaming\Mozilla\Firefox\Profiles\&lt;caracteres_aleatoires&gt;\zotero</code></td>
    </tr>
    <tr class="odd">
    <td><strong>Windows XP/2000</strong></td>
    <td><code>C:\Documents and Settings\&lt;nom_d_utilisateur&gt;\Application Data\Mozilla\Firefox\Profiles\&lt;caracteres_aleatoires&gt;\zotero</code></td>
    </tr>
    <tr class="even">
    <td><strong>Linux (pour la plupart des distributions)</strong></td>
    <td><code>~/.mozilla/firefox/Profiles/&lt;caracteres_aleatoires&gt;/zotero</code></td>
    </tr>
    </tbody>
    </table>

    #### Zotero 4 Standalone (2017 et avant)

    <table>
    <tbody>
    <tr class="odd">
    <td><strong>Mac</strong></td>
    <td><code>/Users/&lt;nom_d_utilisateur&gt;/Bibliotheque/Application Support/Zotero/Profiles/&lt;caracteres_aleatoires&gt;/zotero</code> <small>Note : Le répertoire /Users/&lt;nom_d_utilisateur&gt;/Bibliotheque est caché par défaut. Pour y accéder, cliquez sur votre bureau, maintenez la touche Option (Alt) enfoncée, cliquez sur le menu "Aller" du Finder, puis sélectionnez "Bibliothèque" dans le menu.</small></td>
    </tr>
    <tr class="even">
    <td><strong>Windows 7 et versions ultérieures</strong></td>
    <td><code>C:\Users\&lt;nom_d_utilisateur&gt;\AppData\Roaming\Zotero\Zotero\Profiles\&lt;caracteres_aleatoires&gt;\zotero</code></td>
    </tr>
    <tr class="odd">
    <td><strong>Windows XP/2000</strong></td>
    <td><code>C:\Documents and Settings\&lt;nom_d_utilisateur&gt;\Application Data\Zotero\Profiles\&lt;caracteres_aleatoires&gt;\zotero</code></td>
    </tr>
    <tr class="even">
    <td><strong>Linux (pour la plupart des distributions)</strong></td>
    <td><code>~/.zotero/Profiles/&lt;caracteres_aleatoires&gt;/zotero</code></td>
    </tr>
    </tbody>
    </table>

## Contenu du répertoire de données

Le fichier le plus important dans le répertoire de données est le fichier `zotero.sqlite`, qui contient la majorité de vos données : métadonnées des documents, notes, marqueurs, etc. Quand Zotero démarre, il lit le fichier `zotero.sqlite` dans le répertoire en cours d'utilisation.

Le répertoire contient également un sous-répertoire `storage` avec des sous-dossiers portant des noms à 8 caractères (par exemple "N7SMB24A"). Ce répertoire contient toutes vos pièces jointes, telles que les fichiers PDF, les captures de pages web, les fichiers audio ou tout autre fichier que vous avez importé. Notez que les fichiers qui sont [liés](./attaching_files.md) ne sont pas copiés dans ce sous-répertoire.

Votre répertoire de données contiendra probablement plusieurs autres fichiers. Il peut s'agir :

* d'un fichier `zotero.sqlite.bak` (sauvegarde automatique de `zotero.sqlite` mise à jour lorsque vous quittez Zotero et que le fichier `zotero.sqlite.bak` existant n'a pas été mis à jour dans les 12 dernières heures),
* de fichiers `zotero.sqlite.[nombre].bak` (sauvegardes automatiques de `zotero.sqlite` créées durant certaines mises à jour de Zotero),
* aussi bien que de répertoires comme `locate`, `logs`, `pipes`, `styles`, et `translators`, qui sont créés automatiquement au démarrage de Zotero. 

**Attention** : Avant de copier, supprimer ou déplacer l'un de ces fichiers, assurez-vous que Zotero est fermé. Ne pas fermer Zotero avant de déplacer les fichiers peut endommager vos données.

## Sauvegarder votre bibliothèque Zotero

Nous vous recommandons fortement de sauvegarder régulièrement votre bibliothèque Zotero. Si la [synchronisation](./sync.md) est un excellent moyen de s'assurer que vous pouvez restaurer vos bibliothèques si quelque chose arrive à votre ordinateur, elle ne remplace pas une sauvegarde en bonne et due forme. Les serveurs de Zotero ne stockent que la version la plus récente de vos bibliothèques, et il suffit d'une seule synchronisation (éventuellement automatique) pour modifier la copie du serveur (bien que certaines modifications involontaires puissent être restaurées [à partir des sauvegardes automatiques de Zotero](#Restaurer-a-partir-de-la-dernière-sauvegarde-automatique)). 


Plutôt que de sauvegarder uniquement votre bibliothèque Zotero, nous vous recommandons d'utiliser un utilitaire de sauvegarde qui sauvegarde automatiquement et régulièrement l'ensemble de votre disque dur sur un périphérique externe et conserve des sauvegardes incrémentales de sorte que vous puissiez revenir à une version précise. La plupart des systèmes d'exploitation modernes offrent cette fonctionnalité (par exemple Time Machine sur Mac).

Si vous souhaitez sauvegarder spécifiquement vos données Zotero, [localisez vos données Zotero](#localiser_vos_donnees_zotero), fermez Zotero, et copiez votre répertoire de données (le *répertoire entier*, incluant `zotero.sqlite`, `storage` et les autres sous-répertoires) à un emplacement de sauvegarde, de préférence sur un autre périphérique de stockage. Comme pour toutes les données importantes, il est judicieux de sauvegarder fréquemment vos données Zotero, raison pour laquelle il est recommandé d'utiliser une système de sauvegarde automatique.

**Attention** : Vous ne devez pas utiliser l'export de votre bibliothèque Zotero (p. ex. aux formats Zotero RDF, BibTeX ou RIS) comme méthode de sauvegarde. Exporter et ré-importer une bibliothèque n'aboutit pas à une copie exacte de votre bibliothèque.  Cela réinitialisera les dates et heures des champs "Date d'ajout" et "Modifié", ainsi cela rompera le lien vers les citations existantes dans les documents de traitement de texte, ainsi que d'autres changements potentiels.

## Restaurer votre bibliothèque Zotero à partir d'une sauvegarde

Entre les sauvegardes manuelles, les sauvegardes automatiques et les données synchronisées, il est souvent possible de restaurer une bibliothèque Zotero perdue ou de restaurer des données qui ont été accidentellement supprimées.

Avant de suivre ces étapes, assurez-vous que [Zotero cherche vos données au bon endroit](#localiser-des-donnees-Zotero-manquantes).

### Restaurer votre bibliothèque Zotero en utilisant la synchronisation Zotero

Si vous utilisez la synchronisation Zotero et que vous avez une bibliothèque locale vide, vous pouvez probablement restaurer vos données simplement en synchronisant avec votre bibliothèque en ligne. Après avoir vérifié que votre bibliothèque est correcte sur zotero.org, entrez simplement à nouveau votre nom d'utilisateur et votre mot de passe dans le volet "Synchronisation" des préférences de Zotero et cliquez sur le bouton "Synchroniser avec zotero.org" dans la barre d'outils. (Zotero ne synchronise que les suppressions explicites, aussi synchroniser une bibliothèque vide n'écrasera pas les données du serveur **à moins que vous ne supprimiez les documents manuellement**).

Si vous souhaitez écraser une bibliothèque Zotero locale, fermez Zotero et supprimez l'ancien [répertoire de données Zotero](#localiser-vos-donnees-zotero) avant de synchroniser. Synchroniser votre base de données avec un compte Zotero différent vous invitera également à supprimer la base de données locale existante.

### Restaurer votre bibliothèque Zotero à partir d'une sauvegarde

Si vous n'utilisiez pas la synchronisation Zotero (ou si vous ne voulez pas effectuer une synchronisation complète) et que vous disposez d'une sauvegarde de votre répertoire de données Zotero, vous pouvez restaurer votre bibliothèque en remplaçant votre répertoire de données actif par votre répertoire de données sauvegardé.

Ouvrez le volet "Avancées" des préférences de Zotero et notez le chemin spécifié sous "Emplacement du répertoire de données". (Par défaut, ce sera "Zotero" dans votre répertoire principal.) Cliquez sur "Ouvrir le répertoire de données", ce qui doit afficher votre répertoire de données actif contenant `zotero.sqlite` et éventuellement un sous-répertoire `storage`. Fermez Zotero, placez-vous dans le dossier parent du répertoire de données Zotero (Cmd + flèche vers le haut sous Mac, Alt + flèche vers le haut sous Windows), et renommez le répertoire en "Zotero-Old". Ensuite, copiez le répertoire de données de votre sauvegarde à l'emplacement d'origine, par exemple "Zotero".

Lorsque vous rouvrez Zotero, vous devriez voir vos données Zotero restaurées.

Une fois vos données restaurées avec succès, vous pouvez supprimer le dossier "Zotero-Old", mais c'est une bonne idée de le conserver un certain temps, jusqu'à ce que vous soyez sûr de l'exactitude de vos données.

Notez que si vous utilisez la synchronisation Zotero, toutes les modifications que vous avez apportées à votre bibliothèque depuis la sauvegarde et que vous avez ensuite synchronisées avec votre bibliothèque en ligne seront appliquées à votre base de données restaurée dès que vous aurez effectué une nouvelle synchronisation. Si vous ne voulez pas que cela se produise, consultez la section suivante.

### Restaurer votre bibliothèque Zotero à partir d'une sauvegarde et écraser les modifications synchronisées

Si vous ou quelqu'un d'autre avez apporté des modifications indésirables à votre bibliothèque Zotero et synchronisé ces modifications avec votre bibliothèque en ligne, vous pouvez restaurer les données en utilisant une sauvegarde locale de votre répertoire de données Zotero.

1.  Désactivez temporairement la synchronisation automatique dans le volet "Synchronisation" des préférences de Zotero.
2.  Suivez les étapes de la section précédente pour restaurer votre bibliothèque à partir d'une sauvegarde de votre répertoire de données Zotero.
3. À ce stade, vous voyez vos données restaurées : si vous synchronisiez à nouveau, les données les plus récentes de la bibliothèque en ligne remplaceraient les données que vous venez juste de restaurer. Vous devez donc effectuer les étapes suivantes pour empêcher cela:
    1.  Si vous essayez de restaurer un petit nombre de documents ou de notes supprimés, vous pouvez simplement dupliquer ces documents — en cliquant avec le bouton droit de la souris et en choisissant "Dupliquer le(s) document(s)" — afin que les nouvelles copies restent même après la synchronisation.
    2.  Si vous essayez de restaurer des collections supprimées, vous pouvez dupliquer ces collections en les créant à nouveau, puis faire glisser les documents des anciennes collections vers les nouvelles. Lors de la synchronisation, les anciennes collections seront supprimées mais les nouvelles resteront.
    3.  Si de nombreux documents ont été affectés ou que des collections ont été supprimées, vous pouvez utiliser [Remplacer la bibliothèque en ligne](./kb/sync_reset_options.md#remplacer-la-bibliotheque-en-ligne) pour forcer Zotero à télécharger la version locale de la bibliothèque et écraser les modifications synchronisées précédemment.

Si vous êtes satisfait du résultat, vous pouvez réactiver la synchronisation automatique et continuer à travailler.

### Restaurer à partir de la dernière sauvegarde automatique

Si vous faites une erreur critique en utilisant Zotero (par exemple, si vous supprimez accidentellement une grand nombre de documents), vous pouvez restaurer la dernière sauvegarde automatique. Notez que les sauvegardes automatiques ne contiennent que les données, non les fichiers.

1.  Si vous utilisez la synchronisation, désactivez temporairement la synchronisation automatique dans le volet "Synchronisation" des préférences de Zotero.
2.  [Localisez vos données Zotero](#localiser-vos-donnees-zotero) et faites une copie de n'importe quel fichier `zotero.sqlite.bak`. Les horodatages des fichiers peuvent vous aider à déterminer quel fichier contiendrait les données que vous essayez de restaurer.
3.  Fermez Zotero. Dans votre répertoire de données, renommez `zotero.sqlite` en `zotero.sqlite.old`, renommez en `zotero.sqlite` une des copies `.bak` que vous avez faites, et redémarrez Zotero. Vous devriez maintenant voir la version sauvegardée de votre bibliothèque.
4.  Si vous utilisiez la synchronisation et que les modifications non souhaitées étaient déjà synchronisées, synchroniser à présent entraînerait le remplacement des données que vous venez juste de restaurer par les données les plus récentes de la bibliothèque en ligne. Pour empêcher cela, il faut effectuer les étapes suivantes:
    1.  Si vous essayez de restaurer des collections supprimées, vous pouvez dupliquer ces collections en les créant à nouveau, puis faire glisser les documents des anciennes collections vers les nouvelles. Lors de la synchronisation, les anciennes collections seront supprimées mais les nouvelles resteront.
    2.  Si vous essayez de restaurer un petit nombre de documents ou de notes supprimés, vous pouvez simplement dupliquer ces documents — en cliquant avec le bouton droit de la souris et en choisissant "Dupliquer le(s) document(s)" — afin que les nouvelles copies restent même après la synchronisation.
    3.  Si de nombreux documents ont été affectés ou que des collections ont été supprimées, vous pouvez utiliser [Remplacer la bibliothèque en ligne](./kb/sync_reset_options.md#remplacer-la-bibliotheque-en-ligne) pour forcer Zotero à télécharger la version locale de la bibliothèque et écraser les modifications synchronisées précédemment.

Si vous êtes satisfait du résultat, vous pouvez réactiver la synchronisation automatique et continuer à travailler. Conservez `zotero.sqlite.old` jusqu'à ce que vous soyez sûr que toutes vos données sont intactes et synchronisées sur tous vos ordinateurs.

### Restaurer à partir de la dernière sauvegarde de mise à niveau

Lorsque vous passez à une nouvelle version majeure de Zotero, Zotero met automatiquement à jour votre base de données pour qu'elle fonctionne avec la nouvelle version. Si vous souhaitez ultérieurement revenir à une version précédente de Zotero , vous devrez remplacer manuellement votre base de données par la sauvegarde automatique de Zotero effectuée lors de la mise à niveau. Dans la plupart des cas, il s'agit du fichier `zotero.sqlite.[nombre].bak` avec le nombre le plus élevé de votre répertoire de données Zotero.

Il est judicieux d'effectuer une sauvegarde de l'ensemble de votre répertoire de données Zotero avant toute modification.

Si vous avez synchronisé vos données avec les serveurs Zotero, revenir à une version précédente est aussi simple que de réinstaller la version précédente, en fermant Zotero, en remplaçant `zotero.sqlite` dans votre répertoire de données Zotero par `zotero.sqlite.\[plus grand nombre].bak`, et en relançant Zotero. (Notez que si vous essayez d'ouvrir une base de données mise à jour dans une version antérieure, Zotero affichera une erreur. Fermez simplement Zotero et remplacez le fichier .sqlite.) Zotero synchronisera ensuite à partir de la bibliothèque en ligne tous les changements effectués depuis votre dernière utilisation de l'ancienne base de données.

Si vous n'utilisiez pas la synchronisation, vous pouvez exporter vers Zotero RDF tous les documents ajoutés depuis la mise à niveau de la base de données, puis les réimporter dans la version précédente. [Trier](./sorting.md) votre bibliothèque par date d'ajout peut vous aider à trouver ces documents.

Pour désactiver temporairement les mises à jour ultérieures, allez dans l'Éditeur de configuration du volet "Avancées" des préférences de Zotero et définissez la valeur de `app.update.auto` comme "false". Notez que rester sur une ancienne version n'est pas une solution à long terme, étant donné que les anciennces versions ne sont plus prises en charge et peuvent empêcher la synchronisation ou recevoir des mises à jour de compatibilité de site à tout moment. Postez sur le forum de Zotero les raisons pour lesquels vous avez restauré une version antérieure, et pensez à vérifier périodiquement si cela ferait sens de réactiver les mises à jour automatiques.

## Localiser des données Zotero manquantes

Si vous ouvrez Zotero et découvrez que votre bibliothèque est vide ou qu'il manque beaucoup de données, voici les principales raisons qui peuvent l'expliquer.

* Si vous utilisiez une très ancienne version de Zotero - datant de 2017 ou avant - sans faire aucune mise à jour et que vous venez d'installer la dernière version de Zotero, référez-vous à [Données manquantes aprés la mise à jour vers Zotero 5](https://www.zotero.org/support/kb/data_missing_after_zotero_5_upgrade). Zotero 5 est sorti il y a plusieurs années, cela ne concerne donc plus la majorité des gens.

* Si vous utilisez un ordinateur différent de celui où les données manquent et que les données manquent aussi dans votre bibliothèque en ligne, les données n'ont simplement pas été synchronisées depuis l'ordinateur sur lequel elles ont été créées. Référez-vous à la page [Pourquoi les changements que je fais ne sont-ils pas synchronisés entre plusieurs ordinateurs et/ou zotero.org ?](./kb/changes_not_syncing.md).

* Si vous savez que vous aviez les données sur cette ordinateur auparavant, quelque chose a pu arriver à la base de données, ou Zotero cherche les données au mauvais endroit. Continuez la lecture pour connaître la marche à suivre.

Pour déterminer ce qui est arrivé aux données sur cet ordinateur, utilisez le bouton "Ouvrir le répertoire de données" dans le volet [Avancées](./advanced.md) des préférences Zotero pour afficher le répertoire de données en cours d'utilisation. Prenez note des nom, taille et date des fichiers dont le nom commence par `zotero.sqlite` dans ce dossier : il s'agit de votre base de données Zotero (`zotero.sqlite`) et des sauvegardes automatiques de cette base de données (*.bak). Une base de données Zotero vide fera environ 1 Mo (~1 000 Ko) ou 5 Mo.

Si vous ne voyez que des fichiers `zotero.sqlite` de 1 Mo ou 5 Mo, cherchez dans le dossier `storage` (s'il existe) les dossiers dont les dates correspondent à votre utilisation précédente de Zotero.

* Si vous voyez des dossiers dans `storage`, il s'agit probablement du répertoire de données Zotero que vous utilisiez précédemment, mais quelque chose est arrivé à la base de données `zotero.sqlite` en dehors de Zotero (par exemple, vous pouvez avoir supprimé `zotero.sqlite` par accident et essayant de faire de la place sur votre disque dur). Vous devrez donc peut-être restaurer un fichier `zotero.sqlite` à partir d'une sauvegarde.

    1. Cherchez les fichiers `zotero.sqlite.bak` les plus volumineux dans le répertoire de données ou cherchez le fichier `zotero.sqlite` le plus volumineux dans vos sauvegardes. (Il n'est pas possible de restaurer les données à partir du dossier `storage` seul.) Lorsque vous démarrez Zotero, il lit le fichier `zotero.sqlite` dans le répertoire de données actif. Vous pouvez donc essayer d'autres copies `zotero.sqlite` en les copiant à cet emplacement et avec ce nom. N'importez pas un fichier .sqlite dans Zotero via Fichier > "Importer...", cela ne fonctionnera pas.

    2. Que vous ayez ou non une sauvegarde, si vous avez synchronisé Zotero, vous pouvez synchroniser toutes vos données dans votre [bibliothèque en ligne](https://www.zotero.org/mylibrary). Si vous avez une sauvegarde, toutes les données plus récentes que la sauvegarde seront téléchargées. Si vous n'avez qu'une base de données vide, toutes les données seront téléchargées. Dans un cas comme dans l'autre, vous n'écraserez pas les données de la bibliothèque en ligne par une simple synchronisation - la synchronisation ne fonctionne pas de cette façon.

    3. Si vous ne trouvez pas de copies de `zotero.sqlite` et que vous n'utilisiez pas la synchronisation de Zotero, vous allez sans doute malheureusement devoir recréer votre base de données à partir de zéro. Fermez Zotero, déplacer le répertoire de données actif de Zotero sur le bureau et renommez le "Zotero Old". Redémarrez Zotero pour créer une nouvelle bibliothèque. Vous pouvez cherchez tous les PDF dans le dossier "Zotero Old" et les glisser dans Zotero. Zotero va essayer de [récupérer les métadonnées des PDF](./retrieve_pdf_metadata.md). Vous pouvez aussi extraire les données utilisées par le module pour traitement de texte depuis les documents Word ou LibreOffice à l'aide du module [Reference Extractor](https://rintze.zelle.me/ref-extractor/), mais notez que les données réimportées de cette manière ne seront pas liées aux documents existants.

* Si ce n'est pas le répertoire auquel vous vous attendiez, ou si vous ne voyez pas de répertoire `storage`, vous devrez localiser votre précédent répertoire de données. Ensuite, vous devrez soit modifier le répertoire de données spécifié dans les préférences, soit, après avoir fermé Zotero, renommer le répertoire de données spécifié dans les préférences (par exemple, en "Zotero-Old") et déplacer le répertoire Zotero que vous souhaitez utiliser à l'emplacement spécifié dans les préférences. Si vous n'êtes pas sûr de l'emplacement de vos données Zotero les plus récentes, il est judicieux de rechercher des versions plus volumineuses de `zotero.sqlite` ou `zotero.sqlite.bak` stockées ailleurs sur votre disque dur et de consulter les dates des dossiers dans le répertoire `storage`.

    * A moins que vous n'ayez une bonne raison d'utiliser un emplacement personnalisé pour votre répertoire de données, nous vous recommandons d'utiliser [l'emplacement par défaut](#emplacements-par-defaut) dans votre répertoire principal.

    * Lorsque vous spécifiez un emplacement personnalisé pour votre répertoire de données, gardez à l'esprit que Zotero ne déplace ni ne copie aucune donnée. Vous devez toujours copier vos données à l'emplacement spécifié. De plus, lorsque vous faites pointer l'emplacement du répertoire de données vers un répertoire existant, assurez-vous de spécifier le répertoire parent contenant `zotero.sqlite` et `storage`, et non le répertoire `storage` lui-même.

Si vous avez suivi ces étapes et que vous ne trouvez toujours pas vos données, publiez un message sur les forums Zotero avec les informations suivantes.

-   Les nom, taille et date des fichiers `zotero.sqlite` de votre répertoire de données actuel.
-   S'il existe un dossier `storage` contenant des sous-dossiers dont les dates correspondent à votre utilisation précédente de Zotero.
-   Si votre répertoire de données actuel se trouve à l'emplacement par défaut ("Zotero" dans votre dossier principal).
-   La dernière fois que vous avez utilisé Zotero sur cet ordinateur, et ce qui s'est passé sur votre ordinateur depuis.
-   Ce que vous avez essayé jusqu'à présent.
