# Préférences

 *Consulter cette page dans la documentation officielle de Zotero : [Preferences](https://www.zotero.org/support/preferences) - dernière mise à jour de la traduction : 2022-08-26*
{ data-search-exclude }

## La fenêtre des préférences

De nombreuses fonctions de Zotero peuvent être personnalisées via la fenêtre des préférences de Zotero. Pour ouvrir les préférences, cliquez sur le menu "Édition -&gt; Préférences" (sur Windows ou Linux) ou "Zotero -&gt; Préférences" (sur Mac). Vous pouvez aussi utiliser le raccourci clavier `Ctrl/Cmd`-`,`.

![preferences_tabs.png](./images/preferences_tabs.png)

La fenêtre des préférences est divisée en différents panneaux.

-   **[Générales](./general.md)** - Ajuster l'apparence, les paramètres d'importation et d'autres fonctions générales.
-   **[Synchronisation](./preferences_sync.md)** - Configurer la synchronisation des données et des fichiers.
-   **[Recherche](./search.md)** - Gérer l’indexation du texte intégral des fichiers PDF et visualiser les statistiques correspondantes.
-   **[Exportation](./export.md)** - Définir les paramètres par défaut pour la génération des bibliographies et des citations.
-   **[Citer](./cite.md)** - Ajouter, retirer, modifier et prévisualiser les styles bibliographiques, et installer les modules de traitement de texte.
-   **[Avancées](./advanced.md)** - Localisation des données Zotero, rechercher dans la bibliothèque, et autres paramètres avancés.

## Préférences cachées

En plus des préférences affichées dans la fenêtre des préférences de Zotero, il existe un certain nombre de [préférences cachées (page en anglais)](https://www.zotero.org/support/preferences/hidden_preferences) qui ne peuvent être modifiées que par le biais de l'Éditeur de configuration, accessible depuis l'onglet "Préférences -&gt; Avancées -&gt; Général".
