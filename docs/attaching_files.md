# Ajouter des fichiers à votre bibliothèque Zotero

*Consulter cette page dans la documentation officielle de Zotero : [Adding Files to your Zotero Library](https://www.zotero.org/support/attaching_files) - dernière mise à jour de la traduction : 2022-10-04*
{ data-search-exclude }

En plus des données bibliographiques, des notes et des marqueurs, Zotero peut ausssi être utilisé comme gestionnaire de fichiers. Cette page décrit comment ajouter de différentes façons des fichiers à votre bibliothèque Zotero, et comment les fichiers ajoutés sont stockés et synchronisés.

## Fichiers enfants et fichiers indépendants

Les fichiers peuvent être ajoutés soit comme *documents indépendants*, soit comme *documents enfants* de [documents bibliographiques](./adding_items_to_zotero.md). Il est généralement conseillé de travailler avec des fichiers comme documents enfants. De nombreuses fonctionnalités Zotero ne permettent pas de travailler avec des fichiers indépendants, notamment les fonctionnalités de citation, la fonctionnalité "Mes publications", et la plupart des types de recherche, car ces fichiers indépendants sont dépourvus de métadonnées bibliographiques.

Lorsque vous enregistrez directement un PDF dans votre bibliothèque, Zotero s'efforce de récupérer des métadonnées pertinentes et de créer automatiquement un document parent. Si le document ne peut pas être reconnu, le PDF est laissé comme document indépendant. Vous pouvez créer un document parent en enregistrant un document depuis votre navigateur web et en glissant le PDF sur ce nouveau document, ou alors en faisant un clic droit sur le PDF, puis en choisissant "Créer un document parent", et enfin en indiquant un identifiant tel qu'un DOI ou un ISBN. Si ces procédures ne fonctionnent pas, vous pouvez aussi cliquer sur "Saisie manuelle" après avoir sélectionné "Créer un document parent", et compléter manuellement les métadonnées pour le document.

## Fichiers joints et fichiers liés

Les fichiers peuvent être ajoutés à votre bibliothèque Zotero soit sous la forme de fichiers joints, soit sous la forme de fichiers liés.

### Fichiers joints

Par défaut, les fichiers joints sont stockés dans le [répertoire de données Zotero](./zotero_data.md), et Zotero les gère automatiquement, y compris en les supprimant si vous supprimez les documents auxquels il sont attachés dans Zotero. Si vous utilisez [la synchronisation des fichiers](./sync.md), Zotero synchronise automatiquement les fichiers joints entre vos appareils et les rend disponibles dans votre bibliothèque en ligne sur zotero.org. Lorsque vous ajoutez un fichier joint à partir d'un fichier enregistré sur votre ordinateur, le fichier est copié dans le répertoire de données de Zotero, aussi vous pouvez souhaiter supprimer l'original pour éviter toute confusion.

Lorsque vous utilisez la synchronisation des fichiers, vous pouvez choisir de ne télécharger les fichiers joints qu'au moment où vous en avez besoin, en évitant ainsi la nécessité de télécharger tous les fichiers sur tous vos appareils. Une future version de Zotero vous permettra de choisir combien de temps conserver les fichiers synchronisés sur un ordinateur donné, afin de limiter l'espace disque nécessaire, en les téléchargeant à nouveau, temporairement, quand vous en avez besoin.

Pour utiliser avec d'autres logiciels les fichiers joints, vous pouvez utiliser les fonctionnalités de recherche et d'organisation de Zotero pour trouver rapidement les documents pertinents. Vous pouvez ensuite soit les glisser directement depuis Zotero (p. ex. dans un courriel) ou faire un clic droit et sélectionner "Localiser le fichier" pour l'afficher dans votre gestionnaire de fichier (Explorer, Finder, etc.). Si vous préférez trouver des fichiers sans passer par Zotero, vous pouvez utiliser les fonctionnalités de recherche de votre système d'exploitation (tel que Spotlight sur macOS) ou créer un dossier intelligent pour lister tous les PDF de votre dossier Zotero et pouvoir ainsi interagir directement avec les fichiers.

Nous vous recommandons vivement d'utiliser des fichiers joints pour une expérience optimale.

### Fichiers liés

Dans le cas des fichiers liés, Zotero stocke uniquement un lien vers l'emplacement du fichier original sur votre ordinateur. Les fichiers liés ne sont pas synchronisés, ni supprimés si les documents auxquels ils sont attachés sont supprimés dans Zotero. Ils ne peuvent pas non plus être utilisés au sein d'une bibliothèque de groupe, car il n'y a aucune garantie que les autres membres du groupe aient accès au même emplacement de fichier. En outre, les fichiers liés ne fonctionnent pas dans l'[application Zotero pour iOS](https://​apps.apple.com/​us/​app/​zotero/​id1513554812).

Vous pouvez ajouter un fichier lié en sélectionnant un document existant et en choisissant "Joindre un lien vers le fichier…" depuis le bouton trombone "Ajouter une pièce jointe". Pour utiliser la [récuparation des métadonnées d'un PDF](./retrieve_pdf_metadata.md), utilisez "Lien vers un fichier…" depuis le menu "Nouveau document" (![Icône verte avec le signe +](./images/add.png)). Vous pouvez aussi [maintenir la touche clavier appropriée en glissant un fichier dans Zotero](#ajouter-des-fichiers-lies). Si vous choisissez d'utiliser exclusivement des fichiers liés, le module complémentaire [ZotFile](http://zotfile.com/) peut vous aider à mettre en place un processus de travail plus efficace.

Si vous synchronisez des fichiers liés en utilisant un outil externe (Dropbox, etc.) pour les utiliser sur plusieurs ordinateurs, il est conseillé de définir le [répertoire de base pour les pièces jointes liées](./advanced.md#repertoire-de-base-pour-les-pieces-jointes-liees), afin que les fichiers puissent être trouvés par Zotero sur chaque ordinateur, même si le dossier qui les contient se trouve à un endroit différent dans le système de fichiers.

Étant donné la complexité des flux de travail basés sur des fichiers liés, et les différences entre les systèmes spécifiques, Zotero ne peut pas aider à résoudre les problèmes qui surviennent dans de telles configurations particulières.

Si vous souhaitez convertir des fichiers liés en fichiers joints afin de permettre à Zotero de les gérer, vous pouvez le faire à partir du menu Outils → Gérer les pièces jointes.

## Ajouter des fichiers

### Ajouter des fichiers depuis le navigateur

Zotero peut enregistrer automatiquement des captures de pages web ou des PDF associés quand vous utilisez le [bouton "Save to Zotero" du connecteur Zotero dans votre navigateur](./adding_items_to_zotero.md#via-votre-navigateur-web). Il est possible de désactiver cette sauvegarde automatique dans les [préférences de Zotero](./preferences.md). Ces captures et ces PDF sont stockés comme copies dans le [répertoire de données Zotero](./zotero_data.md) et apparaissent comme documents enfants du document enregistré.

### Ajouter des fichiers depuis la fenêtre Zotero

#### Glisser et déposer

Les fichiers peuvent être copiés dans votre bibliothèque en faisant glisser un fichier depuis votre gestionnaire de fichiers (Explorateur Windows, Finder, Nautilus, etc.) vers la fenêtre Zotero, et en le déposant soit dans une collection dans le panneau de gauche, soit dans le panneau central. Les fichiers déposés sur un document Zotero existant dans le panneau central sont ajoutés comme documents enfants. Les fichiers déposés dans une collection ou entre des documents dans le panneau central sont ajoutés comme documents indépendants.

Vous pouvez aussi glisser et déposer un fichier existant déjà comme document indépendant sur un document Zotero pour qu'il en devienne le document enfant.

##### Ajouter des fichiers liés

Par défaut, les fichiers glissés dans Zotero sont ajoutés en tant que **copies** des fichiers originaux. Pour ajouter à la place des **liens** vers les fichiers originaux, maintenez appuyées les touches `Ctrl`+`Maj` (Windows/Linux) or `Cmd`+`Option` (Mac) pendant le glisser-déposer.

#### Bouton "Nouveau document"

Les copies enregistrées d'un fichier et les liens vers un fichier peuvent être créés en cliquant sur le bouton "Nouveau document" (![Icône verte avec le signe +](./images/add.png)) en haut du panneau central, puis en choisissant "Stocker une copie du fichier..." ou "Lien vers un fichier...". Cela crée des documents indépendants.

#### Menu "Ajouter une pièce jointe"

![](./images/paperclip_dropdown_menu.png)

Lorsque vous avez sélectionné un seul document dans le panneau central, vous pouvez cliquer sur le bouton trombone "Ajouter une pièce jointe" en haut du panneau central. Choisissez soit "Joindre une copie enregistrée d'un fichier…" soit "Joindre un lien vers un fichier..." pour ajouter des fichiers en tant que pièce jointe à ce document.

Vous pouvez aussi "Joindre un lien vers un URI..." pour ajouter un lien à une page web (`http://` ou `https://`) ou à un autre programme de votre ordinateur (par exemple., OneNote `onenote://` ou Evernote `evernote://`).

Ces options sont aussi accessibles par un clic-droit sur un document, sous le menu "Ajouter une pièce jointe".

## Accéder aux fichiers

Pour accéder aux fichiers de votre bibliothèque, double-cliquez sur le document dans le panneau central. Vous pouvez aussi cliquer avec le bouton droit sur le document et choisir "Afficher le PDF" ou "Afficher le fichier".

Pour aller à l'emplacement d'un fichier joint ou d'un fichier lié, cliquez avec le bouton droit sur le document dans le panneau Zotero et choisissez "Localiser le fichier". Les fichiers joints sont stockés dans le [répertoire de données Zotero](./zotero_data.md). Chaque fichier a son propre sous-répertoire, nommé par une chaîne de huit caractères aléatoire (par ex: "M52J3FPU").

## Captures de pages web

![](./images/save_button_webpage.png)

Zotero peut archiver une page Web en créant une capture, un fichier hors ligne qui reflète l'état de la page au moment où la capture a été prise. Si le connecteur Zotero ne reconnaît pas les données d'une page, vous pouvez enregistrer la page en tant que document de type "Page Web" et lui attacher une capture en cliquant sur le bouton "Save to Zotero" dans la barre d'outils du navigateur. Vous pouvez également prendre une capture de n'importe quelle page en cliquant avec le bouton droit de la souris (cliquer et maintenir enfoncé dans Safari) sur le bouton "Save to Zotero" et en choisissant "Save to Zotero (Web Page with Snapshot)".

Par défaut, Zotero fait automatiquement des captures de pages web lorsque vous importez des documents à partir de pages web. Vous pouvez désactiver ce paramètre dans les [préférences générales de Zotero](./general.md).

